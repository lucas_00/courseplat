import Vue from 'vue';
import VueRouter from 'vue-router';

import EmptyRouterView from '@/js/components/global/EmptyRouterView';

import NotFound from '@/js/views/pages/exceptions/NotFound';

import LogoutComponent from '@/js/views/auth/LogoutComponent';
import LoginComponent from '@/js/views/auth/LoginComponent';
import RegisterComponent from '@/js/views/auth/RegisterComponent';

import CoursesDashboardComponent from '@/js/views/layouts/CoursesDashboardComponent';
import UsersIndex from '@/js/views/pages/user/UsersIndex';

import SearchComponent from '@/js/views/pages/search/SearchComponent';

import CoursesIndexComponent from '@/js/views/pages/courses/CoursesIndexComponent';
import CoursesCreateComponent from '@/js/views/pages/courses/CoursesCreateComponent';
import CoursesShowComponent from '@/js/views/pages/courses/CoursesShowComponent';
import CoursesStatisticsComponent from '@/js/views/pages/courses/CoursesStatisticsComponent';
import CoursesEditComponent from '@/js/views/pages/courses/CoursesEditComponent';

import UnitsShowComponent from '@/js/views/pages/courses/units/UnitsShowComponent';
// import UnitsEditComponent from '@/js/views/pages/courses/units/UnitsEditComponent';
import UnitsCreateComponent from '@/js/views/pages/courses/units/UnitsCreateComponent';

import LessonsShowComponent from '@/js/views/pages/courses/units/lessons/LessonsShowComponent';
import LessonsEditComponent from '@/js/views/pages/courses/units/lessons/LessonsEditComponent';
import LessonsCreateComponent from '@/js/views/pages/courses/units/lessons/LessonsCreateComponent';

import ActivitiesCreateComponent from '@/js/views/pages/courses/activities/ActivitiesCreateComponent';

import StadisticsComponent from '@/js/views/pages/stadistics/StadisticsComponent';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		component: EmptyRouterView,
		children:[
			{
				path: '',
				name: 'home',
				component: CoursesIndexComponent,
				meta: {
					requiresAuth: true,
				}
			},{
				path: 'logout',
				name: 'logout',
				component: LogoutComponent,
				meta: {
					requiresAuth: true,
				}
			},{
				path: 'login',
				name: 'login',
				component: LoginComponent,
			},{
				path: 'register',
				name: 'register',
				component: RegisterComponent
			},{
				path: 'user',
				name: 'user',
				component: UsersIndex,
				meta: {
					requiresAuth: true,
				}
			},{
				path: 'courses',
				component: CoursesDashboardComponent,
				meta: {
					requiresAuth: true,
				},
				children: [
					{
						path: '',
						name: 'courses.index',
						component: CoursesIndexComponent,
					},{
						path: ':course(\\d+)',
						component: EmptyRouterView,
						children: [
							{
								path: '',
								name: 'courses.show',
								component: CoursesShowComponent,

							},{
                                path: 'edit',
                                name: 'courses.edit',
                                component: CoursesEditComponent,
                            },{
                                path: 'units',
								component: EmptyRouterView,
								children: [
									{
										path: ':unit(\\d+)',
										component: EmptyRouterView,
										children: [
											{
												path: '',
												name: 'courses.show.units.show',
												component: UnitsShowComponent,
											},{
                                                path: 'lessons',
                                                component: EmptyRouterView,
                                                children: [
                                                    {
                                                        path: ':lesson(\\d+)',
                                                        component: EmptyRouterView,
                                                        children: [
                                                            {
                                                                path: '',
                                                                name: 'courses.show.units.show.lessons.show',
                                                                component: LessonsShowComponent,
                                                            },{
                                                                path: 'edit',
                                                                name: 'courses.show.units.show.lessons.show.edit',
                                                                component: LessonsEditComponent,
                                                            }
                                                        ]
                                                    },{
                                                        path: 'create',
                                                        name: 'courses.show.units.show.lessons.create',
                                                        component: LessonsCreateComponent,
                                                    }
                                                ]
                                            }
										]
                                    },{
										path: 'create',
										name: 'courses.show.units.create',
										component: UnitsCreateComponent,
									},
								]
							},{
								path: 'activities/create',
								name: 'courses.show.activities.create',
								component: ActivitiesCreateComponent,
							}
						]
					},{
						path: 'create',
						name: 'courses.create',
						component: CoursesCreateComponent,
					},{
						path: 'statistics',
						name: 'courses.statistics',
						component: CoursesStatisticsComponent,
					}
				]
			},{
				path: 'stadistics',
				name: 'stadistics',
				component: StadisticsComponent,
				meta: {
					requiresAuth: true,
				}
			},{
				path: 'search',
				name: 'search',
				component: SearchComponent,
				meta: {
					requiresAuth: true,
				}
			}
		]
	},{
		path: '*',
		component: NotFound
	}
];

const router = new VueRouter({
  	mode: 'history',
  	routes
});

export default router;
