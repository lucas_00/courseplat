import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/api/courses', headers);
    },
    find(course_id, unit_id, headers) {
        return axios.get(`/api/courses/${course_id}/units/${unit_id}`, headers);
    },
    create(course_id, data, headers) {
        return axios.post(`/api/courses/${course_id}/units/`, data, headers);
    },
    edit(course_id, unit_id, headers) {
        return axios.get(`/api/courses/${course_id}/units/${unit_id}/edit`, headers);
    },
    update(course_id, unit_id, data, headers) {
        return axios.post(`/api/courses/${course_id}/units/${unit_id}/update`, data, headers);
    },
    destroy(course_id, unit_id, headers) {
        return axios.put(`/api/courses/${course_id}/units/${unit_id}/destroy`, headers);
    },
};
