import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/api/courses', headers);
    },
    find(id, headers) {
        return axios.get(`/api/courses/${id}`, headers);
    },
    create(data, headers) {
        return axios.post(`/api/courses`, data, headers);
    },
    edit(id, headers) {
        return axios.get(`/api/courses/${id}/edit`, headers);
    },
    update(id, data, headers) {
        return axios.post(`/api/courses/${id}/update`, data, headers);
    },
    destroy(id, headers) {
        return axios.put(`/api/courses/${id}`, headers);
    },
};
