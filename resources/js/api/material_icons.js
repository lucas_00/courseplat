import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/api/icons', headers);
    },
    search(icon, headers) {
        return axios.get(`/api/icons/${icon}`, headers);
    },
};
