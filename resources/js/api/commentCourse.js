import axios from 'axios';

export default {
    store(id, data, headers) {
        return axios.post(`/api/courses/${id}/comments`, data, headers);
    },
};