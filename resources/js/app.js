/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');



import EventBus from './EventBus';
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import VueApexCharts from 'vue-apexcharts';
import Routes from '@/js/routes.js';
import Store from '@/js/store.js';
import App from '@/js/views/App';
import '@mdi/font/css/materialdesignicons.css';

import VueQuillEditor from 'vue-quill-editor';

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.config.productionTip = false;

Vue.prototype.$bus = EventBus;

Vue.use(VueAxios, axios);
Vue.use(Vuetify);
Vue.use(VueQuillEditor, /* { default global options } */)
Vue.component('apexchart', VueApexCharts);

Routes.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!Store.getters.loggedIn) {
            next({
                name: 'login',
            })
        } else {
            next();
        }
    } else {
        next();
    }
})

const app = new Vue({
    el: '#app',
    router: Routes,
    store: Store,
    vuetify: new Vuetify(),
    render: h => h(App)
});

export default app;
