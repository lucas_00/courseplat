<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\UnitElement;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(UnitElement::class, function (Faker $faker) {
    return [
        'order' => 1,
        'unit_id' => 1,
        'lesson_id' => 1,
        'exam_id' => null,
        'activity_id' => null,
        'unit_element_type_id' => 2,
    ];
});
