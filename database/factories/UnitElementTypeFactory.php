<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\UnitElementType;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(UnitElementType::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
    ];
});
