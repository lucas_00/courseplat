<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Status;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Status::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
    ];
});
