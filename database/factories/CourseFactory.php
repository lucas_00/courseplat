<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Course;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'title' => $faker->paragraph(1),
        'description' => $faker->paragraph(5),
        'status_id' => 1,
        'user_id' => 1
    ];
});
