<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\LessonElementType;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(LessonElementType::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
