<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Exam;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Exam::class, function (Faker $faker) {
    return [
        'title' => $faker->paragraph(1),
        'subtitle' => $faker->paragraph(5),
        'content' => $faker->paragraph(5),
        'status_id' => 1,
        'course_id' => 1,
        'user_id' => 1
    ];
});
