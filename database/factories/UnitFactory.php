<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Unit;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Unit::class, function (Faker $faker) {
    return [
        'title' => $faker->text,
        'description' => $faker->text,
        'icon_name' => null,
        'user_id' => 1,
        'status_id' => 1,
        'course_id' => 1,
    ];
});
