<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Lesson;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Lesson::class, function (Faker $faker) {
    return [
        'title' => $faker->paragraph(1),
        'info' => $faker->paragraph(5),
        'status_id' => 1,
        'unit_id' => 1,
        'user_id' => 1
    ];
});
