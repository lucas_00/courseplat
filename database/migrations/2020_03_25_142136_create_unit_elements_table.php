<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order');
            $table->integer('unit_id')->unsigned();
            $table->integer('lesson_id')->unsigned()->nullable();
            $table->integer('activity_id')->unsigned()->nullable();
            $table->integer('exam_id')->unsigned()->nullable();
            $table->integer('unit_element_type_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_elements');
    }
}
