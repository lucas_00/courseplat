<?php

use Illuminate\Database\Seeder;

class MaterialIconsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MaterialIcon::class)->create([
            'name' => "ab-testing",
            'content' => "\F001C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "access-point",
            'content' => "\F002",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "access-point-network",
            'content' => "\F003",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "access-point-network-off",
            'content' => "\FBBD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account",
            'content' => "\F004",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-alert",
            'content' => "\F005",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-alert-outline",
            'content' => "\FB2C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-arrow-left",
            'content' => "\FB2D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-arrow-left-outline",
            'content' => "\FB2E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-arrow-right",
            'content' => "\FB2F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-arrow-right-outline",
            'content' => "\FB30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-badge",
            'content' => "\FD83",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-badge-alert",
            'content' => "\FD84",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-badge-alert-outline",
            'content' => "\FD85",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-badge-horizontal",
            'content' => "\FDF0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-badge-horizontal-outline",
            'content' => "\FDF1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-badge-outline",
            'content' => "\FD86",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-box",
            'content' => "\F006",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-box-multiple",
            'content' => "\F933",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-box-multiple-outline",
            'content' => "\F002C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-box-outline",
            'content' => "\F007",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-cancel",
            'content' => "\F030A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-cancel-outline",
            'content' => "\F030B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-card-details",
            'content' => "\F5D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-card-details-outline",
            'content' => "\FD87",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-cash",
            'content' => "\F00C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-cash-outline",
            'content' => "\F00C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-check",
            'content' => "\F008",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-check-outline",
            'content' => "\FBBE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-child",
            'content' => "\FA88",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-child-circle",
            'content' => "\FA89",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-child-outline",
            'content' => "\F00F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-circle",
            'content' => "\F009",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-circle-outline",
            'content' => "\FB31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-clock",
            'content' => "\FB32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-clock-outline",
            'content' => "\FB33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-convert",
            'content' => "\F00A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-convert-outline",
            'content' => "\F032C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-details",
            'content' => "\F631",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-edit",
            'content' => "\F6BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-edit-outline",
            'content' => "\F001D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-group",
            'content' => "\F848",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-group-outline",
            'content' => "\FB34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-heart",
            'content' => "\F898",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-heart-outline",
            'content' => "\FBBF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-key",
            'content' => "\F00B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-key-outline",
            'content' => "\FBC0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-lock",
            'content' => "\F0189",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-lock-outline",
            'content' => "\F018A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-minus",
            'content' => "\F00D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-minus-outline",
            'content' => "\FAEB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple",
            'content' => "\F00E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-check",
            'content' => "\F8C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-check-outline",
            'content' => "\F0229",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-minus",
            'content' => "\F5D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-minus-outline",
            'content' => "\FBC1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-outline",
            'content' => "\F00F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-plus",
            'content' => "\F010",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-plus-outline",
            'content' => "\F7FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-remove",
            'content' => "\F0235",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-multiple-remove-outline",
            'content' => "\F0236",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-network",
            'content' => "\F011",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-network-outline",
            'content' => "\FBC2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-off",
            'content' => "\F012",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-off-outline",
            'content' => "\FBC3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-outline",
            'content' => "\F013",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-plus",
            'content' => "\F014",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-plus-outline",
            'content' => "\F800",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-question",
            'content' => "\FB35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-question-outline",
            'content' => "\FB36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-remove",
            'content' => "\F015",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-remove-outline",
            'content' => "\FAEC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-search",
            'content' => "\F016",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-search-outline",
            'content' => "\F934",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-settings",
            'content' => "\F630",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-settings-outline",
            'content' => "\F00F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-star",
            'content' => "\F017",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-star-outline",
            'content' => "\FBC4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-supervisor",
            'content' => "\FA8A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-supervisor-circle",
            'content' => "\FA8B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-supervisor-outline",
            'content' => "\F0158",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-switch",
            'content' => "\F019",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-tie",
            'content' => "\FCBF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-tie-outline",
            'content' => "\F00F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-tie-voice",
            'content' => "\F0333",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-tie-voice-off",
            'content' => "\F0335",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-tie-voice-off-outline",
            'content' => "\F0336",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "account-tie-voice-outline",
            'content' => "\F0334",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "accusoft",
            'content' => "\F849",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "adjust",
            'content' => "\F01A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "adobe",
            'content' => "\F935",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "adobe-acrobat",
            'content' => "\FFBD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "air-conditioner",
            'content' => "\F01B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "air-filter",
            'content' => "\FD1F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "air-horn",
            'content' => "\FD88",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "air-humidifier",
            'content' => "\F00C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "air-purifier",
            'content' => "\FD20",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airbag",
            'content' => "\FBC5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airballoon",
            'content' => "\F01C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airballoon-outline",
            'content' => "\F002D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airplane",
            'content' => "\F01D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airplane-landing",
            'content' => "\F5D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airplane-off",
            'content' => "\F01E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airplane-takeoff",
            'content' => "\F5D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airplay",
            'content' => "\F01F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "airport",
            'content' => "\F84A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm",
            'content' => "\F020",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-bell",
            'content' => "\F78D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-check",
            'content' => "\F021",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-light",
            'content' => "\F78E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-light-outline",
            'content' => "\FBC6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-multiple",
            'content' => "\F022",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-note",
            'content' => "\FE8E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-note-off",
            'content' => "\FE8F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-off",
            'content' => "\F023",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-plus",
            'content' => "\F024",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alarm-snooze",
            'content' => "\F68D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "album",
            'content' => "\F025",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert",
            'content' => "\F026",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-box",
            'content' => "\F027",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-box-outline",
            'content' => "\FCC0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-circle",
            'content' => "\F028",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-circle-check",
            'content' => "\F0218",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-circle-check-outline",
            'content' => "\F0219",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-circle-outline",
            'content' => "\F5D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-decagram",
            'content' => "\F6BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-decagram-outline",
            'content' => "\FCC1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-octagon",
            'content' => "\F029",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-octagon-outline",
            'content' => "\FCC2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-octagram",
            'content' => "\F766",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-octagram-outline",
            'content' => "\FCC3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-outline",
            'content' => "\F02A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-rhombus",
            'content' => "\F01F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alert-rhombus-outline",
            'content' => "\F01FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alien",
            'content' => "\F899",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alien-outline",
            'content' => "\F00F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "align-horizontal-center",
            'content' => "\F01EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "align-horizontal-left",
            'content' => "\F01ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "align-horizontal-right",
            'content' => "\F01EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "align-vertical-bottom",
            'content' => "\F01F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "align-vertical-center",
            'content' => "\F01F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "align-vertical-top",
            'content' => "\F01F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "all-inclusive",
            'content' => "\F6BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "allergy",
            'content' => "\F0283",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha",
            'content' => "\F02B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-a",
            'content' => "\41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-a-box",
            'content' => "\FAED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-a-box-outline",
            'content' => "\FBC7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-a-circle",
            'content' => "\FBC8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-a-circle-outline",
            'content' => "\FBC9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-b",
            'content' => "\42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-b-box",
            'content' => "\FAEE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-b-box-outline",
            'content' => "\FBCA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-b-circle",
            'content' => "\FBCB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-b-circle-outline",
            'content' => "\FBCC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-c",
            'content' => "\43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-c-box",
            'content' => "\FAEF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-c-box-outline",
            'content' => "\FBCD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-c-circle",
            'content' => "\FBCE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-c-circle-outline",
            'content' => "\FBCF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-d",
            'content' => "\44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-d-box",
            'content' => "\FAF0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-d-box-outline",
            'content' => "\FBD0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-d-circle",
            'content' => "\FBD1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-d-circle-outline",
            'content' => "\FBD2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-e",
            'content' => "\45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-e-box",
            'content' => "\FAF1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-e-box-outline",
            'content' => "\FBD3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-e-circle",
            'content' => "\FBD4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-e-circle-outline",
            'content' => "\FBD5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-f",
            'content' => "\46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-f-box",
            'content' => "\FAF2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-f-box-outline",
            'content' => "\FBD6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-f-circle",
            'content' => "\FBD7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-f-circle-outline",
            'content' => "\FBD8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-g",
            'content' => "\47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-g-box",
            'content' => "\FAF3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-g-box-outline",
            'content' => "\FBD9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-g-circle",
            'content' => "\FBDA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-g-circle-outline",
            'content' => "\FBDB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-h",
            'content' => "\48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-h-box",
            'content' => "\FAF4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-h-box-outline",
            'content' => "\FBDC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-h-circle",
            'content' => "\FBDD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-h-circle-outline",
            'content' => "\FBDE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-i",
            'content' => "\49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-i-box",
            'content' => "\FAF5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-i-box-outline",
            'content' => "\FBDF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-i-circle",
            'content' => "\FBE0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-i-circle-outline",
            'content' => "\FBE1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-j",
            'content' => "\4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-j-box",
            'content' => "\FAF6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-j-box-outline",
            'content' => "\FBE2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-j-circle",
            'content' => "\FBE3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-j-circle-outline",
            'content' => "\FBE4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-k",
            'content' => "\4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-k-box",
            'content' => "\FAF7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-k-box-outline",
            'content' => "\FBE5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-k-circle",
            'content' => "\FBE6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-k-circle-outline",
            'content' => "\FBE7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-l",
            'content' => "\4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-l-box",
            'content' => "\FAF8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-l-box-outline",
            'content' => "\FBE8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-l-circle",
            'content' => "\FBE9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-l-circle-outline",
            'content' => "\FBEA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-m",
            'content' => "\4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-m-box",
            'content' => "\FAF9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-m-box-outline",
            'content' => "\FBEB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-m-circle",
            'content' => "\FBEC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-m-circle-outline",
            'content' => "\FBED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-n",
            'content' => "\4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-n-box",
            'content' => "\FAFA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-n-box-outline",
            'content' => "\FBEE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-n-circle",
            'content' => "\FBEF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-n-circle-outline",
            'content' => "\FBF0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-o",
            'content' => "\4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-o-box",
            'content' => "\FAFB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-o-box-outline",
            'content' => "\FBF1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-o-circle",
            'content' => "\FBF2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-o-circle-outline",
            'content' => "\FBF3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-p",
            'content' => "\50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-p-box",
            'content' => "\FAFC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-p-box-outline",
            'content' => "\FBF4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-p-circle",
            'content' => "\FBF5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-p-circle-outline",
            'content' => "\FBF6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-q",
            'content' => "\51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-q-box",
            'content' => "\FAFD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-q-box-outline",
            'content' => "\FBF7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-q-circle",
            'content' => "\FBF8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-q-circle-outline",
            'content' => "\FBF9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-r",
            'content' => "\52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-r-box",
            'content' => "\FAFE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-r-box-outline",
            'content' => "\FBFA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-r-circle",
            'content' => "\FBFB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-r-circle-outline",
            'content' => "\FBFC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-s",
            'content' => "\53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-s-box",
            'content' => "\FAFF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-s-box-outline",
            'content' => "\FBFD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-s-circle",
            'content' => "\FBFE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-s-circle-outline",
            'content' => "\FBFF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-t",
            'content' => "\54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-t-box",
            'content' => "\FB00",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-t-box-outline",
            'content' => "\FC00",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-t-circle",
            'content' => "\FC01",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-t-circle-outline",
            'content' => "\FC02",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-u",
            'content' => "\55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-u-box",
            'content' => "\FB01",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-u-box-outline",
            'content' => "\FC03",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-u-circle",
            'content' => "\FC04",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-u-circle-outline",
            'content' => "\FC05",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-v",
            'content' => "\56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-v-box",
            'content' => "\FB02",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-v-box-outline",
            'content' => "\FC06",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-v-circle",
            'content' => "\FC07",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-v-circle-outline",
            'content' => "\FC08",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-w",
            'content' => "\57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-w-box",
            'content' => "\FB03",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-w-box-outline",
            'content' => "\FC09",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-w-circle",
            'content' => "\FC0A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-w-circle-outline",
            'content' => "\FC0B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-x",
            'content' => "\58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-x-box",
            'content' => "\FB04",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-x-box-outline",
            'content' => "\FC0C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-x-circle",
            'content' => "\FC0D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-x-circle-outline",
            'content' => "\FC0E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-y",
            'content' => "\59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-y-box",
            'content' => "\FB05",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-y-box-outline",
            'content' => "\FC0F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-y-circle",
            'content' => "\FC10",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-y-circle-outline",
            'content' => "\FC11",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-z",
            'content' => "\5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-z-box",
            'content' => "\FB06",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-z-box-outline",
            'content' => "\FC12",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-z-circle",
            'content' => "\FC13",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alpha-z-circle-outline",
            'content' => "\FC14",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alphabetical",
            'content' => "\F02C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alphabetical-off",
            'content' => "\F002E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alphabetical-variant",
            'content' => "\F002F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "alphabetical-variant-off",
            'content' => "\F0030",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "altimeter",
            'content' => "\F5D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "amazon",
            'content' => "\F02D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "amazon-alexa",
            'content' => "\F8C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "amazon-drive",
            'content' => "\F02E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ambulance",
            'content' => "\F02F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ammunition",
            'content' => "\FCC4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ampersand",
            'content' => "\FA8C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "amplifier",
            'content' => "\F030",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "amplifier-off",
            'content' => "\F01E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "anchor",
            'content' => "\F031",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "android",
            'content' => "\F032",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "android-auto",
            'content' => "\FA8D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "android-debug-bridge",
            'content' => "\F033",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "android-head",
            'content' => "\F78F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "android-messages",
            'content' => "\FD21",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "android-studio",
            'content' => "\F034",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "angle-acute",
            'content' => "\F936",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "angle-obtuse",
            'content' => "\F937",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "angle-right",
            'content' => "\F938",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "angular",
            'content' => "\F6B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "angularjs",
            'content' => "\F6BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "animation",
            'content' => "\F5D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "animation-outline",
            'content' => "\FA8E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "animation-play",
            'content' => "\F939",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "animation-play-outline",
            'content' => "\FA8F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ansible",
            'content' => "\F00C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "antenna",
            'content' => "\F0144",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "anvil",
            'content' => "\F89A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apache-kafka",
            'content' => "\F0031",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "api",
            'content' => "\F00C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "api-off",
            'content' => "\F0282",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple",
            'content' => "\F035",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-finder",
            'content' => "\F036",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-icloud",
            'content' => "\F038",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-ios",
            'content' => "\F037",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-keyboard-caps",
            'content' => "\F632",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-keyboard-command",
            'content' => "\F633",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-keyboard-control",
            'content' => "\F634",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-keyboard-option",
            'content' => "\F635",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-keyboard-shift",
            'content' => "\F636",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apple-safari",
            'content' => "\F039",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "application",
            'content' => "\F614",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "application-export",
            'content' => "\FD89",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "application-import",
            'content' => "\FD8A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "approximately-equal",
            'content' => "\FFBE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "approximately-equal-box",
            'content' => "\FFBF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apps",
            'content' => "\F03B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "apps-box",
            'content' => "\FD22",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arch",
            'content' => "\F8C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "archive",
            'content' => "\F03C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "archive-arrow-down",
            'content' => "\F0284",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "archive-arrow-down-outline",
            'content' => "\F0285",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "archive-arrow-up",
            'content' => "\F0286",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "archive-arrow-up-outline",
            'content' => "\F0287",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "archive-outline",
            'content' => "\F0239",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arm-flex",
            'content' => "\F008F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arm-flex-outline",
            'content' => "\F0090",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrange-bring-forward",
            'content' => "\F03D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrange-bring-to-front",
            'content' => "\F03E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrange-send-backward",
            'content' => "\F03F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrange-send-to-back",
            'content' => "\F040",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-all",
            'content' => "\F041",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-bottom-left",
            'content' => "\F042",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-bottom-left-bold-outline",
            'content' => "\F9B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-bottom-left-thick",
            'content' => "\F9B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-bottom-right",
            'content' => "\F043",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-bottom-right-bold-outline",
            'content' => "\F9B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-bottom-right-thick",
            'content' => "\F9B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse",
            'content' => "\F615",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-all",
            'content' => "\F044",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-down",
            'content' => "\F791",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-horizontal",
            'content' => "\F84B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-left",
            'content' => "\F792",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-right",
            'content' => "\F793",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-up",
            'content' => "\F794",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-collapse-vertical",
            'content' => "\F84C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-decision",
            'content' => "\F9BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-decision-auto",
            'content' => "\F9BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-decision-auto-outline",
            'content' => "\F9BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-decision-outline",
            'content' => "\F9BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down",
            'content' => "\F045",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold",
            'content' => "\F72D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold-box",
            'content' => "\F72E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold-box-outline",
            'content' => "\F72F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold-circle",
            'content' => "\F047",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold-circle-outline",
            'content' => "\F048",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold-hexagon-outline",
            'content' => "\F049",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-bold-outline",
            'content' => "\F9BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-box",
            'content' => "\F6BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-circle",
            'content' => "\FCB7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-circle-outline",
            'content' => "\FCB8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-drop-circle",
            'content' => "\F04A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-drop-circle-outline",
            'content' => "\F04B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-down-thick",
            'content' => "\F046",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand",
            'content' => "\F616",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-all",
            'content' => "\F04C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-down",
            'content' => "\F795",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-horizontal",
            'content' => "\F84D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-left",
            'content' => "\F796",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-right",
            'content' => "\F797",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-up",
            'content' => "\F798",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-expand-vertical",
            'content' => "\F84E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-horizontal-lock",
            'content' => "\F0186",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left",
            'content' => "\F04D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold",
            'content' => "\F730",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold-box",
            'content' => "\F731",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold-box-outline",
            'content' => "\F732",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold-circle",
            'content' => "\F04F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold-circle-outline",
            'content' => "\F050",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold-hexagon-outline",
            'content' => "\F051",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-bold-outline",
            'content' => "\F9BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-box",
            'content' => "\F6C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-circle",
            'content' => "\FCB9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-circle-outline",
            'content' => "\FCBA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-drop-circle",
            'content' => "\F052",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-drop-circle-outline",
            'content' => "\F053",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-right",
            'content' => "\FE90",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-right-bold",
            'content' => "\FE91",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-right-bold-outline",
            'content' => "\F9C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-left-thick",
            'content' => "\F04E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right",
            'content' => "\F054",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold",
            'content' => "\F733",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold-box",
            'content' => "\F734",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold-box-outline",
            'content' => "\F735",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold-circle",
            'content' => "\F056",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold-circle-outline",
            'content' => "\F057",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold-hexagon-outline",
            'content' => "\F058",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-bold-outline",
            'content' => "\F9C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-box",
            'content' => "\F6C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-circle",
            'content' => "\FCBB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-circle-outline",
            'content' => "\FCBC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-drop-circle",
            'content' => "\F059",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-drop-circle-outline",
            'content' => "\F05A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-right-thick",
            'content' => "\F055",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-split-horizontal",
            'content' => "\F93A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-split-vertical",
            'content' => "\F93B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-left",
            'content' => "\F05B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-left-bold-outline",
            'content' => "\F9C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-left-bottom-right",
            'content' => "\FE92",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-left-bottom-right-bold",
            'content' => "\FE93",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-left-thick",
            'content' => "\F9C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-right",
            'content' => "\F05C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-right-bold-outline",
            'content' => "\F9C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-right-bottom-left",
            'content' => "\FE94",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-right-bottom-left-bold",
            'content' => "\FE95",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-top-right-thick",
            'content' => "\F9C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up",
            'content' => "\F05D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold",
            'content' => "\F736",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold-box",
            'content' => "\F737",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold-box-outline",
            'content' => "\F738",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold-circle",
            'content' => "\F05F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold-circle-outline",
            'content' => "\F060",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold-hexagon-outline",
            'content' => "\F061",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-bold-outline",
            'content' => "\F9C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-box",
            'content' => "\F6C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-circle",
            'content' => "\FCBD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-circle-outline",
            'content' => "\FCBE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-down",
            'content' => "\FE96",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-down-bold",
            'content' => "\FE97",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-down-bold-outline",
            'content' => "\F9C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-drop-circle",
            'content' => "\F062",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-drop-circle-outline",
            'content' => "\F063",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-up-thick",
            'content' => "\F05E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "arrow-vertical-lock",
            'content' => "\F0187",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "artist",
            'content' => "\F802",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "artist-outline",
            'content' => "\FCC5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "artstation",
            'content' => "\FB37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "aspect-ratio",
            'content' => "\FA23",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "assistant",
            'content' => "\F064",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "asterisk",
            'content' => "\F6C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "at",
            'content' => "\F065",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "atlassian",
            'content' => "\F803",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "atm",
            'content' => "\FD23",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "atom",
            'content' => "\F767",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "atom-variant",
            'content' => "\FE98",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "attachment",
            'content' => "\F066",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "audio-video",
            'content' => "\F93C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "audio-video-off",
            'content' => "\F01E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "audiobook",
            'content' => "\F067",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "augmented-reality",
            'content' => "\F84F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "auto-fix",
            'content' => "\F068",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "auto-upload",
            'content' => "\F069",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "autorenew",
            'content' => "\F06A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "av-timer",
            'content' => "\F06B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "aws",
            'content' => "\FDF2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axe",
            'content' => "\F8C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis",
            'content' => "\FD24",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-arrow",
            'content' => "\FD25",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-arrow-lock",
            'content' => "\FD26",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-lock",
            'content' => "\FD27",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-x-arrow",
            'content' => "\FD28",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-x-arrow-lock",
            'content' => "\FD29",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-x-rotate-clockwise",
            'content' => "\FD2A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-x-rotate-counterclockwise",
            'content' => "\FD2B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-x-y-arrow-lock",
            'content' => "\FD2C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-y-arrow",
            'content' => "\FD2D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-y-arrow-lock",
            'content' => "\FD2E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-y-rotate-clockwise",
            'content' => "\FD2F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-y-rotate-counterclockwise",
            'content' => "\FD30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-z-arrow",
            'content' => "\FD31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-z-arrow-lock",
            'content' => "\FD32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-z-rotate-clockwise",
            'content' => "\FD33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "axis-z-rotate-counterclockwise",
            'content' => "\FD34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "azure",
            'content' => "\F804",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "azure-devops",
            'content' => "\F0091",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "babel",
            'content' => "\FA24",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby",
            'content' => "\F06C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby-bottle",
            'content' => "\FF56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby-bottle-outline",
            'content' => "\FF57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby-carriage",
            'content' => "\F68E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby-carriage-off",
            'content' => "\FFC0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby-face",
            'content' => "\FE99",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baby-face-outline",
            'content' => "\FE9A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "backburger",
            'content' => "\F06D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "backspace",
            'content' => "\F06E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "backspace-outline",
            'content' => "\FB38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "backspace-reverse",
            'content' => "\FE9B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "backspace-reverse-outline",
            'content' => "\FE9C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "backup-restore",
            'content' => "\F06F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bacteria",
            'content' => "\FEF2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bacteria-outline",
            'content' => "\FEF3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "badminton",
            'content' => "\F850",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-carry-on",
            'content' => "\FF58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-carry-on-check",
            'content' => "\FD41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-carry-on-off",
            'content' => "\FF59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-checked",
            'content' => "\FF5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-personal",
            'content' => "\FDF3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-personal-off",
            'content' => "\FDF4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-personal-off-outline",
            'content' => "\FDF5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bag-personal-outline",
            'content' => "\FDF6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baguette",
            'content' => "\FF5B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "balloon",
            'content' => "\FA25",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ballot",
            'content' => "\F9C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ballot-outline",
            'content' => "\F9C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ballot-recount",
            'content' => "\FC15",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ballot-recount-outline",
            'content' => "\FC16",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bandage",
            'content' => "\FD8B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bandcamp",
            'content' => "\F674",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank",
            'content' => "\F070",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-minus",
            'content' => "\FD8C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-outline",
            'content' => "\FE9D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-plus",
            'content' => "\FD8D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-remove",
            'content' => "\FD8E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-transfer",
            'content' => "\FA26",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-transfer-in",
            'content' => "\FA27",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bank-transfer-out",
            'content' => "\FA28",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barcode",
            'content' => "\F071",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barcode-off",
            'content' => "\F0261",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barcode-scan",
            'content' => "\F072",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barley",
            'content' => "\F073",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barley-off",
            'content' => "\FB39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barn",
            'content' => "\FB3A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "barrel",
            'content' => "\F074",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baseball",
            'content' => "\F851",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "baseball-bat",
            'content' => "\F852",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basecamp",
            'content' => "\F075",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bash",
            'content' => "\F01AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basket",
            'content' => "\F076",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basket-fill",
            'content' => "\F077",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basket-outline",
            'content' => "\F01AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basket-unfill",
            'content' => "\F078",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basketball",
            'content' => "\F805",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basketball-hoop",
            'content' => "\FC17",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "basketball-hoop-outline",
            'content' => "\FC18",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bat",
            'content' => "\FB3B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery",
            'content' => "\F079",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-10",
            'content' => "\F07A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-10-bluetooth",
            'content' => "\F93D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-20",
            'content' => "\F07B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-20-bluetooth",
            'content' => "\F93E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-30",
            'content' => "\F07C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-30-bluetooth",
            'content' => "\F93F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-40",
            'content' => "\F07D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-40-bluetooth",
            'content' => "\F940",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-50",
            'content' => "\F07E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-50-bluetooth",
            'content' => "\F941",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-60",
            'content' => "\F07F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-60-bluetooth",
            'content' => "\F942",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-70",
            'content' => "\F080",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-70-bluetooth",
            'content' => "\F943",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-80",
            'content' => "\F081",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-80-bluetooth",
            'content' => "\F944",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-90",
            'content' => "\F082",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-90-bluetooth",
            'content' => "\F945",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-alert",
            'content' => "\F083",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-alert-bluetooth",
            'content' => "\F946",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-alert-variant",
            'content' => "\F00F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-alert-variant-outline",
            'content' => "\F00F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-bluetooth",
            'content' => "\F947",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-bluetooth-variant",
            'content' => "\F948",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging",
            'content' => "\F084",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-10",
            'content' => "\F89B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-100",
            'content' => "\F085",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-20",
            'content' => "\F086",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-30",
            'content' => "\F087",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-40",
            'content' => "\F088",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-50",
            'content' => "\F89C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-60",
            'content' => "\F089",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-70",
            'content' => "\F89D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-80",
            'content' => "\F08A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-90",
            'content' => "\F08B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-high",
            'content' => "\F02D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-low",
            'content' => "\F02CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-medium",
            'content' => "\F02D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-outline",
            'content' => "\F89E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless",
            'content' => "\F806",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-10",
            'content' => "\F807",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-20",
            'content' => "\F808",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-30",
            'content' => "\F809",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-40",
            'content' => "\F80A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-50",
            'content' => "\F80B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-60",
            'content' => "\F80C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-70",
            'content' => "\F80D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-80",
            'content' => "\F80E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-90",
            'content' => "\F80F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-alert",
            'content' => "\F810",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-charging-wireless-outline",
            'content' => "\F811",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-heart",
            'content' => "\F023A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-heart-outline",
            'content' => "\F023B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-heart-variant",
            'content' => "\F023C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-high",
            'content' => "\F02CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-low",
            'content' => "\F02CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-medium",
            'content' => "\F02CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-minus",
            'content' => "\F08C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-negative",
            'content' => "\F08D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-off",
            'content' => "\F0288",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-off-outline",
            'content' => "\F0289",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-outline",
            'content' => "\F08E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-plus",
            'content' => "\F08F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-positive",
            'content' => "\F090",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-unknown",
            'content' => "\F091",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battery-unknown-bluetooth",
            'content' => "\F949",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "battlenet",
            'content' => "\FB3C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beach",
            'content' => "\F092",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker",
            'content' => "\FCC6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-alert",
            'content' => "\F0254",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-alert-outline",
            'content' => "\F0255",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-check",
            'content' => "\F0256",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-check-outline",
            'content' => "\F0257",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-minus",
            'content' => "\F0258",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-minus-outline",
            'content' => "\F0259",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-outline",
            'content' => "\F68F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-plus",
            'content' => "\F025A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-plus-outline",
            'content' => "\F025B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-question",
            'content' => "\F025C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-question-outline",
            'content' => "\F025D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-remove",
            'content' => "\F025E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beaker-remove-outline",
            'content' => "\F025F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beats",
            'content' => "\F097",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-double",
            'content' => "\F0092",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-double-outline",
            'content' => "\F0093",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-empty",
            'content' => "\F89F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-king",
            'content' => "\F0094",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-king-outline",
            'content' => "\F0095",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-queen",
            'content' => "\F0096",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-queen-outline",
            'content' => "\F0097",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-single",
            'content' => "\F0098",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bed-single-outline",
            'content' => "\F0099",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bee",
            'content' => "\FFC1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bee-flower",
            'content' => "\FFC2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beehive-outline",
            'content' => "\F00F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beer",
            'content' => "\F098",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beer-outline",
            'content' => "\F0337",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "behance",
            'content' => "\F099",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell",
            'content' => "\F09A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-alert",
            'content' => "\FD35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-alert-outline",
            'content' => "\FE9E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-check",
            'content' => "\F0210",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-check-outline",
            'content' => "\F0211",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-circle",
            'content' => "\FD36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-circle-outline",
            'content' => "\FD37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-off",
            'content' => "\F09B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-off-outline",
            'content' => "\FA90",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-outline",
            'content' => "\F09C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-plus",
            'content' => "\F09D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-plus-outline",
            'content' => "\FA91",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-ring",
            'content' => "\F09E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-ring-outline",
            'content' => "\F09F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-sleep",
            'content' => "\F0A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bell-sleep-outline",
            'content' => "\FA92",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "beta",
            'content' => "\F0A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "betamax",
            'content' => "\F9CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "biathlon",
            'content' => "\FDF7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bible",
            'content' => "\F0A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bicycle",
            'content' => "\F00C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bicycle-basket",
            'content' => "\F0260",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bike",
            'content' => "\F0A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bike-fast",
            'content' => "\F014A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "billboard",
            'content' => "\F0032",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "billiards",
            'content' => "\FB3D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "billiards-rack",
            'content' => "\FB3E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bing",
            'content' => "\F0A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "binoculars",
            'content' => "\F0A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bio",
            'content' => "\F0A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "biohazard",
            'content' => "\F0A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bitbucket",
            'content' => "\F0A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bitcoin",
            'content' => "\F812",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "black-mesa",
            'content' => "\F0A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blackberry",
            'content' => "\F0AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blender",
            'content' => "\FCC7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blender-software",
            'content' => "\F0AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blinds",
            'content' => "\F0AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blinds-open",
            'content' => "\F0033",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "block-helper",
            'content' => "\F0AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blogger",
            'content' => "\F0AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blood-bag",
            'content' => "\FCC8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bluetooth",
            'content' => "\F0AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bluetooth-audio",
            'content' => "\F0B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bluetooth-connect",
            'content' => "\F0B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bluetooth-off",
            'content' => "\F0B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bluetooth-settings",
            'content' => "\F0B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bluetooth-transfer",
            'content' => "\F0B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blur",
            'content' => "\F0B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blur-linear",
            'content' => "\F0B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blur-off",
            'content' => "\F0B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blur-radial",
            'content' => "\F0B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bolnisi-cross",
            'content' => "\FCC9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bolt",
            'content' => "\FD8F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bomb",
            'content' => "\F690",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bomb-off",
            'content' => "\F6C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bone",
            'content' => "\F0B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book",
            'content' => "\F0BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-information-variant",
            'content' => "\F009A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-lock",
            'content' => "\F799",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-lock-open",
            'content' => "\F79A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-minus",
            'content' => "\F5D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-minus-multiple",
            'content' => "\FA93",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-multiple",
            'content' => "\F0BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-open",
            'content' => "\F0BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-open-outline",
            'content' => "\FB3F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-open-page-variant",
            'content' => "\F5DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-open-variant",
            'content' => "\F0BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-outline",
            'content' => "\FB40",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-play",
            'content' => "\FE9F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-play-outline",
            'content' => "\FEA0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-plus",
            'content' => "\F5DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-plus-multiple",
            'content' => "\FA94",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-remove",
            'content' => "\FA96",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-remove-multiple",
            'content' => "\FA95",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-search",
            'content' => "\FEA1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-search-outline",
            'content' => "\FEA2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-variant",
            'content' => "\F0BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "book-variant-multiple",
            'content' => "\F0BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark",
            'content' => "\F0C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-check",
            'content' => "\F0C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-minus",
            'content' => "\F9CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-minus-outline",
            'content' => "\F9CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-multiple",
            'content' => "\FDF8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-multiple-outline",
            'content' => "\FDF9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-music",
            'content' => "\F0C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-off",
            'content' => "\F9CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-off-outline",
            'content' => "\F9CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-outline",
            'content' => "\F0C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-plus",
            'content' => "\F0C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-plus-outline",
            'content' => "\F0C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookmark-remove",
            'content' => "\F0C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bookshelf",
            'content' => "\F028A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate",
            'content' => "\FEA3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-alert",
            'content' => "\FEA4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-alert-outline",
            'content' => "\FEA5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-down",
            'content' => "\FEA6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-down-outline",
            'content' => "\FEA7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-outline",
            'content' => "\FEA8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-up",
            'content' => "\FEA9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boom-gate-up-outline",
            'content' => "\FEAA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boombox",
            'content' => "\F5DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boomerang",
            'content' => "\F00FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bootstrap",
            'content' => "\F6C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-all",
            'content' => "\F0C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-all-variant",
            'content' => "\F8A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-bottom",
            'content' => "\F0C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-bottom-variant",
            'content' => "\F8A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-color",
            'content' => "\F0C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-horizontal",
            'content' => "\F0CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-inside",
            'content' => "\F0CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-left",
            'content' => "\F0CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-left-variant",
            'content' => "\F8A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-none",
            'content' => "\F0CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-none-variant",
            'content' => "\F8A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-outside",
            'content' => "\F0CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-right",
            'content' => "\F0CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-right-variant",
            'content' => "\F8A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-style",
            'content' => "\F0D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-top",
            'content' => "\F0D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-top-variant",
            'content' => "\F8A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "border-vertical",
            'content' => "\F0D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-soda",
            'content' => "\F009B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-soda-classic",
            'content' => "\F009C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-soda-outline",
            'content' => "\F009D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-tonic",
            'content' => "\F0159",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-tonic-outline",
            'content' => "\F015A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-tonic-plus",
            'content' => "\F015B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-tonic-plus-outline",
            'content' => "\F015C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-tonic-skull",
            'content' => "\F015D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-tonic-skull-outline",
            'content' => "\F015E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-wine",
            'content' => "\F853",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bottle-wine-outline",
            'content' => "\F033B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bow-tie",
            'content' => "\F677",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bowl",
            'content' => "\F617",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bowling",
            'content' => "\F0D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "box",
            'content' => "\F0D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "box-cutter",
            'content' => "\F0D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "box-shadow",
            'content' => "\F637",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "boxing-glove",
            'content' => "\FB41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "braille",
            'content' => "\F9CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brain",
            'content' => "\F9D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bread-slice",
            'content' => "\FCCA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bread-slice-outline",
            'content' => "\FCCB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bridge",
            'content' => "\F618",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase",
            'content' => "\F0D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-account",
            'content' => "\FCCC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-account-outline",
            'content' => "\FCCD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-check",
            'content' => "\F0D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-check-outline",
            'content' => "\F0349",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-clock",
            'content' => "\F00FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-clock-outline",
            'content' => "\F00FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-download",
            'content' => "\F0D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-download-outline",
            'content' => "\FC19",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-edit",
            'content' => "\FA97",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-edit-outline",
            'content' => "\FC1A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-minus",
            'content' => "\FA29",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-minus-outline",
            'content' => "\FC1B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-outline",
            'content' => "\F813",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-plus",
            'content' => "\FA2A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-plus-outline",
            'content' => "\FC1C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-remove",
            'content' => "\FA2B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-remove-outline",
            'content' => "\FC1D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-search",
            'content' => "\FA2C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-search-outline",
            'content' => "\FC1E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-upload",
            'content' => "\F0D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "briefcase-upload-outline",
            'content' => "\FC1F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-1",
            'content' => "\F0DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-2",
            'content' => "\F0DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-3",
            'content' => "\F0DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-4",
            'content' => "\F0DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-5",
            'content' => "\F0DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-6",
            'content' => "\F0DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-7",
            'content' => "\F0E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-auto",
            'content' => "\F0E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brightness-percent",
            'content' => "\FCCE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "broom",
            'content' => "\F0E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "brush",
            'content' => "\F0E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "buddhism",
            'content' => "\F94A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "buffer",
            'content' => "\F619",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bug",
            'content' => "\F0E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bug-check",
            'content' => "\FA2D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bug-check-outline",
            'content' => "\FA2E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bug-outline",
            'content' => "\FA2F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bugle",
            'content' => "\FD90",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bulldozer",
            'content' => "\FB07",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bullet",
            'content' => "\FCCF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bulletin-board",
            'content' => "\F0E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bullhorn",
            'content' => "\F0E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bullhorn-outline",
            'content' => "\FB08",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bullseye",
            'content' => "\F5DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bullseye-arrow",
            'content' => "\F8C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bulma",
            'content' => "\F0312",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bunk-bed",
            'content' => "\F032D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus",
            'content' => "\F0E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-alert",
            'content' => "\FA98",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-articulated-end",
            'content' => "\F79B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-articulated-front",
            'content' => "\F79C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-clock",
            'content' => "\F8C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-double-decker",
            'content' => "\F79D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-marker",
            'content' => "\F023D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-multiple",
            'content' => "\FF5C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-school",
            'content' => "\F79E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-side",
            'content' => "\F79F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-stop",
            'content' => "\F0034",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-stop-covered",
            'content' => "\F0035",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "bus-stop-uncovered",
            'content' => "\F0036",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cached",
            'content' => "\F0E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cactus",
            'content' => "\FD91",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cake",
            'content' => "\F0E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cake-layered",
            'content' => "\F0EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cake-variant",
            'content' => "\F0EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calculator",
            'content' => "\F0EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calculator-variant",
            'content' => "\FA99",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar",
            'content' => "\F0ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-account",
            'content' => "\FEF4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-account-outline",
            'content' => "\FEF5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-alert",
            'content' => "\FA30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-arrow-left",
            'content' => "\F015F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-arrow-right",
            'content' => "\F0160",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-blank",
            'content' => "\F0EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-blank-multiple",
            'content' => "\F009E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-blank-outline",
            'content' => "\FB42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-check",
            'content' => "\F0EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-check-outline",
            'content' => "\FC20",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-clock",
            'content' => "\F0F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-edit",
            'content' => "\F8A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-export",
            'content' => "\FB09",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-heart",
            'content' => "\F9D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-import",
            'content' => "\FB0A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-minus",
            'content' => "\FD38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-month",
            'content' => "\FDFA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-month-outline",
            'content' => "\FDFB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-multiple",
            'content' => "\F0F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-multiple-check",
            'content' => "\F0F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-multiselect",
            'content' => "\FA31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-outline",
            'content' => "\FB43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-plus",
            'content' => "\F0F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-question",
            'content' => "\F691",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-range",
            'content' => "\F678",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-range-outline",
            'content' => "\FB44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-remove",
            'content' => "\F0F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-remove-outline",
            'content' => "\FC21",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-repeat",
            'content' => "\FEAB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-repeat-outline",
            'content' => "\FEAC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-search",
            'content' => "\F94B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-star",
            'content' => "\F9D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-text",
            'content' => "\F0F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-text-outline",
            'content' => "\FC22",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-today",
            'content' => "\F0F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-week",
            'content' => "\FA32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-week-begin",
            'content' => "\FA33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-weekend",
            'content' => "\FEF6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "calendar-weekend-outline",
            'content' => "\FEF7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "call-made",
            'content' => "\F0F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "call-merge",
            'content' => "\F0F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "call-missed",
            'content' => "\F0F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "call-received",
            'content' => "\F0FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "call-split",
            'content' => "\F0FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camcorder",
            'content' => "\F0FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camcorder-box",
            'content' => "\F0FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camcorder-box-off",
            'content' => "\F0FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camcorder-off",
            'content' => "\F0FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera",
            'content' => "\F100",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-account",
            'content' => "\F8CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-burst",
            'content' => "\F692",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-control",
            'content' => "\FB45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-enhance",
            'content' => "\F101",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-enhance-outline",
            'content' => "\FB46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-front",
            'content' => "\F102",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-front-variant",
            'content' => "\F103",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-gopro",
            'content' => "\F7A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-image",
            'content' => "\F8CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-iris",
            'content' => "\F104",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-metering-center",
            'content' => "\F7A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-metering-matrix",
            'content' => "\F7A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-metering-partial",
            'content' => "\F7A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-metering-spot",
            'content' => "\F7A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-off",
            'content' => "\F5DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-outline",
            'content' => "\FD39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-party-mode",
            'content' => "\F105",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-plus",
            'content' => "\FEF8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-plus-outline",
            'content' => "\FEF9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-rear",
            'content' => "\F106",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-rear-variant",
            'content' => "\F107",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-retake",
            'content' => "\FDFC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-retake-outline",
            'content' => "\FDFD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-switch",
            'content' => "\F108",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-timer",
            'content' => "\F109",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-wireless",
            'content' => "\FD92",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "camera-wireless-outline",
            'content' => "\FD93",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "campfire",
            'content' => "\FEFA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cancel",
            'content' => "\F739",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "candle",
            'content' => "\F5E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "candycane",
            'content' => "\F10A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cannabis",
            'content' => "\F7A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "caps-lock",
            'content' => "\FA9A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car",
            'content' => "\F10B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-2-plus",
            'content' => "\F0037",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-3-plus",
            'content' => "\F0038",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-back",
            'content' => "\FDFE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-battery",
            'content' => "\F10C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-brake-abs",
            'content' => "\FC23",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-brake-alert",
            'content' => "\FC24",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-brake-hold",
            'content' => "\FD3A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-brake-parking",
            'content' => "\FD3B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-brake-retarder",
            'content' => "\F0039",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-child-seat",
            'content' => "\FFC3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-clutch",
            'content' => "\F003A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-connected",
            'content' => "\F10D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-convertible",
            'content' => "\F7A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-coolant-level",
            'content' => "\F003B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-cruise-control",
            'content' => "\FD3C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-defrost-front",
            'content' => "\FD3D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-defrost-rear",
            'content' => "\FD3E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-door",
            'content' => "\FB47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-door-lock",
            'content' => "\F00C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-electric",
            'content' => "\FB48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-esp",
            'content' => "\FC25",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-estate",
            'content' => "\F7A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-hatchback",
            'content' => "\F7A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-info",
            'content' => "\F01E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-key",
            'content' => "\FB49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-light-dimmed",
            'content' => "\FC26",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-light-fog",
            'content' => "\FC27",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-light-high",
            'content' => "\FC28",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-limousine",
            'content' => "\F8CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-multiple",
            'content' => "\FB4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-off",
            'content' => "\FDFF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-parking-lights",
            'content' => "\FD3F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-pickup",
            'content' => "\F7A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-seat",
            'content' => "\FFC4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-seat-cooler",
            'content' => "\FFC5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-seat-heater",
            'content' => "\FFC6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-shift-pattern",
            'content' => "\FF5D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-side",
            'content' => "\F7AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-sports",
            'content' => "\F7AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-tire-alert",
            'content' => "\FC29",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-traction-control",
            'content' => "\FD40",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-turbocharger",
            'content' => "\F003C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-wash",
            'content' => "\F10E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-windshield",
            'content' => "\F003D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "car-windshield-outline",
            'content' => "\F003E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "caravan",
            'content' => "\F7AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card",
            'content' => "\FB4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-bulleted",
            'content' => "\FB4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-bulleted-off",
            'content' => "\FB4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-bulleted-off-outline",
            'content' => "\FB4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-bulleted-outline",
            'content' => "\FB4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-bulleted-settings",
            'content' => "\FB50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-bulleted-settings-outline",
            'content' => "\FB51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-outline",
            'content' => "\FB52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-plus",
            'content' => "\F022A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-plus-outline",
            'content' => "\F022B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-search",
            'content' => "\F009F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-search-outline",
            'content' => "\F00A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-text",
            'content' => "\FB53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "card-text-outline",
            'content' => "\FB54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards",
            'content' => "\F638",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-club",
            'content' => "\F8CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-diamond",
            'content' => "\F8CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-diamond-outline",
            'content' => "\F003F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-heart",
            'content' => "\F8CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-outline",
            'content' => "\F639",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-playing-outline",
            'content' => "\F63A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-spade",
            'content' => "\F8D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cards-variant",
            'content' => "\F6C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "carrot",
            'content' => "\F10F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart",
            'content' => "\F110",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-arrow-down",
            'content' => "\FD42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-arrow-right",
            'content' => "\FC2A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-arrow-up",
            'content' => "\FD43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-minus",
            'content' => "\FD44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-off",
            'content' => "\F66B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-outline",
            'content' => "\F111",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-plus",
            'content' => "\F112",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cart-remove",
            'content' => "\FD45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "case-sensitive-alt",
            'content' => "\F113",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash",
            'content' => "\F114",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-100",
            'content' => "\F115",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-marker",
            'content' => "\FD94",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-minus",
            'content' => "\F028B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-multiple",
            'content' => "\F116",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-plus",
            'content' => "\F028C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-refund",
            'content' => "\FA9B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-register",
            'content' => "\FCD0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-remove",
            'content' => "\F028D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-usd",
            'content' => "\F01A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cash-usd-outline",
            'content' => "\F117",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cassette",
            'content' => "\F9D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cast",
            'content' => "\F118",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cast-audio",
            'content' => "\F0040",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cast-connected",
            'content' => "\F119",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cast-education",
            'content' => "\FE6D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cast-off",
            'content' => "\F789",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "castle",
            'content' => "\F11A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cat",
            'content' => "\F11B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cctv",
            'content' => "\F7AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ceiling-light",
            'content' => "\F768",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone",
            'content' => "\F11C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-android",
            'content' => "\F11D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-arrow-down",
            'content' => "\F9D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-basic",
            'content' => "\F11E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-dock",
            'content' => "\F11F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-erase",
            'content' => "\F94C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-information",
            'content' => "\FF5E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-iphone",
            'content' => "\F120",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-key",
            'content' => "\F94D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-link",
            'content' => "\F121",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-link-off",
            'content' => "\F122",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-lock",
            'content' => "\F94E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-message",
            'content' => "\F8D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-message-off",
            'content' => "\F00FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-nfc",
            'content' => "\FEAD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-nfc-off",
            'content' => "\F0303",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-off",
            'content' => "\F94F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-play",
            'content' => "\F0041",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-screenshot",
            'content' => "\FA34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-settings",
            'content' => "\F123",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-settings-variant",
            'content' => "\F950",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-sound",
            'content' => "\F951",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-text",
            'content' => "\F8D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cellphone-wireless",
            'content' => "\F814",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "celtic-cross",
            'content' => "\FCD1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "centos",
            'content' => "\F0145",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "certificate",
            'content' => "\F124",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "certificate-outline",
            'content' => "\F01B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chair-rolling",
            'content' => "\FFBA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chair-school",
            'content' => "\F125",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "charity",
            'content' => "\FC2B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-arc",
            'content' => "\F126",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-areaspline",
            'content' => "\F127",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-areaspline-variant",
            'content' => "\FEAE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-bar",
            'content' => "\F128",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-bar-stacked",
            'content' => "\F769",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-bell-curve",
            'content' => "\FC2C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-bell-curve-cumulative",
            'content' => "\FFC7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-bubble",
            'content' => "\F5E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-donut",
            'content' => "\F7AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-donut-variant",
            'content' => "\F7AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-gantt",
            'content' => "\F66C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-histogram",
            'content' => "\F129",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-line",
            'content' => "\F12A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-line-stacked",
            'content' => "\F76A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-line-variant",
            'content' => "\F7B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-multiline",
            'content' => "\F8D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-multiple",
            'content' => "\F023E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-pie",
            'content' => "\F12B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-scatter-plot",
            'content' => "\FEAF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-scatter-plot-hexbin",
            'content' => "\F66D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-snakey",
            'content' => "\F020A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-snakey-variant",
            'content' => "\F020B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-timeline",
            'content' => "\F66E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-timeline-variant",
            'content' => "\FEB0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chart-tree",
            'content' => "\FEB1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat",
            'content' => "\FB55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-alert",
            'content' => "\FB56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-alert-outline",
            'content' => "\F02F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-outline",
            'content' => "\FEFB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-processing",
            'content' => "\FB57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-processing-outline",
            'content' => "\F02F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-sleep",
            'content' => "\F02FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chat-sleep-outline",
            'content' => "\F02FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check",
            'content' => "\F12C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-all",
            'content' => "\F12D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-bold",
            'content' => "\FE6E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-box-multiple-outline",
            'content' => "\FC2D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-box-outline",
            'content' => "\FC2E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-circle",
            'content' => "\F5E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-circle-outline",
            'content' => "\F5E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-decagram",
            'content' => "\F790",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-network",
            'content' => "\FC2F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-network-outline",
            'content' => "\FC30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-outline",
            'content' => "\F854",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-underline",
            'content' => "\FE70",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-underline-circle",
            'content' => "\FE71",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "check-underline-circle-outline",
            'content' => "\FE72",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbook",
            'content' => "\FA9C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-blank",
            'content' => "\F12E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-blank-circle",
            'content' => "\F12F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-blank-circle-outline",
            'content' => "\F130",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-blank-off",
            'content' => "\F0317",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-blank-off-outline",
            'content' => "\F0318",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-blank-outline",
            'content' => "\F131",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-intermediate",
            'content' => "\F855",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-marked",
            'content' => "\F132",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-marked-circle",
            'content' => "\F133",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-marked-circle-outline",
            'content' => "\F134",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-marked-outline",
            'content' => "\F135",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-blank",
            'content' => "\F136",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-blank-circle",
            'content' => "\F63B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-blank-circle-outline",
            'content' => "\F63C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-blank-outline",
            'content' => "\F137",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-marked",
            'content' => "\F138",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-marked-circle",
            'content' => "\F63D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-marked-circle-outline",
            'content' => "\F63E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkbox-multiple-marked-outline",
            'content' => "\F139",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkerboard",
            'content' => "\F13A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkerboard-minus",
            'content' => "\F022D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkerboard-plus",
            'content' => "\F022C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "checkerboard-remove",
            'content' => "\F022E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cheese",
            'content' => "\F02E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chef-hat",
            'content' => "\FB58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chemical-weapon",
            'content' => "\F13B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chess-bishop",
            'content' => "\F85B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chess-king",
            'content' => "\F856",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chess-knight",
            'content' => "\F857",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chess-pawn",
            'content' => "\F858",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chess-queen",
            'content' => "\F859",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chess-rook",
            'content' => "\F85A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-double-down",
            'content' => "\F13C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-double-left",
            'content' => "\F13D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-double-right",
            'content' => "\F13E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-double-up",
            'content' => "\F13F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-down",
            'content' => "\F140",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-down-box",
            'content' => "\F9D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-down-box-outline",
            'content' => "\F9D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-down-circle",
            'content' => "\FB0B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-down-circle-outline",
            'content' => "\FB0C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-left",
            'content' => "\F141",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-left-box",
            'content' => "\F9D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-left-box-outline",
            'content' => "\F9D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-left-circle",
            'content' => "\FB0D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-left-circle-outline",
            'content' => "\FB0E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-right",
            'content' => "\F142",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-right-box",
            'content' => "\F9D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-right-box-outline",
            'content' => "\F9DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-right-circle",
            'content' => "\FB0F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-right-circle-outline",
            'content' => "\FB10",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-triple-down",
            'content' => "\FD95",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-triple-left",
            'content' => "\FD96",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-triple-right",
            'content' => "\FD97",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-triple-up",
            'content' => "\FD98",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-up",
            'content' => "\F143",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-up-box",
            'content' => "\F9DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-up-box-outline",
            'content' => "\F9DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-up-circle",
            'content' => "\FB11",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chevron-up-circle-outline",
            'content' => "\FB12",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chili-hot",
            'content' => "\F7B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chili-medium",
            'content' => "\F7B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chili-mild",
            'content' => "\F7B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "chip",
            'content' => "\F61A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "christianity",
            'content' => "\F952",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "christianity-outline",
            'content' => "\FCD2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "church",
            'content' => "\F144",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cigar",
            'content' => "\F01B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle",
            'content' => "\F764",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-double",
            'content' => "\FEB2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-edit-outline",
            'content' => "\F8D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-expand",
            'content' => "\FEB3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-medium",
            'content' => "\F9DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-off-outline",
            'content' => "\F00FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-outline",
            'content' => "\F765",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-1",
            'content' => "\FA9D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-2",
            'content' => "\FA9E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-3",
            'content' => "\FA9F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-4",
            'content' => "\FAA0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-5",
            'content' => "\FAA1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-6",
            'content' => "\FAA2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-7",
            'content' => "\FAA3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-slice-8",
            'content' => "\FAA4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circle-small",
            'content' => "\F9DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "circular-saw",
            'content' => "\FE73",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cisco-webex",
            'content' => "\F145",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "city",
            'content' => "\F146",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "city-variant",
            'content' => "\FA35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "city-variant-outline",
            'content' => "\FA36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard",
            'content' => "\F147",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-account",
            'content' => "\F148",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-account-outline",
            'content' => "\FC31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-alert",
            'content' => "\F149",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-alert-outline",
            'content' => "\FCD3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-down",
            'content' => "\F14A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-down-outline",
            'content' => "\FC32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-left",
            'content' => "\F14B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-left-outline",
            'content' => "\FCD4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-right",
            'content' => "\FCD5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-right-outline",
            'content' => "\FCD6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-up",
            'content' => "\FC33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-arrow-up-outline",
            'content' => "\FC34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-check",
            'content' => "\F14C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-check-multiple",
            'content' => "\F028E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-check-multiple-outline",
            'content' => "\F028F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-check-outline",
            'content' => "\F8A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-file",
            'content' => "\F0290",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-file-outline",
            'content' => "\F0291",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-flow",
            'content' => "\F6C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-flow-outline",
            'content' => "\F0142",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-list",
            'content' => "\F00FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-list-outline",
            'content' => "\F0100",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-multiple",
            'content' => "\F0292",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-multiple-outline",
            'content' => "\F0293",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-outline",
            'content' => "\F14D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-play",
            'content' => "\FC35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-play-multiple",
            'content' => "\F0294",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-play-multiple-outline",
            'content' => "\F0295",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-play-outline",
            'content' => "\FC36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-plus",
            'content' => "\F750",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-plus-outline",
            'content' => "\F034A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-pulse",
            'content' => "\F85C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-pulse-outline",
            'content' => "\F85D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-text",
            'content' => "\F14E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-text-multiple",
            'content' => "\F0296",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-text-multiple-outline",
            'content' => "\F0297",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-text-outline",
            'content' => "\FA37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-text-play",
            'content' => "\FC37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clipboard-text-play-outline",
            'content' => "\FC38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clippy",
            'content' => "\F14F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock",
            'content' => "\F953",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-alert",
            'content' => "\F954",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-alert-outline",
            'content' => "\F5CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-check",
            'content' => "\FFC8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-check-outline",
            'content' => "\FFC9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-digital",
            'content' => "\FEB4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-end",
            'content' => "\F151",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-fast",
            'content' => "\F152",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-in",
            'content' => "\F153",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-out",
            'content' => "\F154",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-outline",
            'content' => "\F150",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clock-start",
            'content' => "\F155",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close",
            'content' => "\F156",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-box",
            'content' => "\F157",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-box-multiple",
            'content' => "\FC39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-box-multiple-outline",
            'content' => "\FC3A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-box-outline",
            'content' => "\F158",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-circle",
            'content' => "\F159",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-circle-outline",
            'content' => "\F15A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-network",
            'content' => "\F15B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-network-outline",
            'content' => "\FC3B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-octagon",
            'content' => "\F15C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-octagon-outline",
            'content' => "\F15D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "close-outline",
            'content' => "\F6C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "closed-caption",
            'content' => "\F15E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "closed-caption-outline",
            'content' => "\FD99",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud",
            'content' => "\F15F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-alert",
            'content' => "\F9DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-braces",
            'content' => "\F7B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-check",
            'content' => "\F160",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-check-outline",
            'content' => "\F02F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-circle",
            'content' => "\F161",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-download",
            'content' => "\F162",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-download-outline",
            'content' => "\FB59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-lock",
            'content' => "\F021C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-lock-outline",
            'content' => "\F021D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-off-outline",
            'content' => "\F164",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-outline",
            'content' => "\F163",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-print",
            'content' => "\F165",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-print-outline",
            'content' => "\F166",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-question",
            'content' => "\FA38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-search",
            'content' => "\F955",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-search-outline",
            'content' => "\F956",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-sync",
            'content' => "\F63F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-sync-outline",
            'content' => "\F0301",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-tags",
            'content' => "\F7B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-upload",
            'content' => "\F167",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cloud-upload-outline",
            'content' => "\FB5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "clover",
            'content' => "\F815",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coach-lamp",
            'content' => "\F0042",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coat-rack",
            'content' => "\F00C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-array",
            'content' => "\F168",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-braces",
            'content' => "\F169",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-braces-box",
            'content' => "\F0101",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-brackets",
            'content' => "\F16A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-equal",
            'content' => "\F16B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-greater-than",
            'content' => "\F16C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-greater-than-or-equal",
            'content' => "\F16D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-less-than",
            'content' => "\F16E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-less-than-or-equal",
            'content' => "\F16F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-not-equal",
            'content' => "\F170",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-not-equal-variant",
            'content' => "\F171",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-parentheses",
            'content' => "\F172",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-parentheses-box",
            'content' => "\F0102",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-string",
            'content' => "\F173",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-tags",
            'content' => "\F174",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "code-tags-check",
            'content' => "\F693",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "codepen",
            'content' => "\F175",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee",
            'content' => "\F176",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee-maker",
            'content' => "\F00CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee-off",
            'content' => "\FFCA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee-off-outline",
            'content' => "\FFCB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee-outline",
            'content' => "\F6C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee-to-go",
            'content' => "\F177",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffee-to-go-outline",
            'content' => "\F0339",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coffin",
            'content' => "\FB5B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cog-clockwise",
            'content' => "\F0208",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cog-counterclockwise",
            'content' => "\F0209",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cogs",
            'content' => "\F8D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coin",
            'content' => "\F0196",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coin-outline",
            'content' => "\F178",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coins",
            'content' => "\F694",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "collage",
            'content' => "\F640",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "collapse-all",
            'content' => "\FAA5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "collapse-all-outline",
            'content' => "\FAA6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "color-helper",
            'content' => "\F179",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comma",
            'content' => "\FE74",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comma-box",
            'content' => "\FE75",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comma-box-outline",
            'content' => "\FE76",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comma-circle",
            'content' => "\FE77",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comma-circle-outline",
            'content' => "\FE78",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment",
            'content' => "\F17A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-account",
            'content' => "\F17B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-account-outline",
            'content' => "\F17C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-alert",
            'content' => "\F17D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-alert-outline",
            'content' => "\F17E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-arrow-left",
            'content' => "\F9E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-arrow-left-outline",
            'content' => "\F9E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-arrow-right",
            'content' => "\F9E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-arrow-right-outline",
            'content' => "\F9E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-check",
            'content' => "\F17F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-check-outline",
            'content' => "\F180",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-edit",
            'content' => "\F01EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-edit-outline",
            'content' => "\F02EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-eye",
            'content' => "\FA39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-eye-outline",
            'content' => "\FA3A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-multiple",
            'content' => "\F85E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-multiple-outline",
            'content' => "\F181",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-outline",
            'content' => "\F182",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-plus",
            'content' => "\F9E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-plus-outline",
            'content' => "\F183",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-processing",
            'content' => "\F184",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-processing-outline",
            'content' => "\F185",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-question",
            'content' => "\F816",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-question-outline",
            'content' => "\F186",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-quote",
            'content' => "\F0043",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-quote-outline",
            'content' => "\F0044",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-remove",
            'content' => "\F5DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-remove-outline",
            'content' => "\F187",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-search",
            'content' => "\FA3B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-search-outline",
            'content' => "\FA3C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-text",
            'content' => "\F188",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-text-multiple",
            'content' => "\F85F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-text-multiple-outline",
            'content' => "\F860",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "comment-text-outline",
            'content' => "\F189",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "compare",
            'content' => "\F18A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "compass",
            'content' => "\F18B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "compass-off",
            'content' => "\FB5C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "compass-off-outline",
            'content' => "\FB5D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "compass-outline",
            'content' => "\F18C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "concourse-ci",
            'content' => "\F00CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "console",
            'content' => "\F18D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "console-line",
            'content' => "\F7B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "console-network",
            'content' => "\F8A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "console-network-outline",
            'content' => "\FC3C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "consolidate",
            'content' => "\F0103",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contact-mail",
            'content' => "\F18E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contact-mail-outline",
            'content' => "\FEB5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contact-phone",
            'content' => "\FEB6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contact-phone-outline",
            'content' => "\FEB7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contactless-payment",
            'content' => "\FD46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contacts",
            'content' => "\F6CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contain",
            'content' => "\FA3D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contain-end",
            'content' => "\FA3E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contain-start",
            'content' => "\FA3F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-copy",
            'content' => "\F18F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-cut",
            'content' => "\F190",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-duplicate",
            'content' => "\F191",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-paste",
            'content' => "\F192",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save",
            'content' => "\F193",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-alert",
            'content' => "\FF5F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-alert-outline",
            'content' => "\FF60",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-all",
            'content' => "\F194",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-all-outline",
            'content' => "\FF61",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-edit",
            'content' => "\FCD7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-edit-outline",
            'content' => "\FCD8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-move",
            'content' => "\FE79",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-move-outline",
            'content' => "\FE7A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-outline",
            'content' => "\F817",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-settings",
            'content' => "\F61B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "content-save-settings-outline",
            'content' => "\FB13",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contrast",
            'content' => "\F195",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contrast-box",
            'content' => "\F196",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "contrast-circle",
            'content' => "\F197",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "controller-classic",
            'content' => "\FB5E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "controller-classic-outline",
            'content' => "\FB5F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cookie",
            'content' => "\F198",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "coolant-temperature",
            'content' => "\F3C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "copyright",
            'content' => "\F5E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cordova",
            'content' => "\F957",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "corn",
            'content' => "\F7B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "counter",
            'content' => "\F199",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cow",
            'content' => "\F19A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cowboy",
            'content' => "\FEB8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cpu-32-bit",
            'content' => "\FEFC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cpu-64-bit",
            'content' => "\FEFD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crane",
            'content' => "\F861",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "creation",
            'content' => "\F1C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "creative-commons",
            'content' => "\FD47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card",
            'content' => "\F0010",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-clock",
            'content' => "\FEFE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-clock-outline",
            'content' => "\FFBC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-marker",
            'content' => "\F6A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-marker-outline",
            'content' => "\FD9A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-minus",
            'content' => "\FFCC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-minus-outline",
            'content' => "\FFCD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-multiple",
            'content' => "\F0011",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-multiple-outline",
            'content' => "\F19C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-off",
            'content' => "\F0012",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-off-outline",
            'content' => "\F5E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-outline",
            'content' => "\F19B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-plus",
            'content' => "\F0013",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-plus-outline",
            'content' => "\F675",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-refund",
            'content' => "\F0014",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-refund-outline",
            'content' => "\FAA7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-remove",
            'content' => "\FFCE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-remove-outline",
            'content' => "\FFCF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-scan",
            'content' => "\F0015",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-scan-outline",
            'content' => "\F19D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-settings",
            'content' => "\F0016",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-settings-outline",
            'content' => "\F8D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-wireless",
            'content' => "\F801",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "credit-card-wireless-outline",
            'content' => "\FD48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cricket",
            'content' => "\FD49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crop",
            'content' => "\F19E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crop-free",
            'content' => "\F19F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crop-landscape",
            'content' => "\F1A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crop-portrait",
            'content' => "\F1A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crop-rotate",
            'content' => "\F695",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crop-square",
            'content' => "\F1A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crosshairs",
            'content' => "\F1A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crosshairs-gps",
            'content' => "\F1A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crosshairs-off",
            'content' => "\FF62",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crosshairs-question",
            'content' => "\F0161",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crown",
            'content' => "\F1A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crown-outline",
            'content' => "\F01FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cryengine",
            'content' => "\F958",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "crystal-ball",
            'content' => "\FB14",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cube",
            'content' => "\F1A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cube-outline",
            'content' => "\F1A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cube-scan",
            'content' => "\FB60",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cube-send",
            'content' => "\F1A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cube-unfolded",
            'content' => "\F1A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cup",
            'content' => "\F1AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cup-off",
            'content' => "\F5E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cup-outline",
            'content' => "\F033A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cup-water",
            'content' => "\F1AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cupboard",
            'content' => "\FF63",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cupboard-outline",
            'content' => "\FF64",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cupcake",
            'content' => "\F959",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "curling",
            'content' => "\F862",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-bdt",
            'content' => "\F863",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-brl",
            'content' => "\FB61",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-btc",
            'content' => "\F1AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-cny",
            'content' => "\F7B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-eth",
            'content' => "\F7BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-eur",
            'content' => "\F1AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-eur-off",
            'content' => "\F0340",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-gbp",
            'content' => "\F1AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-ils",
            'content' => "\FC3D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-inr",
            'content' => "\F1AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-jpy",
            'content' => "\F7BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-krw",
            'content' => "\F7BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-kzt",
            'content' => "\F864",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-ngn",
            'content' => "\F1B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-php",
            'content' => "\F9E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-rial",
            'content' => "\FEB9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-rub",
            'content' => "\F1B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-sign",
            'content' => "\F7BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-try",
            'content' => "\F1B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-twd",
            'content' => "\F7BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-usd",
            'content' => "\F1B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "currency-usd-off",
            'content' => "\F679",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "current-ac",
            'content' => "\F95A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "current-dc",
            'content' => "\F95B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-default",
            'content' => "\F1B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-default-click",
            'content' => "\FCD9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-default-click-outline",
            'content' => "\FCDA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-default-gesture",
            'content' => "\F0152",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-default-gesture-outline",
            'content' => "\F0153",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-default-outline",
            'content' => "\F1B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-move",
            'content' => "\F1B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-pointer",
            'content' => "\F1B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "cursor-text",
            'content' => "\F5E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database",
            'content' => "\F1B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-check",
            'content' => "\FAA8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-edit",
            'content' => "\FB62",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-export",
            'content' => "\F95D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-import",
            'content' => "\F95C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-lock",
            'content' => "\FAA9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-marker",
            'content' => "\F0321",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-minus",
            'content' => "\F1B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-plus",
            'content' => "\F1BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-refresh",
            'content' => "\FCDB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-remove",
            'content' => "\FCDC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-search",
            'content' => "\F865",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "database-settings",
            'content' => "\FCDD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "death-star",
            'content' => "\F8D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "death-star-variant",
            'content' => "\F8D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "deathly-hallows",
            'content' => "\FB63",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "debian",
            'content' => "\F8D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "debug-step-into",
            'content' => "\F1BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "debug-step-out",
            'content' => "\F1BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "debug-step-over",
            'content' => "\F1BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decagram",
            'content' => "\F76B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decagram-outline",
            'content' => "\F76C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decimal",
            'content' => "\F00CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decimal-comma",
            'content' => "\F00CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decimal-comma-decrease",
            'content' => "\F00CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decimal-comma-increase",
            'content' => "\F00CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decimal-decrease",
            'content' => "\F1BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "decimal-increase",
            'content' => "\F1BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete",
            'content' => "\F1C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-alert",
            'content' => "\F00D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-alert-outline",
            'content' => "\F00D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-circle",
            'content' => "\F682",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-circle-outline",
            'content' => "\FB64",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-empty",
            'content' => "\F6CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-empty-outline",
            'content' => "\FEBA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-forever",
            'content' => "\F5E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-forever-outline",
            'content' => "\FB65",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-off",
            'content' => "\F00D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-off-outline",
            'content' => "\F00D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-outline",
            'content' => "\F9E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-restore",
            'content' => "\F818",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-sweep",
            'content' => "\F5E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-sweep-outline",
            'content' => "\FC3E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delete-variant",
            'content' => "\F1C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "delta",
            'content' => "\F1C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desk",
            'content' => "\F0264",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desk-lamp",
            'content' => "\F95E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "deskphone",
            'content' => "\F1C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desktop-classic",
            'content' => "\F7BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desktop-mac",
            'content' => "\F1C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desktop-mac-dashboard",
            'content' => "\F9E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desktop-tower",
            'content' => "\F1C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "desktop-tower-monitor",
            'content' => "\FAAA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "details",
            'content' => "\F1C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dev-to",
            'content' => "\FD4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "developer-board",
            'content' => "\F696",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "deviantart",
            'content' => "\F1C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "devices",
            'content' => "\FFD0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diabetes",
            'content' => "\F0151",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dialpad",
            'content' => "\F61C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diameter",
            'content' => "\FC3F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diameter-outline",
            'content' => "\FC40",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diameter-variant",
            'content' => "\FC41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diamond",
            'content' => "\FB66",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diamond-outline",
            'content' => "\FB67",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diamond-stone",
            'content' => "\F1C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-1",
            'content' => "\F1CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-1-outline",
            'content' => "\F0175",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-2",
            'content' => "\F1CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-2-outline",
            'content' => "\F0176",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-3",
            'content' => "\F1CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-3-outline",
            'content' => "\F0177",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-4",
            'content' => "\F1CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-4-outline",
            'content' => "\F0178",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-5",
            'content' => "\F1CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-5-outline",
            'content' => "\F0179",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-6",
            'content' => "\F1CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-6-outline",
            'content' => "\F017A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d10",
            'content' => "\F017E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d10-outline",
            'content' => "\F76E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d12",
            'content' => "\F017F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d12-outline",
            'content' => "\F866",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d20",
            'content' => "\F0180",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d20-outline",
            'content' => "\F5EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d4",
            'content' => "\F017B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d4-outline",
            'content' => "\F5EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d6",
            'content' => "\F017C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d6-outline",
            'content' => "\F5EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d8",
            'content' => "\F017D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-d8-outline",
            'content' => "\F5ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-multiple",
            'content' => "\F76D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dice-multiple-outline",
            'content' => "\F0181",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dictionary",
            'content' => "\F61D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "digital-ocean",
            'content' => "\F0262",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dip-switch",
            'content' => "\F7C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "directions",
            'content' => "\F1D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "directions-fork",
            'content' => "\F641",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "disc",
            'content' => "\F5EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "disc-alert",
            'content' => "\F1D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "disc-player",
            'content' => "\F95F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "discord",
            'content' => "\F66F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dishwasher",
            'content' => "\FAAB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dishwasher-alert",
            'content' => "\F01E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dishwasher-off",
            'content' => "\F01E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "disqus",
            'content' => "\F1D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "disqus-outline",
            'content' => "\F1D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "distribute-horizontal-center",
            'content' => "\F01F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "distribute-horizontal-left",
            'content' => "\F01F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "distribute-horizontal-right",
            'content' => "\F01F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "distribute-vertical-bottom",
            'content' => "\F01F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "distribute-vertical-center",
            'content' => "\F01F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "distribute-vertical-top",
            'content' => "\F01F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-flippers",
            'content' => "\FD9B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-helmet",
            'content' => "\FD9C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-scuba",
            'content' => "\FD9D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-scuba-flag",
            'content' => "\FD9E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-scuba-tank",
            'content' => "\FD9F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-scuba-tank-multiple",
            'content' => "\FDA0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "diving-snorkel",
            'content' => "\FDA1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "division",
            'content' => "\F1D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "division-box",
            'content' => "\F1D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dlna",
            'content' => "\FA40",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dna",
            'content' => "\F683",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dns",
            'content' => "\F1D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dns-outline",
            'content' => "\FB68",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "do-not-disturb",
            'content' => "\F697",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "do-not-disturb-off",
            'content' => "\F698",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dock-bottom",
            'content' => "\F00D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dock-left",
            'content' => "\F00D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dock-right",
            'content' => "\F00D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dock-window",
            'content' => "\F00D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "docker",
            'content' => "\F867",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "doctor",
            'content' => "\FA41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dog",
            'content' => "\FA42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dog-service",
            'content' => "\FAAC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dog-side",
            'content' => "\FA43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dolby",
            'content' => "\F6B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dolly",
            'content' => "\FEBB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "domain",
            'content' => "\F1D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "domain-off",
            'content' => "\FD4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "domain-plus",
            'content' => "\F00D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "domain-remove",
            'content' => "\F00D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "domino-mask",
            'content' => "\F0045",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "donkey",
            'content' => "\F7C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "door",
            'content' => "\F819",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "door-closed",
            'content' => "\F81A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "door-closed-lock",
            'content' => "\F00DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "door-open",
            'content' => "\F81B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "doorbell",
            'content' => "\F0311",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "doorbell-video",
            'content' => "\F868",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dot-net",
            'content' => "\FAAD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dots-horizontal",
            'content' => "\F1D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dots-horizontal-circle",
            'content' => "\F7C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dots-horizontal-circle-outline",
            'content' => "\FB69",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dots-vertical",
            'content' => "\F1D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dots-vertical-circle",
            'content' => "\F7C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dots-vertical-circle-outline",
            'content' => "\FB6A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "douban",
            'content' => "\F699",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download",
            'content' => "\F1DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download-multiple",
            'content' => "\F9E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download-network",
            'content' => "\F6F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download-network-outline",
            'content' => "\FC42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download-off",
            'content' => "\F00DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download-off-outline",
            'content' => "\F00DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "download-outline",
            'content' => "\FB6B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drag",
            'content' => "\F1DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drag-horizontal",
            'content' => "\F1DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drag-horizontal-variant",
            'content' => "\F031B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drag-variant",
            'content' => "\FB6C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drag-vertical",
            'content' => "\F1DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drag-vertical-variant",
            'content' => "\F031C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drama-masks",
            'content' => "\FCDE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "draw",
            'content' => "\FF66",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drawing",
            'content' => "\F1DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drawing-box",
            'content' => "\F1DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dresser",
            'content' => "\FF67",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dresser-outline",
            'content' => "\FF68",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dribbble",
            'content' => "\F1E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dribbble-box",
            'content' => "\F1E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drone",
            'content' => "\F1E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dropbox",
            'content' => "\F1E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "drupal",
            'content' => "\F1E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "duck",
            'content' => "\F1E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dumbbell",
            'content' => "\F1E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "dump-truck",
            'content' => "\FC43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ear-hearing",
            'content' => "\F7C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ear-hearing-off",
            'content' => "\FA44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "earth",
            'content' => "\F1E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "earth-arrow-right",
            'content' => "\F033C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "earth-box",
            'content' => "\F6CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "earth-box-off",
            'content' => "\F6CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "earth-off",
            'content' => "\F1E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "edge",
            'content' => "\F1E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "edge-legacy",
            'content' => "\F027B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "egg",
            'content' => "\FAAE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "egg-easter",
            'content' => "\FAAF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eight-track",
            'content' => "\F9E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eject",
            'content' => "\F1EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eject-outline",
            'content' => "\FB6D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "electric-switch",
            'content' => "\FEBC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "electric-switch-closed",
            'content' => "\F0104",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "electron-framework",
            'content' => "\F0046",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "elephant",
            'content' => "\F7C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "elevation-decline",
            'content' => "\F1EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "elevation-rise",
            'content' => "\F1EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "elevator",
            'content' => "\F1ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "elevator-down",
            'content' => "\F02ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "elevator-up",
            'content' => "\F02EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ellipse",
            'content' => "\FEBD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ellipse-outline",
            'content' => "\FEBE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email",
            'content' => "\F1EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-alert",
            'content' => "\F6CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-alert-outline",
            'content' => "\FD1E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-box",
            'content' => "\FCDF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-check",
            'content' => "\FAB0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-check-outline",
            'content' => "\FAB1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-edit",
            'content' => "\FF00",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-edit-outline",
            'content' => "\FF01",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-lock",
            'content' => "\F1F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-mark-as-unread",
            'content' => "\FB6E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-minus",
            'content' => "\FF02",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-minus-outline",
            'content' => "\FF03",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-multiple",
            'content' => "\FF04",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-multiple-outline",
            'content' => "\FF05",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-newsletter",
            'content' => "\FFD1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-open",
            'content' => "\F1EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-open-multiple",
            'content' => "\FF06",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-open-multiple-outline",
            'content' => "\FF07",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-open-outline",
            'content' => "\F5EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-outline",
            'content' => "\F1F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-plus",
            'content' => "\F9EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-plus-outline",
            'content' => "\F9EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-receive",
            'content' => "\F0105",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-receive-outline",
            'content' => "\F0106",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-search",
            'content' => "\F960",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-search-outline",
            'content' => "\F961",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-send",
            'content' => "\F0107",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-send-outline",
            'content' => "\F0108",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-sync",
            'content' => "\F02F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-sync-outline",
            'content' => "\F02F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "email-variant",
            'content' => "\F5F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ember",
            'content' => "\FB15",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emby",
            'content' => "\F6B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon",
            'content' => "\FC44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-angry",
            'content' => "\FC45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-angry-outline",
            'content' => "\FC46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-confused",
            'content' => "\F0109",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-confused-outline",
            'content' => "\F010A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-cool",
            'content' => "\FC47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-cool-outline",
            'content' => "\F1F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-cry",
            'content' => "\FC48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-cry-outline",
            'content' => "\FC49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-dead",
            'content' => "\FC4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-dead-outline",
            'content' => "\F69A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-devil",
            'content' => "\FC4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-devil-outline",
            'content' => "\F1F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-excited",
            'content' => "\FC4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-excited-outline",
            'content' => "\F69B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-frown",
            'content' => "\FF69",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-frown-outline",
            'content' => "\FF6A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-happy",
            'content' => "\FC4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-happy-outline",
            'content' => "\F1F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-kiss",
            'content' => "\FC4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-kiss-outline",
            'content' => "\FC4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-lol",
            'content' => "\F023F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-lol-outline",
            'content' => "\F0240",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-neutral",
            'content' => "\FC50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-neutral-outline",
            'content' => "\F1F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-outline",
            'content' => "\F1F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-poop",
            'content' => "\F1F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-poop-outline",
            'content' => "\FC51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-sad",
            'content' => "\FC52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-sad-outline",
            'content' => "\F1F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-tongue",
            'content' => "\F1F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-tongue-outline",
            'content' => "\FC53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-wink",
            'content' => "\FC54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "emoticon-wink-outline",
            'content' => "\FC55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "engine",
            'content' => "\F1FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "engine-off",
            'content' => "\FA45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "engine-off-outline",
            'content' => "\FA46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "engine-outline",
            'content' => "\F1FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "epsilon",
            'content' => "\F010B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "equal",
            'content' => "\F1FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "equal-box",
            'content' => "\F1FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "equalizer",
            'content' => "\FEBF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "equalizer-outline",
            'content' => "\FEC0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eraser",
            'content' => "\F1FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eraser-variant",
            'content' => "\F642",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "escalator",
            'content' => "\F1FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "escalator-down",
            'content' => "\F02EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "escalator-up",
            'content' => "\F02EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eslint",
            'content' => "\FC56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "et",
            'content' => "\FAB2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ethereum",
            'content' => "\F869",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ethernet",
            'content' => "\F200",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ethernet-cable",
            'content' => "\F201",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ethernet-cable-off",
            'content' => "\F202",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "etsy",
            'content' => "\F203",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ev-station",
            'content' => "\F5F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eventbrite",
            'content' => "\F7C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "evernote",
            'content' => "\F204",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "excavator",
            'content' => "\F0047",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "exclamation",
            'content' => "\F205",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "exclamation-thick",
            'content' => "\F0263",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "exit-run",
            'content' => "\FA47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "exit-to-app",
            'content' => "\F206",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "expand-all",
            'content' => "\FAB3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "expand-all-outline",
            'content' => "\FAB4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "expansion-card",
            'content' => "\F8AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "expansion-card-variant",
            'content' => "\FFD2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "exponent",
            'content' => "\F962",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "exponent-box",
            'content' => "\F963",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "export",
            'content' => "\F207",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "export-variant",
            'content' => "\FB6F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye",
            'content' => "\F208",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-check",
            'content' => "\FCE0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-check-outline",
            'content' => "\FCE1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-circle",
            'content' => "\FB70",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-circle-outline",
            'content' => "\FB71",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-minus",
            'content' => "\F0048",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-minus-outline",
            'content' => "\F0049",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-off",
            'content' => "\F209",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-off-outline",
            'content' => "\F6D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-outline",
            'content' => "\F6CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-plus",
            'content' => "\F86A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-plus-outline",
            'content' => "\F86B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-settings",
            'content' => "\F86C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eye-settings-outline",
            'content' => "\F86D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eyedropper",
            'content' => "\F20A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "eyedropper-variant",
            'content' => "\F20B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face",
            'content' => "\F643",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-agent",
            'content' => "\FD4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-outline",
            'content' => "\FB72",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-profile",
            'content' => "\F644",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-profile-woman",
            'content' => "\F00A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-recognition",
            'content' => "\FC57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-woman",
            'content' => "\F00A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "face-woman-outline",
            'content' => "\F00A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "facebook",
            'content' => "\F20C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "facebook-box",
            'content' => "\F20D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "facebook-messenger",
            'content' => "\F20E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "facebook-workplace",
            'content' => "\FB16",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "factory",
            'content' => "\F20F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fan",
            'content' => "\F210",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fan-off",
            'content' => "\F81C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fast-forward",
            'content' => "\F211",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fast-forward-10",
            'content' => "\FD4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fast-forward-30",
            'content' => "\FCE2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fast-forward-5",
            'content' => "\F0223",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fast-forward-outline",
            'content' => "\F6D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fax",
            'content' => "\F212",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "feather",
            'content' => "\F6D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "feature-search",
            'content' => "\FA48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "feature-search-outline",
            'content' => "\FA49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fedora",
            'content' => "\F8DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ferris-wheel",
            'content' => "\FEC1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ferry",
            'content' => "\F213",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file",
            'content' => "\F214",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-account",
            'content' => "\F73A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-account-outline",
            'content' => "\F004A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-alert",
            'content' => "\FA4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-alert-outline",
            'content' => "\FA4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cabinet",
            'content' => "\FAB5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cad",
            'content' => "\FF08",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cad-box",
            'content' => "\FF09",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cancel",
            'content' => "\FDA2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cancel-outline",
            'content' => "\FDA3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-certificate",
            'content' => "\F01B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-certificate-outline",
            'content' => "\F01B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-chart",
            'content' => "\F215",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-chart-outline",
            'content' => "\F004B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-check",
            'content' => "\F216",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-check-outline",
            'content' => "\FE7B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-clock",
            'content' => "\F030C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-clock-outline",
            'content' => "\F030D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cloud",
            'content' => "\F217",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-cloud-outline",
            'content' => "\F004C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-code",
            'content' => "\F22E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-code-outline",
            'content' => "\F004D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-compare",
            'content' => "\F8A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-delimited",
            'content' => "\F218",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-delimited-outline",
            'content' => "\FEC2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document",
            'content' => "\F219",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box",
            'content' => "\F21A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-check",
            'content' => "\FEC3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-check-outline",
            'content' => "\FEC4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-minus",
            'content' => "\FEC5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-minus-outline",
            'content' => "\FEC6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-multiple",
            'content' => "\FAB6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-multiple-outline",
            'content' => "\FAB7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-outline",
            'content' => "\F9EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-plus",
            'content' => "\FEC7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-plus-outline",
            'content' => "\FEC8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-remove",
            'content' => "\FEC9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-remove-outline",
            'content' => "\FECA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-search",
            'content' => "\FECB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-box-search-outline",
            'content' => "\FECC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-edit",
            'content' => "\FDA4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-edit-outline",
            'content' => "\FDA5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-document-outline",
            'content' => "\F9ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-download",
            'content' => "\F964",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-download-outline",
            'content' => "\F965",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-edit",
            'content' => "\F0212",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-edit-outline",
            'content' => "\F0213",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-excel",
            'content' => "\F21B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-excel-box",
            'content' => "\F21C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-excel-box-outline",
            'content' => "\F004E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-excel-outline",
            'content' => "\F004F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-export",
            'content' => "\F21D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-export-outline",
            'content' => "\F0050",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-eye",
            'content' => "\FDA6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-eye-outline",
            'content' => "\FDA7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-find",
            'content' => "\F21E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-find-outline",
            'content' => "\FB73",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-hidden",
            'content' => "\F613",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-image",
            'content' => "\F21F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-image-outline",
            'content' => "\FECD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-import",
            'content' => "\F220",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-import-outline",
            'content' => "\F0051",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-key",
            'content' => "\F01AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-key-outline",
            'content' => "\F01B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-link",
            'content' => "\F01A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-link-outline",
            'content' => "\F01A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-lock",
            'content' => "\F221",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-lock-outline",
            'content' => "\F0052",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-move",
            'content' => "\FAB8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-move-outline",
            'content' => "\F0053",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-multiple",
            'content' => "\F222",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-multiple-outline",
            'content' => "\F0054",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-music",
            'content' => "\F223",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-music-outline",
            'content' => "\FE7C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-outline",
            'content' => "\F224",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-pdf",
            'content' => "\F225",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-pdf-box",
            'content' => "\F226",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-pdf-box-outline",
            'content' => "\FFD3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-pdf-outline",
            'content' => "\FE7D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-percent",
            'content' => "\F81D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-percent-outline",
            'content' => "\F0055",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-phone",
            'content' => "\F01A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-phone-outline",
            'content' => "\F01A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-plus",
            'content' => "\F751",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-plus-outline",
            'content' => "\FF0A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-powerpoint",
            'content' => "\F227",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-powerpoint-box",
            'content' => "\F228",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-powerpoint-box-outline",
            'content' => "\F0056",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-powerpoint-outline",
            'content' => "\F0057",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-presentation-box",
            'content' => "\F229",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-question",
            'content' => "\F86E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-question-outline",
            'content' => "\F0058",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-remove",
            'content' => "\FB74",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-remove-outline",
            'content' => "\F0059",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-replace",
            'content' => "\FB17",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-replace-outline",
            'content' => "\FB18",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-restore",
            'content' => "\F670",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-restore-outline",
            'content' => "\F005A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-search",
            'content' => "\FC58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-search-outline",
            'content' => "\FC59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-send",
            'content' => "\F22A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-send-outline",
            'content' => "\F005B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-settings",
            'content' => "\F00A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-settings-outline",
            'content' => "\F00A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-settings-variant",
            'content' => "\F00A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-settings-variant-outline",
            'content' => "\F00A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-star",
            'content' => "\F005C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-star-outline",
            'content' => "\F005D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-swap",
            'content' => "\FFD4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-swap-outline",
            'content' => "\FFD5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-sync",
            'content' => "\F0241",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-sync-outline",
            'content' => "\F0242",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-table",
            'content' => "\FC5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-table-box",
            'content' => "\F010C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-table-box-multiple",
            'content' => "\F010D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-table-box-multiple-outline",
            'content' => "\F010E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-table-box-outline",
            'content' => "\F010F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-table-outline",
            'content' => "\FC5B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-tree",
            'content' => "\F645",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-undo",
            'content' => "\F8DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-undo-outline",
            'content' => "\F005E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-upload",
            'content' => "\FA4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-upload-outline",
            'content' => "\FA4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-video",
            'content' => "\F22B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-video-outline",
            'content' => "\FE10",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-word",
            'content' => "\F22C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-word-box",
            'content' => "\F22D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-word-box-outline",
            'content' => "\F005F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "file-word-outline",
            'content' => "\F0060",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "film",
            'content' => "\F22F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filmstrip",
            'content' => "\F230",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filmstrip-off",
            'content' => "\F231",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter",
            'content' => "\F232",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-menu",
            'content' => "\F0110",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-menu-outline",
            'content' => "\F0111",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-minus",
            'content' => "\FF0B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-minus-outline",
            'content' => "\FF0C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-outline",
            'content' => "\F233",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-plus",
            'content' => "\FF0D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-plus-outline",
            'content' => "\FF0E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-remove",
            'content' => "\F234",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-remove-outline",
            'content' => "\F235",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-variant",
            'content' => "\F236",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-variant-minus",
            'content' => "\F013D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-variant-plus",
            'content' => "\F013E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "filter-variant-remove",
            'content' => "\F0061",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "finance",
            'content' => "\F81E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "find-replace",
            'content' => "\F6D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fingerprint",
            'content' => "\F237",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fingerprint-off",
            'content' => "\FECE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fire",
            'content' => "\F238",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fire-extinguisher",
            'content' => "\FF0F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fire-hydrant",
            'content' => "\F0162",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fire-hydrant-alert",
            'content' => "\F0163",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fire-hydrant-off",
            'content' => "\F0164",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fire-truck",
            'content' => "\F8AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "firebase",
            'content' => "\F966",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "firefox",
            'content' => "\F239",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fireplace",
            'content' => "\FE11",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fireplace-off",
            'content' => "\FE12",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "firework",
            'content' => "\FE13",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fish",
            'content' => "\F23A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fishbowl",
            'content' => "\FF10",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fishbowl-outline",
            'content' => "\FF11",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fit-to-page",
            'content' => "\FF12",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fit-to-page-outline",
            'content' => "\FF13",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag",
            'content' => "\F23B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-checkered",
            'content' => "\F23C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-minus",
            'content' => "\FB75",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-minus-outline",
            'content' => "\F00DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-outline",
            'content' => "\F23D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-plus",
            'content' => "\FB76",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-plus-outline",
            'content' => "\F00DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-remove",
            'content' => "\FB77",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-remove-outline",
            'content' => "\F00DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-triangle",
            'content' => "\F23F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-variant",
            'content' => "\F240",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flag-variant-outline",
            'content' => "\F23E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flare",
            'content' => "\FD4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash",
            'content' => "\F241",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-alert",
            'content' => "\FF14",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-alert-outline",
            'content' => "\FF15",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-auto",
            'content' => "\F242",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-circle",
            'content' => "\F81F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-off",
            'content' => "\F243",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-outline",
            'content' => "\F6D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flash-red-eye",
            'content' => "\F67A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flashlight",
            'content' => "\F244",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flashlight-off",
            'content' => "\F245",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask",
            'content' => "\F093",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty",
            'content' => "\F094",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-minus",
            'content' => "\F0265",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-minus-outline",
            'content' => "\F0266",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-outline",
            'content' => "\F095",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-plus",
            'content' => "\F0267",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-plus-outline",
            'content' => "\F0268",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-remove",
            'content' => "\F0269",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-empty-remove-outline",
            'content' => "\F026A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-minus",
            'content' => "\F026B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-minus-outline",
            'content' => "\F026C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-outline",
            'content' => "\F096",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-plus",
            'content' => "\F026D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-plus-outline",
            'content' => "\F026E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-remove",
            'content' => "\F026F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-remove-outline",
            'content' => "\F0270",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-round-bottom",
            'content' => "\F0276",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-round-bottom-empty",
            'content' => "\F0277",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-round-bottom-empty-outline",
            'content' => "\F0278",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flask-round-bottom-outline",
            'content' => "\F0279",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flattr",
            'content' => "\F246",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fleur-de-lis",
            'content' => "\F032E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flickr",
            'content' => "\FCE3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flip-horizontal",
            'content' => "\F0112",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flip-to-back",
            'content' => "\F247",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flip-to-front",
            'content' => "\F248",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flip-vertical",
            'content' => "\F0113",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "floor-lamp",
            'content' => "\F8DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "floor-lamp-dual",
            'content' => "\F0062",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "floor-lamp-variant",
            'content' => "\F0063",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "floor-plan",
            'content' => "\F820",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "floppy",
            'content' => "\F249",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "floppy-variant",
            'content' => "\F9EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flower",
            'content' => "\F24A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flower-outline",
            'content' => "\F9EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flower-poppy",
            'content' => "\FCE4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flower-tulip",
            'content' => "\F9F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "flower-tulip-outline",
            'content' => "\F9F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "focus-auto",
            'content' => "\FF6B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "focus-field",
            'content' => "\FF6C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "focus-field-horizontal",
            'content' => "\FF6D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "focus-field-vertical",
            'content' => "\FF6E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder",
            'content' => "\F24B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-account",
            'content' => "\F24C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-account-outline",
            'content' => "\FB78",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-alert",
            'content' => "\FDA8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-alert-outline",
            'content' => "\FDA9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-clock",
            'content' => "\FAB9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-clock-outline",
            'content' => "\FABA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-download",
            'content' => "\F24D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-download-outline",
            'content' => "\F0114",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-edit",
            'content' => "\F8DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-edit-outline",
            'content' => "\FDAA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-google-drive",
            'content' => "\F24E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-heart",
            'content' => "\F0115",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-heart-outline",
            'content' => "\F0116",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-home",
            'content' => "\F00E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-home-outline",
            'content' => "\F00E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-image",
            'content' => "\F24F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-information",
            'content' => "\F00E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-information-outline",
            'content' => "\F00E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-key",
            'content' => "\F8AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-key-network",
            'content' => "\F8AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-key-network-outline",
            'content' => "\FC5C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-key-outline",
            'content' => "\F0117",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-lock",
            'content' => "\F250",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-lock-open",
            'content' => "\F251",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-marker",
            'content' => "\F0298",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-marker-outline",
            'content' => "\F0299",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-move",
            'content' => "\F252",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-move-outline",
            'content' => "\F0271",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-multiple",
            'content' => "\F253",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-multiple-image",
            'content' => "\F254",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-multiple-outline",
            'content' => "\F255",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-network",
            'content' => "\F86F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-network-outline",
            'content' => "\FC5D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-open",
            'content' => "\F76F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-open-outline",
            'content' => "\FDAB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-outline",
            'content' => "\F256",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-plus",
            'content' => "\F257",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-plus-outline",
            'content' => "\FB79",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-pound",
            'content' => "\FCE5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-pound-outline",
            'content' => "\FCE6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-remove",
            'content' => "\F258",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-remove-outline",
            'content' => "\FB7A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-search",
            'content' => "\F967",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-search-outline",
            'content' => "\F968",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-settings",
            'content' => "\F00A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-settings-outline",
            'content' => "\F00A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-settings-variant",
            'content' => "\F00AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-settings-variant-outline",
            'content' => "\F00AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-star",
            'content' => "\F69C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-star-outline",
            'content' => "\FB7B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-swap",
            'content' => "\FFD6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-swap-outline",
            'content' => "\FFD7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-sync",
            'content' => "\FCE7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-sync-outline",
            'content' => "\FCE8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-table",
            'content' => "\F030E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-table-outline",
            'content' => "\F030F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-text",
            'content' => "\FC5E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-text-outline",
            'content' => "\FC5F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-upload",
            'content' => "\F259",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-upload-outline",
            'content' => "\F0118",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-zip",
            'content' => "\F6EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "folder-zip-outline",
            'content' => "\F7B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "font-awesome",
            'content' => "\F03A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food",
            'content' => "\F25A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food-apple",
            'content' => "\F25B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food-apple-outline",
            'content' => "\FC60",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food-croissant",
            'content' => "\F7C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food-fork-drink",
            'content' => "\F5F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food-off",
            'content' => "\F5F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "food-variant",
            'content' => "\F25C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "foot-print",
            'content' => "\FF6F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "football",
            'content' => "\F25D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "football-australian",
            'content' => "\F25E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "football-helmet",
            'content' => "\F25F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "forklift",
            'content' => "\F7C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-bottom",
            'content' => "\F752",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-center",
            'content' => "\F260",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-justify",
            'content' => "\F261",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-left",
            'content' => "\F262",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-middle",
            'content' => "\F753",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-right",
            'content' => "\F263",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-align-top",
            'content' => "\F754",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-annotation-minus",
            'content' => "\FABB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-annotation-plus",
            'content' => "\F646",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-bold",
            'content' => "\F264",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-clear",
            'content' => "\F265",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-color-fill",
            'content' => "\F266",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-color-highlight",
            'content' => "\FE14",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-color-marker-cancel",
            'content' => "\F033E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-color-text",
            'content' => "\F69D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-columns",
            'content' => "\F8DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-float-center",
            'content' => "\F267",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-float-left",
            'content' => "\F268",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-float-none",
            'content' => "\F269",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-float-right",
            'content' => "\F26A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-font",
            'content' => "\F6D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-font-size-decrease",
            'content' => "\F9F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-font-size-increase",
            'content' => "\F9F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-1",
            'content' => "\F26B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-2",
            'content' => "\F26C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-3",
            'content' => "\F26D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-4",
            'content' => "\F26E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-5",
            'content' => "\F26F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-6",
            'content' => "\F270",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-decrease",
            'content' => "\F271",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-equal",
            'content' => "\F272",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-increase",
            'content' => "\F273",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-header-pound",
            'content' => "\F274",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-horizontal-align-center",
            'content' => "\F61E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-horizontal-align-left",
            'content' => "\F61F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-horizontal-align-right",
            'content' => "\F620",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-indent-decrease",
            'content' => "\F275",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-indent-increase",
            'content' => "\F276",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-italic",
            'content' => "\F277",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-letter-case",
            'content' => "\FB19",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-letter-case-lower",
            'content' => "\FB1A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-letter-case-upper",
            'content' => "\FB1B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-letter-ends-with",
            'content' => "\FFD8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-letter-matches",
            'content' => "\FFD9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-letter-starts-with",
            'content' => "\FFDA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-line-spacing",
            'content' => "\F278",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-line-style",
            'content' => "\F5C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-line-weight",
            'content' => "\F5C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-bulleted",
            'content' => "\F279",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-bulleted-square",
            'content' => "\FDAC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-bulleted-triangle",
            'content' => "\FECF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-bulleted-type",
            'content' => "\F27A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-checkbox",
            'content' => "\F969",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-checks",
            'content' => "\F755",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-numbered",
            'content' => "\F27B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-numbered-rtl",
            'content' => "\FCE9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-list-text",
            'content' => "\F029A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-overline",
            'content' => "\FED0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-page-break",
            'content' => "\F6D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-paint",
            'content' => "\F27C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-paragraph",
            'content' => "\F27D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-pilcrow",
            'content' => "\F6D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-quote-close",
            'content' => "\F27E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-quote-close-outline",
            'content' => "\F01D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-quote-open",
            'content' => "\F756",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-quote-open-outline",
            'content' => "\F01D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-rotate-90",
            'content' => "\F6A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-section",
            'content' => "\F69E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-size",
            'content' => "\F27F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-strikethrough",
            'content' => "\F280",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-strikethrough-variant",
            'content' => "\F281",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-subscript",
            'content' => "\F282",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-superscript",
            'content' => "\F283",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text",
            'content' => "\F284",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-angle-down",
            'content' => "\FFDB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-angle-up",
            'content' => "\FFDC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-down",
            'content' => "\FD4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-down-vertical",
            'content' => "\FFDD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-none",
            'content' => "\FD50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-up",
            'content' => "\FFDE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-rotation-vertical",
            'content' => "\FFDF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-variant",
            'content' => "\FE15",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-wrapping-clip",
            'content' => "\FCEA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-wrapping-overflow",
            'content' => "\FCEB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-text-wrapping-wrap",
            'content' => "\FCEC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-textbox",
            'content' => "\FCED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-textdirection-l-to-r",
            'content' => "\F285",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-textdirection-r-to-l",
            'content' => "\F286",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-title",
            'content' => "\F5F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-underline",
            'content' => "\F287",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-vertical-align-bottom",
            'content' => "\F621",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-vertical-align-center",
            'content' => "\F622",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-vertical-align-top",
            'content' => "\F623",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-wrap-inline",
            'content' => "\F288",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-wrap-square",
            'content' => "\F289",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-wrap-tight",
            'content' => "\F28A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "format-wrap-top-bottom",
            'content' => "\F28B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "forum",
            'content' => "\F28C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "forum-outline",
            'content' => "\F821",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "forward",
            'content' => "\F28D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "forwardburger",
            'content' => "\FD51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fountain",
            'content' => "\F96A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fountain-pen",
            'content' => "\FCEE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fountain-pen-tip",
            'content' => "\FCEF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "foursquare",
            'content' => "\F28E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "freebsd",
            'content' => "\F8DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "frequently-asked-questions",
            'content' => "\FED1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge",
            'content' => "\F290",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-alert",
            'content' => "\F01DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-alert-outline",
            'content' => "\F01DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-bottom",
            'content' => "\F292",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-off",
            'content' => "\F01DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-off-outline",
            'content' => "\F01DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-outline",
            'content' => "\F28F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fridge-top",
            'content' => "\F291",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fruit-cherries",
            'content' => "\F0064",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fruit-citrus",
            'content' => "\F0065",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fruit-grapes",
            'content' => "\F0066",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fruit-grapes-outline",
            'content' => "\F0067",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fruit-pineapple",
            'content' => "\F0068",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fruit-watermelon",
            'content' => "\F0069",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fuel",
            'content' => "\F7C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fullscreen",
            'content' => "\F293",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fullscreen-exit",
            'content' => "\F294",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "function",
            'content' => "\F295",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "function-variant",
            'content' => "\F870",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "furigana-horizontal",
            'content' => "\F00AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "furigana-vertical",
            'content' => "\F00AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fuse",
            'content' => "\FC61",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "fuse-blade",
            'content' => "\FC62",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad",
            'content' => "\F296",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-circle",
            'content' => "\FE16",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-circle-down",
            'content' => "\FE17",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-circle-left",
            'content' => "\FE18",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-circle-outline",
            'content' => "\FE19",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-circle-right",
            'content' => "\FE1A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-circle-up",
            'content' => "\FE1B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-down",
            'content' => "\FE1C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-left",
            'content' => "\FE1D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-right",
            'content' => "\FE1E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-round",
            'content' => "\FE1F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-round-down",
            'content' => "\FE7E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-round-left",
            'content' => "\FE7F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-round-outline",
            'content' => "\FE80",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-round-right",
            'content' => "\FE81",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-round-up",
            'content' => "\FE82",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-square",
            'content' => "\FED2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-square-outline",
            'content' => "\FED3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-up",
            'content' => "\FE83",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-variant",
            'content' => "\F297",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamepad-variant-outline",
            'content' => "\FED4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gamma",
            'content' => "\F0119",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gantry-crane",
            'content' => "\FDAD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "garage",
            'content' => "\F6D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "garage-alert",
            'content' => "\F871",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "garage-alert-variant",
            'content' => "\F0300",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "garage-open",
            'content' => "\F6D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "garage-open-variant",
            'content' => "\F02FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "garage-variant",
            'content' => "\F02FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gas-cylinder",
            'content' => "\F647",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gas-station",
            'content' => "\F298",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gas-station-outline",
            'content' => "\FED5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate",
            'content' => "\F299",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-and",
            'content' => "\F8E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-arrow-right",
            'content' => "\F0194",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-nand",
            'content' => "\F8E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-nor",
            'content' => "\F8E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-not",
            'content' => "\F8E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-open",
            'content' => "\F0195",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-or",
            'content' => "\F8E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-xnor",
            'content' => "\F8E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gate-xor",
            'content' => "\F8E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gatsby",
            'content' => "\FE84",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gauge",
            'content' => "\F29A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gauge-empty",
            'content' => "\F872",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gauge-full",
            'content' => "\F873",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gauge-low",
            'content' => "\F874",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gavel",
            'content' => "\F29B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gender-female",
            'content' => "\F29C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gender-male",
            'content' => "\F29D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gender-male-female",
            'content' => "\F29E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gender-male-female-variant",
            'content' => "\F016A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gender-non-binary",
            'content' => "\F016B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gender-transgender",
            'content' => "\F29F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gentoo",
            'content' => "\F8E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture",
            'content' => "\F7CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-double-tap",
            'content' => "\F73B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-pinch",
            'content' => "\FABC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-spread",
            'content' => "\FABD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe",
            'content' => "\FD52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe-down",
            'content' => "\F73C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe-horizontal",
            'content' => "\FABE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe-left",
            'content' => "\F73D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe-right",
            'content' => "\F73E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe-up",
            'content' => "\F73F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-swipe-vertical",
            'content' => "\FABF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-tap",
            'content' => "\F740",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-tap-box",
            'content' => "\F02D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-tap-button",
            'content' => "\F02D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-tap-hold",
            'content' => "\FD53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-two-double-tap",
            'content' => "\F741",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gesture-two-tap",
            'content' => "\F742",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ghost",
            'content' => "\F2A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ghost-off",
            'content' => "\F9F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gif",
            'content' => "\FD54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gift",
            'content' => "\FE85",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gift-outline",
            'content' => "\F2A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "git",
            'content' => "\F2A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "github-box",
            'content' => "\F2A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "github-circle",
            'content' => "\F2A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "github-face",
            'content' => "\F6DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gitlab",
            'content' => "\FB7C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-cocktail",
            'content' => "\F356",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-flute",
            'content' => "\F2A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-mug",
            'content' => "\F2A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-mug-variant",
            'content' => "\F0141",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-pint-outline",
            'content' => "\F0338",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-stange",
            'content' => "\F2A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-tulip",
            'content' => "\F2A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glass-wine",
            'content' => "\F875",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glassdoor",
            'content' => "\F2A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "glasses",
            'content' => "\F2AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "globe-light",
            'content' => "\F0302",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "globe-model",
            'content' => "\F8E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gmail",
            'content' => "\F2AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gnome",
            'content' => "\F2AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "go-kart",
            'content' => "\FD55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "go-kart-track",
            'content' => "\FD56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gog",
            'content' => "\FB7D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gold",
            'content' => "\F027A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "golf",
            'content' => "\F822",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "golf-cart",
            'content' => "\F01CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "golf-tee",
            'content' => "\F00AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gondola",
            'content' => "\F685",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "goodreads",
            'content' => "\FD57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google",
            'content' => "\F2AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-adwords",
            'content' => "\FC63",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-analytics",
            'content' => "\F7CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-assistant",
            'content' => "\F7CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-cardboard",
            'content' => "\F2AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-chrome",
            'content' => "\F2AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-circles",
            'content' => "\F2B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-circles-communities",
            'content' => "\F2B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-circles-extended",
            'content' => "\F2B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-circles-group",
            'content' => "\F2B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-classroom",
            'content' => "\F2C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-cloud",
            'content' => "\F0221",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-controller",
            'content' => "\F2B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-controller-off",
            'content' => "\F2B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-drive",
            'content' => "\F2B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-earth",
            'content' => "\F2B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-fit",
            'content' => "\F96B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-glass",
            'content' => "\F2B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-hangouts",
            'content' => "\F2C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-home",
            'content' => "\F823",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-keep",
            'content' => "\F6DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-lens",
            'content' => "\F9F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-maps",
            'content' => "\F5F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-my-business",
            'content' => "\F006A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-nearby",
            'content' => "\F2B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-pages",
            'content' => "\F2BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-photos",
            'content' => "\F6DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-physical-web",
            'content' => "\F2BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-play",
            'content' => "\F2BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-plus",
            'content' => "\F2BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-plus-box",
            'content' => "\F2BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-podcast",
            'content' => "\FED6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-spreadsheet",
            'content' => "\F9F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-street-view",
            'content' => "\FC64",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "google-translate",
            'content' => "\F2BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "gradient",
            'content' => "\F69F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grain",
            'content' => "\FD58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "graph",
            'content' => "\F006B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "graph-outline",
            'content' => "\F006C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "graphql",
            'content' => "\F876",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grave-stone",
            'content' => "\FB7E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grease-pencil",
            'content' => "\F648",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "greater-than",
            'content' => "\F96C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "greater-than-or-equal",
            'content' => "\F96D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grid",
            'content' => "\F2C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grid-large",
            'content' => "\F757",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grid-off",
            'content' => "\F2C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grill",
            'content' => "\FE86",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "grill-outline",
            'content' => "\F01B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "group",
            'content' => "\F2C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "guitar-acoustic",
            'content' => "\F770",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "guitar-electric",
            'content' => "\F2C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "guitar-pick",
            'content' => "\F2C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "guitar-pick-outline",
            'content' => "\F2C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "guy-fawkes-mask",
            'content' => "\F824",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hackernews",
            'content' => "\F624",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hail",
            'content' => "\FAC0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hair-dryer",
            'content' => "\F011A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hair-dryer-outline",
            'content' => "\F011B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "halloween",
            'content' => "\FB7F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hamburger",
            'content' => "\F684",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hammer",
            'content' => "\F8E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand",
            'content' => "\FA4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-heart",
            'content' => "\F011C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-left",
            'content' => "\FE87",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-okay",
            'content' => "\FA4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-peace",
            'content' => "\FA50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-peace-variant",
            'content' => "\FA51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-pointing-down",
            'content' => "\FA52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-pointing-left",
            'content' => "\FA53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-pointing-right",
            'content' => "\F2C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-pointing-up",
            'content' => "\FA54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-right",
            'content' => "\FE88",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hand-saw",
            'content' => "\FE89",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "handball",
            'content' => "\FF70",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "handcuffs",
            'content' => "\F0169",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "handshake",
            'content' => "\F0243",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hanger",
            'content' => "\F2C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hard-hat",
            'content' => "\F96E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "harddisk",
            'content' => "\F2CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "harddisk-plus",
            'content' => "\F006D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "harddisk-remove",
            'content' => "\F006E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hat-fedora",
            'content' => "\FB80",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hazard-lights",
            'content' => "\FC65",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hdr",
            'content' => "\FD59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hdr-off",
            'content' => "\FD5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headphones",
            'content' => "\F2CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headphones-bluetooth",
            'content' => "\F96F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headphones-box",
            'content' => "\F2CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headphones-off",
            'content' => "\F7CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headphones-settings",
            'content' => "\F2CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headset",
            'content' => "\F2CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headset-dock",
            'content' => "\F2CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "headset-off",
            'content' => "\F2D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart",
            'content' => "\F2D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-box",
            'content' => "\F2D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-box-outline",
            'content' => "\F2D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-broken",
            'content' => "\F2D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-broken-outline",
            'content' => "\FCF0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-circle",
            'content' => "\F970",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-circle-outline",
            'content' => "\F971",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-flash",
            'content' => "\FF16",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-half",
            'content' => "\F6DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-half-full",
            'content' => "\F6DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-half-outline",
            'content' => "\F6DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-multiple",
            'content' => "\FA55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-multiple-outline",
            'content' => "\FA56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-off",
            'content' => "\F758",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-outline",
            'content' => "\F2D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "heart-pulse",
            'content' => "\F5F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "helicopter",
            'content' => "\FAC1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help",
            'content' => "\F2D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-box",
            'content' => "\F78A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-circle",
            'content' => "\F2D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-circle-outline",
            'content' => "\F625",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-network",
            'content' => "\F6F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-network-outline",
            'content' => "\FC66",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-rhombus",
            'content' => "\FB81",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "help-rhombus-outline",
            'content' => "\FB82",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexadecimal",
            'content' => "\F02D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon",
            'content' => "\F2D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-multiple",
            'content' => "\F6E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-multiple-outline",
            'content' => "\F011D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-outline",
            'content' => "\F2D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-slice-1",
            'content' => "\FAC2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-slice-2",
            'content' => "\FAC3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-slice-3",
            'content' => "\FAC4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-slice-4",
            'content' => "\FAC5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-slice-5",
            'content' => "\FAC6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagon-slice-6",
            'content' => "\FAC7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagram",
            'content' => "\FAC8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hexagram-outline",
            'content' => "\FAC9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "high-definition",
            'content' => "\F7CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "high-definition-box",
            'content' => "\F877",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "highway",
            'content' => "\F5F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hiking",
            'content' => "\FD5B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hinduism",
            'content' => "\F972",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "history",
            'content' => "\F2DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hockey-puck",
            'content' => "\F878",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hockey-sticks",
            'content' => "\F879",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hololens",
            'content' => "\F2DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home",
            'content' => "\F2DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-account",
            'content' => "\F825",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-alert",
            'content' => "\F87A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-analytics",
            'content' => "\FED7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-assistant",
            'content' => "\F7CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-automation",
            'content' => "\F7D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-circle",
            'content' => "\F7D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-circle-outline",
            'content' => "\F006F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-city",
            'content' => "\FCF1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-city-outline",
            'content' => "\FCF2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-currency-usd",
            'content' => "\F8AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-edit",
            'content' => "\F0184",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-edit-outline",
            'content' => "\F0185",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-export-outline",
            'content' => "\FFB8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-flood",
            'content' => "\FF17",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-0",
            'content' => "\FDAE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-1",
            'content' => "\FD5C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-2",
            'content' => "\FD5D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-3",
            'content' => "\FD5E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-a",
            'content' => "\FD5F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-b",
            'content' => "\FD60",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-g",
            'content' => "\FD61",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-l",
            'content' => "\FD62",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-floor-negative-1",
            'content' => "\FDAF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-group",
            'content' => "\FDB0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-heart",
            'content' => "\F826",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-import-outline",
            'content' => "\FFB9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-lightbulb",
            'content' => "\F027C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-lightbulb-outline",
            'content' => "\F027D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-lock",
            'content' => "\F8EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-lock-open",
            'content' => "\F8EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-map-marker",
            'content' => "\F5F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-minus",
            'content' => "\F973",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-modern",
            'content' => "\F2DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-outline",
            'content' => "\F6A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-plus",
            'content' => "\F974",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-remove",
            'content' => "\F0272",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-roof",
            'content' => "\F0156",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-thermometer",
            'content' => "\FF71",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-thermometer-outline",
            'content' => "\FF72",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-variant",
            'content' => "\F2DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "home-variant-outline",
            'content' => "\FB83",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hook",
            'content' => "\F6E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hook-off",
            'content' => "\F6E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hops",
            'content' => "\F2DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "horizontal-rotate-clockwise",
            'content' => "\F011E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "horizontal-rotate-counterclockwise",
            'content' => "\F011F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "horseshoe",
            'content' => "\FA57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hospital",
            'content' => "\F0017",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hospital-box",
            'content' => "\F2E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hospital-box-outline",
            'content' => "\F0018",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hospital-building",
            'content' => "\F2E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hospital-marker",
            'content' => "\F2E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hot-tub",
            'content' => "\F827",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hotel",
            'content' => "\F2E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "houzz",
            'content' => "\F2E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "houzz-box",
            'content' => "\F2E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hubspot",
            'content' => "\FCF3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hulu",
            'content' => "\F828",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human",
            'content' => "\F2E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-child",
            'content' => "\F2E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-female",
            'content' => "\F649",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-female-boy",
            'content' => "\FA58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-female-female",
            'content' => "\FA59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-female-girl",
            'content' => "\FA5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-greeting",
            'content' => "\F64A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-handsdown",
            'content' => "\F64B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-handsup",
            'content' => "\F64C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male",
            'content' => "\F64D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male-boy",
            'content' => "\FA5B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male-female",
            'content' => "\F2E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male-girl",
            'content' => "\FA5C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male-height",
            'content' => "\FF18",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male-height-variant",
            'content' => "\FF19",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-male-male",
            'content' => "\FA5D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "human-pregnant",
            'content' => "\F5CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "humble-bundle",
            'content' => "\F743",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "hydro-power",
            'content' => "\F0310",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ice-cream",
            'content' => "\F829",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ice-pop",
            'content' => "\FF1A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "id-card",
            'content' => "\FFE0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "identifier",
            'content' => "\FF1B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe",
            'content' => "\FC67",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-array",
            'content' => "\F0120",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-array-outline",
            'content' => "\F0121",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-braces",
            'content' => "\F0122",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-braces-outline",
            'content' => "\F0123",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-outline",
            'content' => "\FC68",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-parentheses",
            'content' => "\F0124",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-parentheses-outline",
            'content' => "\F0125",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-variable",
            'content' => "\F0126",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iframe-variable-outline",
            'content' => "\F0127",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image",
            'content' => "\F2E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-album",
            'content' => "\F2EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-area",
            'content' => "\F2EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-area-close",
            'content' => "\F2EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-auto-adjust",
            'content' => "\FFE1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-broken",
            'content' => "\F2ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-broken-variant",
            'content' => "\F2EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-edit",
            'content' => "\F020E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-edit-outline",
            'content' => "\F020F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter",
            'content' => "\F2EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-black-white",
            'content' => "\F2F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-center-focus",
            'content' => "\F2F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-center-focus-strong",
            'content' => "\FF1C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-center-focus-strong-outline",
            'content' => "\FF1D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-center-focus-weak",
            'content' => "\F2F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-drama",
            'content' => "\F2F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-frames",
            'content' => "\F2F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-hdr",
            'content' => "\F2F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-none",
            'content' => "\F2F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-tilt-shift",
            'content' => "\F2F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-filter-vintage",
            'content' => "\F2F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-frame",
            'content' => "\FE8A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-move",
            'content' => "\F9F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-multiple",
            'content' => "\F2F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-off",
            'content' => "\F82A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-off-outline",
            'content' => "\F01FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-outline",
            'content' => "\F975",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-plus",
            'content' => "\F87B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-search",
            'content' => "\F976",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-search-outline",
            'content' => "\F977",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-size-select-actual",
            'content' => "\FC69",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-size-select-large",
            'content' => "\FC6A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "image-size-select-small",
            'content' => "\FC6B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "import",
            'content' => "\F2FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox",
            'content' => "\F686",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-arrow-down",
            'content' => "\F2FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-arrow-down-outline",
            'content' => "\F029B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-arrow-up",
            'content' => "\F3D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-arrow-up-outline",
            'content' => "\F029C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-full",
            'content' => "\F029D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-full-outline",
            'content' => "\F029E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-multiple",
            'content' => "\F8AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-multiple-outline",
            'content' => "\FB84",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "inbox-outline",
            'content' => "\F029F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "incognito",
            'content' => "\F5F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "infinity",
            'content' => "\F6E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "information",
            'content' => "\F2FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "information-outline",
            'content' => "\F2FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "information-variant",
            'content' => "\F64E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "instagram",
            'content' => "\F2FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "instapaper",
            'content' => "\F2FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "instrument-triangle",
            'content' => "\F0070",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "internet-explorer",
            'content' => "\F300",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "invert-colors",
            'content' => "\F301",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "invert-colors-off",
            'content' => "\FE8B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iobroker",
            'content' => "\F0313",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ip",
            'content' => "\FA5E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ip-network",
            'content' => "\FA5F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ip-network-outline",
            'content' => "\FC6C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ipod",
            'content' => "\FC6D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "islam",
            'content' => "\F978",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "island",
            'content' => "\F0071",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "itunes",
            'content' => "\F676",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "iv-bag",
            'content' => "\F00E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jabber",
            'content' => "\FDB1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jeepney",
            'content' => "\F302",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jellyfish",
            'content' => "\FF1E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jellyfish-outline",
            'content' => "\FF1F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jira",
            'content' => "\F303",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jquery",
            'content' => "\F87C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jsfiddle",
            'content' => "\F304",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "json",
            'content' => "\F626",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "judaism",
            'content' => "\F979",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "jump-rope",
            'content' => "\F032A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kabaddi",
            'content' => "\FD63",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "karate",
            'content' => "\F82B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keg",
            'content' => "\F305",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle",
            'content' => "\F5FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-alert",
            'content' => "\F0342",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-alert-outline",
            'content' => "\F0343",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-off",
            'content' => "\F0346",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-off-outline",
            'content' => "\F0347",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-outline",
            'content' => "\FF73",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-steam",
            'content' => "\F0344",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettle-steam-outline",
            'content' => "\F0345",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kettlebell",
            'content' => "\F032B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key",
            'content' => "\F306",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-arrow-right",
            'content' => "\F033D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-change",
            'content' => "\F307",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-link",
            'content' => "\F01CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-minus",
            'content' => "\F308",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-outline",
            'content' => "\FDB2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-plus",
            'content' => "\F309",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-remove",
            'content' => "\F30A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-star",
            'content' => "\F01C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-variant",
            'content' => "\F30B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "key-wireless",
            'content' => "\FFE2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard",
            'content' => "\F30C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-backspace",
            'content' => "\F30D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-caps",
            'content' => "\F30E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-close",
            'content' => "\F30F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-esc",
            'content' => "\F02E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f1",
            'content' => "\F02D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f10",
            'content' => "\F02DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f11",
            'content' => "\F02E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f12",
            'content' => "\F02E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f2",
            'content' => "\F02D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f3",
            'content' => "\F02D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f4",
            'content' => "\F02D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f5",
            'content' => "\F02DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f6",
            'content' => "\F02DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f7",
            'content' => "\F02DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f8",
            'content' => "\F02DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-f9",
            'content' => "\F02DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-off",
            'content' => "\F310",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-off-outline",
            'content' => "\FE8C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-outline",
            'content' => "\F97A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-return",
            'content' => "\F311",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-settings",
            'content' => "\F9F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-settings-outline",
            'content' => "\F9F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-space",
            'content' => "\F0072",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-tab",
            'content' => "\F312",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "keyboard-variant",
            'content' => "\F313",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "khanda",
            'content' => "\F0128",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kickstarter",
            'content' => "\F744",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "knife",
            'content' => "\F9FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "knife-military",
            'content' => "\F9FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kodi",
            'content' => "\F314",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kotlin",
            'content' => "\F0244",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "kubernetes",
            'content' => "\F0129",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label",
            'content' => "\F315",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-off",
            'content' => "\FACA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-off-outline",
            'content' => "\FACB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-outline",
            'content' => "\F316",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-percent",
            'content' => "\F0315",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-percent-outline",
            'content' => "\F0316",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-variant",
            'content' => "\FACC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "label-variant-outline",
            'content' => "\FACD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ladybug",
            'content' => "\F82C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lambda",
            'content' => "\F627",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lamp",
            'content' => "\F6B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lan",
            'content' => "\F317",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lan-check",
            'content' => "\F02D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lan-connect",
            'content' => "\F318",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lan-disconnect",
            'content' => "\F319",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lan-pending",
            'content' => "\F31A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-c",
            'content' => "\F671",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-cpp",
            'content' => "\F672",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-csharp",
            'content' => "\F31B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-css3",
            'content' => "\F31C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-fortran",
            'content' => "\F0245",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-go",
            'content' => "\F7D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-haskell",
            'content' => "\FC6E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-html5",
            'content' => "\F31D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-java",
            'content' => "\FB1C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-javascript",
            'content' => "\F31E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-lua",
            'content' => "\F8B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-php",
            'content' => "\F31F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-python",
            'content' => "\F320",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-python-text",
            'content' => "\F321",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-r",
            'content' => "\F7D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-ruby-on-rails",
            'content' => "\FACE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-swift",
            'content' => "\F6E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "language-typescript",
            'content' => "\F6E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "laptop",
            'content' => "\F322",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "laptop-chromebook",
            'content' => "\F323",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "laptop-mac",
            'content' => "\F324",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "laptop-off",
            'content' => "\F6E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "laptop-windows",
            'content' => "\F325",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "laravel",
            'content' => "\FACF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lasso",
            'content' => "\FF20",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lastfm",
            'content' => "\F326",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lastpass",
            'content' => "\F446",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "latitude",
            'content' => "\FF74",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "launch",
            'content' => "\F327",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lava-lamp",
            'content' => "\F7D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers",
            'content' => "\F328",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-minus",
            'content' => "\FE8D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-off",
            'content' => "\F329",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-off-outline",
            'content' => "\F9FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-outline",
            'content' => "\F9FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-plus",
            'content' => "\FE30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-remove",
            'content' => "\FE31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-search",
            'content' => "\F0231",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-search-outline",
            'content' => "\F0232",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-triple",
            'content' => "\FF75",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "layers-triple-outline",
            'content' => "\FF76",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lead-pencil",
            'content' => "\F64F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leaf",
            'content' => "\F32A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leaf-maple",
            'content' => "\FC6F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leaf-maple-off",
            'content' => "\F0305",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leaf-off",
            'content' => "\F0304",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leak",
            'content' => "\FDB3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leak-off",
            'content' => "\FDB4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-off",
            'content' => "\F32B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-on",
            'content' => "\F32C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-outline",
            'content' => "\F32D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-strip",
            'content' => "\F7D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-strip-variant",
            'content' => "\F0073",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-variant-off",
            'content' => "\F32E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-variant-on",
            'content' => "\F32F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "led-variant-outline",
            'content' => "\F330",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "leek",
            'content' => "\F01A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "less-than",
            'content' => "\F97B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "less-than-or-equal",
            'content' => "\F97C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library",
            'content' => "\F331",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library-books",
            'content' => "\F332",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library-movie",
            'content' => "\FCF4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library-music",
            'content' => "\F333",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library-music-outline",
            'content' => "\FF21",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library-shelves",
            'content' => "\FB85",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "library-video",
            'content' => "\FCF5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "license",
            'content' => "\FFE3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lifebuoy",
            'content' => "\F87D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "light-switch",
            'content' => "\F97D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb",
            'content' => "\F335",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-cfl",
            'content' => "\F0233",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-cfl-off",
            'content' => "\F0234",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-cfl-spiral",
            'content' => "\F02A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-cfl-spiral-off",
            'content' => "\F02EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-group",
            'content' => "\F027E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-group-off",
            'content' => "\F02F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-group-off-outline",
            'content' => "\F02F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-group-outline",
            'content' => "\F027F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-multiple",
            'content' => "\F0280",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-multiple-off",
            'content' => "\F02FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-multiple-off-outline",
            'content' => "\F02FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-multiple-outline",
            'content' => "\F0281",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-off",
            'content' => "\FE32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-off-outline",
            'content' => "\FE33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-on",
            'content' => "\F6E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-on-outline",
            'content' => "\F6E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lightbulb-outline",
            'content' => "\F336",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lighthouse",
            'content' => "\F9FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lighthouse-on",
            'content' => "\F9FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link",
            'content' => "\F337",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-box",
            'content' => "\FCF6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-box-outline",
            'content' => "\FCF7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-box-variant",
            'content' => "\FCF8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-box-variant-outline",
            'content' => "\FCF9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-lock",
            'content' => "\F00E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-off",
            'content' => "\F338",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-plus",
            'content' => "\FC70",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-variant",
            'content' => "\F339",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-variant-minus",
            'content' => "\F012A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-variant-off",
            'content' => "\F33A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-variant-plus",
            'content' => "\F012B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "link-variant-remove",
            'content' => "\F012C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "linkedin",
            'content' => "\F33B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "linkedin-box",
            'content' => "\F33C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "linux",
            'content' => "\F33D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "linux-mint",
            'content' => "\F8EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "litecoin",
            'content' => "\FA60",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "loading",
            'content' => "\F771",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "location-enter",
            'content' => "\FFE4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "location-exit",
            'content' => "\FFE5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock",
            'content' => "\F33E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-alert",
            'content' => "\F8ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-clock",
            'content' => "\F97E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-open",
            'content' => "\F33F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-open-outline",
            'content' => "\F340",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-open-variant",
            'content' => "\FFE6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-open-variant-outline",
            'content' => "\FFE7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-outline",
            'content' => "\F341",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-pattern",
            'content' => "\F6E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-plus",
            'content' => "\F5FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-question",
            'content' => "\F8EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-reset",
            'content' => "\F772",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lock-smart",
            'content' => "\F8B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "locker",
            'content' => "\F7D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "locker-multiple",
            'content' => "\F7D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "login",
            'content' => "\F342",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "login-variant",
            'content' => "\F5FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "logout",
            'content' => "\F343",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "logout-variant",
            'content' => "\F5FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "longitude",
            'content' => "\FF77",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "looks",
            'content' => "\F344",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "loupe",
            'content' => "\F345",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lumx",
            'content' => "\F346",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lungs",
            'content' => "\F00AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "lyft",
            'content' => "\FB1D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnet",
            'content' => "\F347",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnet-on",
            'content' => "\F348",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify",
            'content' => "\F349",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-close",
            'content' => "\F97F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-minus",
            'content' => "\F34A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-minus-cursor",
            'content' => "\FA61",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-minus-outline",
            'content' => "\F6EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-plus",
            'content' => "\F34B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-plus-cursor",
            'content' => "\FA62",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-plus-outline",
            'content' => "\F6EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-remove-cursor",
            'content' => "\F0237",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-remove-outline",
            'content' => "\F0238",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "magnify-scan",
            'content' => "\F02A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mail",
            'content' => "\FED8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mail-ru",
            'content' => "\F34C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox",
            'content' => "\F6ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-open",
            'content' => "\FD64",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-open-outline",
            'content' => "\FD65",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-open-up",
            'content' => "\FD66",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-open-up-outline",
            'content' => "\FD67",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-outline",
            'content' => "\FD68",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-up",
            'content' => "\FD69",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mailbox-up-outline",
            'content' => "\FD6A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map",
            'content' => "\F34D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-check",
            'content' => "\FED9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-check-outline",
            'content' => "\FEDA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-clock",
            'content' => "\FCFA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-clock-outline",
            'content' => "\FCFB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-legend",
            'content' => "\FA00",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker",
            'content' => "\F34E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-alert",
            'content' => "\FF22",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-alert-outline",
            'content' => "\FF23",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-check",
            'content' => "\FC71",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-check-outline",
            'content' => "\F0326",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-circle",
            'content' => "\F34F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-distance",
            'content' => "\F8EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-down",
            'content' => "\F012D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-left",
            'content' => "\F0306",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-left-outline",
            'content' => "\F0308",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-minus",
            'content' => "\F650",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-minus-outline",
            'content' => "\F0324",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-multiple",
            'content' => "\F350",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-multiple-outline",
            'content' => "\F02A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-off",
            'content' => "\F351",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-off-outline",
            'content' => "\F0328",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-outline",
            'content' => "\F7D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-path",
            'content' => "\FCFC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-plus",
            'content' => "\F651",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-plus-outline",
            'content' => "\F0323",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-question",
            'content' => "\FF24",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-question-outline",
            'content' => "\FF25",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-radius",
            'content' => "\F352",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-radius-outline",
            'content' => "\F0327",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-remove",
            'content' => "\FF26",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-remove-outline",
            'content' => "\F0325",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-remove-variant",
            'content' => "\FF27",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-right",
            'content' => "\F0307",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-right-outline",
            'content' => "\F0309",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-marker-up",
            'content' => "\F012E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-minus",
            'content' => "\F980",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-outline",
            'content' => "\F981",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-plus",
            'content' => "\F982",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-search",
            'content' => "\F983",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "map-search-outline",
            'content' => "\F984",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mapbox",
            'content' => "\FB86",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "margin",
            'content' => "\F353",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "markdown",
            'content' => "\F354",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "markdown-outline",
            'content' => "\FF78",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "marker",
            'content' => "\F652",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "marker-cancel",
            'content' => "\FDB5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "marker-check",
            'content' => "\F355",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mastodon",
            'content' => "\FAD0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mastodon-variant",
            'content' => "\FAD1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "material-design",
            'content' => "\F985",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "material-ui",
            'content' => "\F357",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-compass",
            'content' => "\F358",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-cos",
            'content' => "\FC72",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-integral",
            'content' => "\FFE8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-integral-box",
            'content' => "\FFE9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-log",
            'content' => "\F00B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-norm",
            'content' => "\FFEA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-norm-box",
            'content' => "\FFEB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-sin",
            'content' => "\FC73",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "math-tan",
            'content' => "\FC74",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "matrix",
            'content' => "\F628",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "medal",
            'content' => "\F986",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "medical-bag",
            'content' => "\F6EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "meditation",
            'content' => "\F01A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "medium",
            'content' => "\F35A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "meetup",
            'content' => "\FAD2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "memory",
            'content' => "\F35B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu",
            'content' => "\F35C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-down",
            'content' => "\F35D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-down-outline",
            'content' => "\F6B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-left",
            'content' => "\F35E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-left-outline",
            'content' => "\FA01",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-open",
            'content' => "\FB87",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-right",
            'content' => "\F35F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-right-outline",
            'content' => "\FA02",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-swap",
            'content' => "\FA63",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-swap-outline",
            'content' => "\FA64",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-up",
            'content' => "\F360",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "menu-up-outline",
            'content' => "\F6B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "merge",
            'content' => "\FF79",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message",
            'content' => "\F361",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-alert",
            'content' => "\F362",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-alert-outline",
            'content' => "\FA03",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-arrow-left",
            'content' => "\F031D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-arrow-left-outline",
            'content' => "\F031E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-arrow-right",
            'content' => "\F031F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-arrow-right-outline",
            'content' => "\F0320",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-bulleted",
            'content' => "\F6A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-bulleted-off",
            'content' => "\F6A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-draw",
            'content' => "\F363",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-image",
            'content' => "\F364",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-image-outline",
            'content' => "\F0197",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-lock",
            'content' => "\FFEC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-lock-outline",
            'content' => "\F0198",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-minus",
            'content' => "\F0199",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-minus-outline",
            'content' => "\F019A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-outline",
            'content' => "\F365",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-plus",
            'content' => "\F653",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-plus-outline",
            'content' => "\F00E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-processing",
            'content' => "\F366",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-processing-outline",
            'content' => "\F019B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-reply",
            'content' => "\F367",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-reply-text",
            'content' => "\F368",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-settings",
            'content' => "\F6EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-settings-outline",
            'content' => "\F019C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-settings-variant",
            'content' => "\F6F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-settings-variant-outline",
            'content' => "\F019D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-text",
            'content' => "\F369",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-text-clock",
            'content' => "\F019E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-text-clock-outline",
            'content' => "\F019F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-text-lock",
            'content' => "\FFED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-text-lock-outline",
            'content' => "\F01A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-text-outline",
            'content' => "\F36A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "message-video",
            'content' => "\F36B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "meteor",
            'content' => "\F629",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "metronome",
            'content' => "\F7D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "metronome-tick",
            'content' => "\F7DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "micro-sd",
            'content' => "\F7DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone",
            'content' => "\F36C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-minus",
            'content' => "\F8B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-off",
            'content' => "\F36D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-outline",
            'content' => "\F36E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-plus",
            'content' => "\F8B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-settings",
            'content' => "\F36F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-variant",
            'content' => "\F370",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microphone-variant-off",
            'content' => "\F371",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microscope",
            'content' => "\F654",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microsoft",
            'content' => "\F372",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microsoft-dynamics",
            'content' => "\F987",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "microwave",
            'content' => "\FC75",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "middleware",
            'content' => "\FF7A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "middleware-outline",
            'content' => "\FF7B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "midi",
            'content' => "\F8F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "midi-port",
            'content' => "\F8F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mine",
            'content' => "\FDB6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minecraft",
            'content' => "\F373",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mini-sd",
            'content' => "\FA04",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minidisc",
            'content' => "\FA05",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus",
            'content' => "\F374",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-box",
            'content' => "\F375",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-box-multiple",
            'content' => "\F016C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-box-multiple-outline",
            'content' => "\F016D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-box-outline",
            'content' => "\F6F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-circle",
            'content' => "\F376",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-circle-outline",
            'content' => "\F377",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-network",
            'content' => "\F378",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "minus-network-outline",
            'content' => "\FC76",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mirror",
            'content' => "\F0228",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mixcloud",
            'content' => "\F62A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mixed-martial-arts",
            'content' => "\FD6B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mixed-reality",
            'content' => "\F87E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mixer",
            'content' => "\F7DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "molecule",
            'content' => "\FB88",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor",
            'content' => "\F379",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-cellphone",
            'content' => "\F988",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-cellphone-star",
            'content' => "\F989",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-clean",
            'content' => "\F012F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-dashboard",
            'content' => "\FA06",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-edit",
            'content' => "\F02F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-lock",
            'content' => "\FDB7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-multiple",
            'content' => "\F37A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-off",
            'content' => "\FD6C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-screenshot",
            'content' => "\FE34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-speaker",
            'content' => "\FF7C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-speaker-off",
            'content' => "\FF7D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "monitor-star",
            'content' => "\FDB8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-first-quarter",
            'content' => "\FF7E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-full",
            'content' => "\FF7F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-last-quarter",
            'content' => "\FF80",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-new",
            'content' => "\FF81",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-waning-crescent",
            'content' => "\FF82",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-waning-gibbous",
            'content' => "\FF83",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-waxing-crescent",
            'content' => "\FF84",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moon-waxing-gibbous",
            'content' => "\FF85",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "moped",
            'content' => "\F00B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "more",
            'content' => "\F37B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mother-heart",
            'content' => "\F033F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mother-nurse",
            'content' => "\FCFD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "motion-sensor",
            'content' => "\FD6D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "motorbike",
            'content' => "\F37C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mouse",
            'content' => "\F37D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mouse-bluetooth",
            'content' => "\F98A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mouse-off",
            'content' => "\F37E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mouse-variant",
            'content' => "\F37F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mouse-variant-off",
            'content' => "\F380",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "move-resize",
            'content' => "\F655",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "move-resize-variant",
            'content' => "\F656",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie",
            'content' => "\F381",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-edit",
            'content' => "\F014D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-edit-outline",
            'content' => "\F014E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-filter",
            'content' => "\F014F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-filter-outline",
            'content' => "\F0150",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-open",
            'content' => "\FFEE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-open-outline",
            'content' => "\FFEF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-outline",
            'content' => "\FDB9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-roll",
            'content' => "\F7DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-search",
            'content' => "\F01FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "movie-search-outline",
            'content' => "\F01FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "muffin",
            'content' => "\F98B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "multiplication",
            'content' => "\F382",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "multiplication-box",
            'content' => "\F383",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mushroom",
            'content' => "\F7DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "mushroom-outline",
            'content' => "\F7DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music",
            'content' => "\F759",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-accidental-double-flat",
            'content' => "\FF86",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-accidental-double-sharp",
            'content' => "\FF87",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-accidental-flat",
            'content' => "\FF88",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-accidental-natural",
            'content' => "\FF89",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-accidental-sharp",
            'content' => "\FF8A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-box",
            'content' => "\F384",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-box-outline",
            'content' => "\F385",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-circle",
            'content' => "\F386",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-circle-outline",
            'content' => "\FAD3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-clef-alto",
            'content' => "\FF8B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-clef-bass",
            'content' => "\FF8C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-clef-treble",
            'content' => "\FF8D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note",
            'content' => "\F387",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-bluetooth",
            'content' => "\F5FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-bluetooth-off",
            'content' => "\F5FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-eighth",
            'content' => "\F388",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-eighth-dotted",
            'content' => "\FF8E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-half",
            'content' => "\F389",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-half-dotted",
            'content' => "\FF8F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-off",
            'content' => "\F38A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-off-outline",
            'content' => "\FF90",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-outline",
            'content' => "\FF91",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-plus",
            'content' => "\FDBA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-quarter",
            'content' => "\F38B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-quarter-dotted",
            'content' => "\FF92",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-sixteenth",
            'content' => "\F38C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-sixteenth-dotted",
            'content' => "\FF93",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-whole",
            'content' => "\F38D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-note-whole-dotted",
            'content' => "\FF94",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-off",
            'content' => "\F75A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-rest-eighth",
            'content' => "\FF95",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-rest-half",
            'content' => "\FF96",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-rest-quarter",
            'content' => "\FF97",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-rest-sixteenth",
            'content' => "\FF98",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "music-rest-whole",
            'content' => "\FF99",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nail",
            'content' => "\FDBB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nas",
            'content' => "\F8F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nativescript",
            'content' => "\F87F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nature",
            'content' => "\F38E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nature-people",
            'content' => "\F38F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "navigation",
            'content' => "\F390",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "near-me",
            'content' => "\F5CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "necklace",
            'content' => "\FF28",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "needle",
            'content' => "\F391",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "netflix",
            'content' => "\F745",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network",
            'content' => "\F6F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-off",
            'content' => "\FC77",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-off-outline",
            'content' => "\FC78",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-outline",
            'content' => "\FC79",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-router",
            'content' => "\F00B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-1",
            'content' => "\F8F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-1-alert",
            'content' => "\F8F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-2",
            'content' => "\F8F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-2-alert",
            'content' => "\F8F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-3",
            'content' => "\F8F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-3-alert",
            'content' => "\F8F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-4",
            'content' => "\F8F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-4-alert",
            'content' => "\F8FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-off",
            'content' => "\F8FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-off-outline",
            'content' => "\F8FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "network-strength-outline",
            'content' => "\F8FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "new-box",
            'content' => "\F394",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper",
            'content' => "\F395",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper-minus",
            'content' => "\FF29",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper-plus",
            'content' => "\FF2A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper-variant",
            'content' => "\F0023",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper-variant-multiple",
            'content' => "\F0024",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper-variant-multiple-outline",
            'content' => "\F0025",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "newspaper-variant-outline",
            'content' => "\F0026",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nfc",
            'content' => "\F396",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nfc-off",
            'content' => "\FE35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nfc-search-variant",
            'content' => "\FE36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nfc-tap",
            'content' => "\F397",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nfc-variant",
            'content' => "\F398",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nfc-variant-off",
            'content' => "\FE37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ninja",
            'content' => "\F773",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nintendo-switch",
            'content' => "\F7E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nix",
            'content' => "\F0130",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nodejs",
            'content' => "\F399",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "noodles",
            'content' => "\F01A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "not-equal",
            'content' => "\F98C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "not-equal-variant",
            'content' => "\F98D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note",
            'content' => "\F39A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-multiple",
            'content' => "\F6B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-multiple-outline",
            'content' => "\F6B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-outline",
            'content' => "\F39B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-plus",
            'content' => "\F39C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-plus-outline",
            'content' => "\F39D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-text",
            'content' => "\F39E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "note-text-outline",
            'content' => "\F0202",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "notebook",
            'content' => "\F82D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "notebook-multiple",
            'content' => "\FE38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "notebook-outline",
            'content' => "\FEDC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "notification-clear-all",
            'content' => "\F39F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "npm",
            'content' => "\F6F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "npm-variant",
            'content' => "\F98E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "npm-variant-outline",
            'content' => "\F98F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nuke",
            'content' => "\F6A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "null",
            'content' => "\F7E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric",
            'content' => "\F3A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0",
            'content' => "\30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0-box",
            'content' => "\F3A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0-box-multiple",
            'content' => "\FF2B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0-box-multiple-outline",
            'content' => "\F3A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0-box-outline",
            'content' => "\F3A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0-circle",
            'content' => "\FC7A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-0-circle-outline",
            'content' => "\FC7B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1",
            'content' => "\31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1-box",
            'content' => "\F3A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1-box-multiple",
            'content' => "\FF2C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1-box-multiple-outline",
            'content' => "\F3A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1-box-outline",
            'content' => "\F3A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1-circle",
            'content' => "\FC7C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-1-circle-outline",
            'content' => "\FC7D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10",
            'content' => "\F000A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10-box",
            'content' => "\FF9A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10-box-multiple",
            'content' => "\F000B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10-box-multiple-outline",
            'content' => "\F000C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10-box-outline",
            'content' => "\FF9B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10-circle",
            'content' => "\F000D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-10-circle-outline",
            'content' => "\F000E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2",
            'content' => "\32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2-box",
            'content' => "\F3A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2-box-multiple",
            'content' => "\FF2D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2-box-multiple-outline",
            'content' => "\F3A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2-box-outline",
            'content' => "\F3A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2-circle",
            'content' => "\FC7E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-2-circle-outline",
            'content' => "\FC7F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3",
            'content' => "\33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3-box",
            'content' => "\F3AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3-box-multiple",
            'content' => "\FF2E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3-box-multiple-outline",
            'content' => "\F3AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3-box-outline",
            'content' => "\F3AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3-circle",
            'content' => "\FC80",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-3-circle-outline",
            'content' => "\FC81",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4",
            'content' => "\34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4-box",
            'content' => "\F3AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4-box-multiple",
            'content' => "\FF2F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4-box-multiple-outline",
            'content' => "\F3AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4-box-outline",
            'content' => "\F3AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4-circle",
            'content' => "\FC82",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-4-circle-outline",
            'content' => "\FC83",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5",
            'content' => "\35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5-box",
            'content' => "\F3B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5-box-multiple",
            'content' => "\FF30",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5-box-multiple-outline",
            'content' => "\F3B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5-box-outline",
            'content' => "\F3B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5-circle",
            'content' => "\FC84",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-5-circle-outline",
            'content' => "\FC85",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6",
            'content' => "\36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6-box",
            'content' => "\F3B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6-box-multiple",
            'content' => "\FF31",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6-box-multiple-outline",
            'content' => "\F3B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6-box-outline",
            'content' => "\F3B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6-circle",
            'content' => "\FC86",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-6-circle-outline",
            'content' => "\FC87",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7",
            'content' => "\37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7-box",
            'content' => "\F3B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7-box-multiple",
            'content' => "\FF32",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7-box-multiple-outline",
            'content' => "\F3B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7-box-outline",
            'content' => "\F3B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7-circle",
            'content' => "\FC88",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-7-circle-outline",
            'content' => "\FC89",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8",
            'content' => "\38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8-box",
            'content' => "\F3B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8-box-multiple",
            'content' => "\FF33",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8-box-multiple-outline",
            'content' => "\F3BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8-box-outline",
            'content' => "\F3BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8-circle",
            'content' => "\FC8A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-8-circle-outline",
            'content' => "\FC8B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9",
            'content' => "\39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-box",
            'content' => "\F3BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-box-multiple",
            'content' => "\FF34",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-box-multiple-outline",
            'content' => "\F3BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-box-outline",
            'content' => "\F3BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-circle",
            'content' => "\FC8C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-circle-outline",
            'content' => "\FC8D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus",
            'content' => "\F000F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus-box",
            'content' => "\F3BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus-box-multiple",
            'content' => "\FF35",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus-box-multiple-outline",
            'content' => "\F3C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus-box-outline",
            'content' => "\F3C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus-circle",
            'content' => "\FC8E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-9-plus-circle-outline",
            'content' => "\FC8F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "numeric-negative-1",
            'content' => "\F0074",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nut",
            'content' => "\F6F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nutrition",
            'content' => "\F3C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "nuxt",
            'content' => "\F0131",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "oar",
            'content' => "\F67B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ocarina",
            'content' => "\FDBC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "oci",
            'content' => "\F0314",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ocr",
            'content' => "\F0165",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "octagon",
            'content' => "\F3C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "octagon-outline",
            'content' => "\F3C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "octagram",
            'content' => "\F6F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "octagram-outline",
            'content' => "\F774",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "odnoklassniki",
            'content' => "\F3C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "offer",
            'content' => "\F0246",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "office",
            'content' => "\F3C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "office-building",
            'content' => "\F990",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "oil",
            'content' => "\F3C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "oil-lamp",
            'content' => "\FF36",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "oil-level",
            'content' => "\F0075",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "oil-temperature",
            'content' => "\F0019",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "omega",
            'content' => "\F3C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "one-up",
            'content' => "\FB89",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "onedrive",
            'content' => "\F3CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "onenote",
            'content' => "\F746",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "onepassword",
            'content' => "\F880",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "opacity",
            'content' => "\F5CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "open-in-app",
            'content' => "\F3CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "open-in-new",
            'content' => "\F3CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "open-source-initiative",
            'content' => "\FB8A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "openid",
            'content' => "\F3CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "opera",
            'content' => "\F3CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "orbit",
            'content' => "\F018",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "origin",
            'content' => "\FB2B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ornament",
            'content' => "\F3CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ornament-variant",
            'content' => "\F3D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "outdoor-lamp",
            'content' => "\F0076",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "outlook",
            'content' => "\FCFE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "overscan",
            'content' => "\F0027",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "owl",
            'content' => "\F3D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pac-man",
            'content' => "\FB8B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "package",
            'content' => "\F3D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "package-down",
            'content' => "\F3D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "package-up",
            'content' => "\F3D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "package-variant",
            'content' => "\F3D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "package-variant-closed",
            'content' => "\F3D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-first",
            'content' => "\F600",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-last",
            'content' => "\F601",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-layout-body",
            'content' => "\F6F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-layout-footer",
            'content' => "\F6FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-layout-header",
            'content' => "\F6FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-layout-header-footer",
            'content' => "\FF9C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-layout-sidebar-left",
            'content' => "\F6FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-layout-sidebar-right",
            'content' => "\F6FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-next",
            'content' => "\FB8C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-next-outline",
            'content' => "\FB8D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-previous",
            'content' => "\FB8E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "page-previous-outline",
            'content' => "\FB8F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "palette",
            'content' => "\F3D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "palette-advanced",
            'content' => "\F3D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "palette-outline",
            'content' => "\FE6C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "palette-swatch",
            'content' => "\F8B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "palm-tree",
            'content' => "\F0077",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan",
            'content' => "\FB90",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-bottom-left",
            'content' => "\FB91",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-bottom-right",
            'content' => "\FB92",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-down",
            'content' => "\FB93",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-horizontal",
            'content' => "\FB94",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-left",
            'content' => "\FB95",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-right",
            'content' => "\FB96",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-top-left",
            'content' => "\FB97",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-top-right",
            'content' => "\FB98",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-up",
            'content' => "\FB99",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pan-vertical",
            'content' => "\FB9A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "panda",
            'content' => "\F3DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pandora",
            'content' => "\F3DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "panorama",
            'content' => "\F3DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "panorama-fisheye",
            'content' => "\F3DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "panorama-horizontal",
            'content' => "\F3DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "panorama-vertical",
            'content' => "\F3DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "panorama-wide-angle",
            'content' => "\F3E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paper-cut-vertical",
            'content' => "\F3E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paper-roll",
            'content' => "\F0182",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paper-roll-outline",
            'content' => "\F0183",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paperclip",
            'content' => "\F3E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "parachute",
            'content' => "\FC90",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "parachute-outline",
            'content' => "\FC91",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "parking",
            'content' => "\F3E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "party-popper",
            'content' => "\F0078",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "passport",
            'content' => "\F7E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "passport-biometric",
            'content' => "\FDBD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pasta",
            'content' => "\F018B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "patio-heater",
            'content' => "\FF9D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "patreon",
            'content' => "\F881",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pause",
            'content' => "\F3E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pause-circle",
            'content' => "\F3E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pause-circle-outline",
            'content' => "\F3E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pause-octagon",
            'content' => "\F3E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pause-octagon-outline",
            'content' => "\F3E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paw",
            'content' => "\F3E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paw-off",
            'content' => "\F657",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "paypal",
            'content' => "\F882",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pdf-box",
            'content' => "\FE39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "peace",
            'content' => "\F883",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "peanut",
            'content' => "\F001E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "peanut-off",
            'content' => "\F001F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "peanut-off-outline",
            'content' => "\F0021",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "peanut-outline",
            'content' => "\F0020",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pen",
            'content' => "\F3EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pen-lock",
            'content' => "\FDBE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pen-minus",
            'content' => "\FDBF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pen-off",
            'content' => "\FDC0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pen-plus",
            'content' => "\FDC1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pen-remove",
            'content' => "\FDC2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil",
            'content' => "\F3EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-box",
            'content' => "\F3EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-box-multiple",
            'content' => "\F016F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-box-multiple-outline",
            'content' => "\F0170",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-box-outline",
            'content' => "\F3ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-circle",
            'content' => "\F6FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-circle-outline",
            'content' => "\F775",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-lock",
            'content' => "\F3EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-lock-outline",
            'content' => "\FDC3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-minus",
            'content' => "\FDC4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-minus-outline",
            'content' => "\FDC5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-off",
            'content' => "\F3EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-off-outline",
            'content' => "\FDC6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-outline",
            'content' => "\FC92",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-plus",
            'content' => "\FDC7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-plus-outline",
            'content' => "\FDC8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-remove",
            'content' => "\FDC9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pencil-remove-outline",
            'content' => "\FDCA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "penguin",
            'content' => "\FEDD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pentagon",
            'content' => "\F6FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pentagon-outline",
            'content' => "\F700",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "percent",
            'content' => "\F3F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "percent-outline",
            'content' => "\F02A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "periodic-table",
            'content' => "\F8B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "periodic-table-co",
            'content' => "\F0329",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "periodic-table-co2",
            'content' => "\F7E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "periscope",
            'content' => "\F747",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "perspective-less",
            'content' => "\FCFF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "perspective-more",
            'content' => "\FD00",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pharmacy",
            'content' => "\F3F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone",
            'content' => "\F3F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-alert",
            'content' => "\FF37",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-alert-outline",
            'content' => "\F01B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-bluetooth",
            'content' => "\F3F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-bluetooth-outline",
            'content' => "\F01BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-cancel",
            'content' => "\F00E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-cancel-outline",
            'content' => "\F01BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-check",
            'content' => "\F01D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-check-outline",
            'content' => "\F01D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-classic",
            'content' => "\F602",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-classic-off",
            'content' => "\F02A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-forward",
            'content' => "\F3F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-forward-outline",
            'content' => "\F01BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-hangup",
            'content' => "\F3F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-hangup-outline",
            'content' => "\F01BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-in-talk",
            'content' => "\F3F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-in-talk-outline",
            'content' => "\F01AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-incoming",
            'content' => "\F3F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-incoming-outline",
            'content' => "\F01BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-lock",
            'content' => "\F3F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-lock-outline",
            'content' => "\F01BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-log",
            'content' => "\F3F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-log-outline",
            'content' => "\F01C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-message",
            'content' => "\F01C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-message-outline",
            'content' => "\F01C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-minus",
            'content' => "\F658",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-minus-outline",
            'content' => "\F01C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-missed",
            'content' => "\F3FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-missed-outline",
            'content' => "\F01D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-off",
            'content' => "\FDCB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-off-outline",
            'content' => "\F01D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-outgoing",
            'content' => "\F3FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-outgoing-outline",
            'content' => "\F01C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-outline",
            'content' => "\FDCC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-paused",
            'content' => "\F3FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-paused-outline",
            'content' => "\F01C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-plus",
            'content' => "\F659",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-plus-outline",
            'content' => "\F01C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-return",
            'content' => "\F82E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-return-outline",
            'content' => "\F01C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-ring",
            'content' => "\F01D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-ring-outline",
            'content' => "\F01D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-rotate-landscape",
            'content' => "\F884",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-rotate-portrait",
            'content' => "\F885",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-settings",
            'content' => "\F3FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-settings-outline",
            'content' => "\F01C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "phone-voip",
            'content' => "\F3FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pi",
            'content' => "\F3FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pi-box",
            'content' => "\F400",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pi-hole",
            'content' => "\FDCD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "piano",
            'content' => "\F67C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pickaxe",
            'content' => "\F8B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "picture-in-picture-bottom-right",
            'content' => "\FE3A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "picture-in-picture-bottom-right-outline",
            'content' => "\FE3B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "picture-in-picture-top-right",
            'content' => "\FE3C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "picture-in-picture-top-right-outline",
            'content' => "\FE3D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pier",
            'content' => "\F886",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pier-crane",
            'content' => "\F887",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pig",
            'content' => "\F401",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pig-variant",
            'content' => "\F0028",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "piggy-bank",
            'content' => "\F0029",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pill",
            'content' => "\F402",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pillar",
            'content' => "\F701",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pin",
            'content' => "\F403",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pin-off",
            'content' => "\F404",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pin-off-outline",
            'content' => "\F92F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pin-outline",
            'content' => "\F930",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pine-tree",
            'content' => "\F405",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pine-tree-box",
            'content' => "\F406",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pinterest",
            'content' => "\F407",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pinterest-box",
            'content' => "\F408",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pinwheel",
            'content' => "\FAD4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pinwheel-outline",
            'content' => "\FAD5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pipe",
            'content' => "\F7E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pipe-disconnected",
            'content' => "\F7E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pipe-leak",
            'content' => "\F888",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pirate",
            'content' => "\FA07",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pistol",
            'content' => "\F702",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "piston",
            'content' => "\F889",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pizza",
            'content' => "\F409",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play",
            'content' => "\F40A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-box",
            'content' => "\F02A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-box-outline",
            'content' => "\F40B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-circle",
            'content' => "\F40C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-circle-outline",
            'content' => "\F40D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-network",
            'content' => "\F88A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-network-outline",
            'content' => "\FC93",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-outline",
            'content' => "\FF38",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-pause",
            'content' => "\F40E",
        ]);

            factory(App\MaterialIcon::class)->create([
            'name' => "play-protected-content",
            'content' => "\F40F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "play-speed",
            'content' => "\F8FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-check",
            'content' => "\F5C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-edit",
            'content' => "\F8FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-minus",
            'content' => "\F410",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-music",
            'content' => "\FC94",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-music-outline",
            'content' => "\FC95",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-play",
            'content' => "\F411",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-plus",
            'content' => "\F412",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-remove",
            'content' => "\F413",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playlist-star",
            'content' => "\FDCE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "playstation",
            'content' => "\F414",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plex",
            'content' => "\F6B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus",
            'content' => "\F415",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-box",
            'content' => "\F416",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-box-multiple",
            'content' => "\F334",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-box-multiple-outline",
            'content' => "\F016E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-box-outline",
            'content' => "\F703",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-circle",
            'content' => "\F417",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-circle-multiple-outline",
            'content' => "\F418",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-circle-outline",
            'content' => "\F419",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-minus",
            'content' => "\F991",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-minus-box",
            'content' => "\F992",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-network",
            'content' => "\F41A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-network-outline",
            'content' => "\FC96",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-one",
            'content' => "\F41B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-outline",
            'content' => "\F704",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "plus-thick",
            'content' => "\F0217",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pocket",
            'content' => "\F41C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "podcast",
            'content' => "\F993",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "podium",
            'content' => "\FD01",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "podium-bronze",
            'content' => "\FD02",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "podium-gold",
            'content' => "\FD03",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "podium-silver",
            'content' => "\FD04",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "point-of-sale",
            'content' => "\FD6E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pokeball",
            'content' => "\F41D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pokemon-go",
            'content' => "\FA08",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "poker-chip",
            'content' => "\F82F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "polaroid",
            'content' => "\F41E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "police-badge",
            'content' => "\F0192",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "police-badge-outline",
            'content' => "\F0193",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "poll",
            'content' => "\F41F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "poll-box",
            'content' => "\F420",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "poll-box-outline",
            'content' => "\F02A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "polymer",
            'content' => "\F421",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pool",
            'content' => "\F606",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "popcorn",
            'content' => "\F422",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "post",
            'content' => "\F002A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "post-outline",
            'content' => "\F002B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "postage-stamp",
            'content' => "\FC97",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pot",
            'content' => "\F65A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pot-mix",
            'content' => "\F65B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pound",
            'content' => "\F423",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pound-box",
            'content' => "\F424",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pound-box-outline",
            'content' => "\F01AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power",
            'content' => "\F425",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-cycle",
            'content' => "\F900",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-off",
            'content' => "\F901",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-on",
            'content' => "\F902",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-plug",
            'content' => "\F6A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-plug-off",
            'content' => "\F6A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-settings",
            'content' => "\F426",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-sleep",
            'content' => "\F903",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket",
            'content' => "\F427",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-au",
            'content' => "\F904",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-de",
            'content' => "\F0132",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-eu",
            'content' => "\F7E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-fr",
            'content' => "\F0133",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-jp",
            'content' => "\F0134",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-uk",
            'content' => "\F7E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-socket-us",
            'content' => "\F7E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "power-standby",
            'content' => "\F905",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "powershell",
            'content' => "\FA09",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "prescription",
            'content' => "\F705",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "presentation",
            'content' => "\F428",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "presentation-play",
            'content' => "\F429",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer",
            'content' => "\F42A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-3d",
            'content' => "\F42B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-3d-nozzle",
            'content' => "\FE3E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-3d-nozzle-alert",
            'content' => "\F01EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-3d-nozzle-alert-outline",
            'content' => "\F01EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-3d-nozzle-outline",
            'content' => "\FE3F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-alert",
            'content' => "\F42C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-check",
            'content' => "\F0171",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-off",
            'content' => "\FE40",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-pos",
            'content' => "\F0079",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-settings",
            'content' => "\F706",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "printer-wireless",
            'content' => "\FA0A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "priority-high",
            'content' => "\F603",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "priority-low",
            'content' => "\F604",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "professional-hexagon",
            'content' => "\F42D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-alert",
            'content' => "\FC98",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-check",
            'content' => "\F994",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-clock",
            'content' => "\F995",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-close",
            'content' => "\F0135",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-download",
            'content' => "\F996",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-upload",
            'content' => "\F997",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "progress-wrench",
            'content' => "\FC99",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "projector",
            'content' => "\F42E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "projector-screen",
            'content' => "\F42F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "protocol",
            'content' => "\FFF9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "publish",
            'content' => "\F6A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pulse",
            'content' => "\F430",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "pumpkin",
            'content' => "\FB9B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "purse",
            'content' => "\FF39",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "purse-outline",
            'content' => "\FF3A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "puzzle",
            'content' => "\F431",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "puzzle-outline",
            'content' => "\FA65",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qi",
            'content' => "\F998",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qqchat",
            'content' => "\F605",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qrcode",
            'content' => "\F432",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qrcode-edit",
            'content' => "\F8B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qrcode-minus",
            'content' => "\F01B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qrcode-plus",
            'content' => "\F01B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qrcode-remove",
            'content' => "\F01B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "qrcode-scan",
            'content' => "\F433",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "quadcopter",
            'content' => "\F434",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "quality-high",
            'content' => "\F435",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "quality-low",
            'content' => "\FA0B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "quality-medium",
            'content' => "\FA0C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "quicktime",
            'content' => "\F436",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "quora",
            'content' => "\FD05",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rabbit",
            'content' => "\F906",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "racing-helmet",
            'content' => "\FD6F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "racquetball",
            'content' => "\FD70",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radar",
            'content' => "\F437",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radiator",
            'content' => "\F438",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radiator-disabled",
            'content' => "\FAD6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radiator-off",
            'content' => "\FAD7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radio",
            'content' => "\F439",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radio-am",
            'content' => "\FC9A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radio-fm",
            'content' => "\FC9B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radio-handheld",
            'content' => "\F43A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radio-off",
            'content' => "\F0247",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radio-tower",
            'content' => "\F43B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radioactive",
            'content' => "\F43C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radioactive-off",
            'content' => "\FEDE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radiobox-blank",
            'content' => "\F43D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radiobox-marked",
            'content' => "\F43E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radius",
            'content' => "\FC9C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "radius-outline",
            'content' => "\FC9D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "railroad-light",
            'content' => "\FF3B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "raspberry-pi",
            'content' => "\F43F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ray-end",
            'content' => "\F440",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ray-end-arrow",
            'content' => "\F441",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ray-start",
            'content' => "\F442",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ray-start-arrow",
            'content' => "\F443",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ray-start-end",
            'content' => "\F444",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ray-vertex",
            'content' => "\F445",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "react",
            'content' => "\F707",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "read",
            'content' => "\F447",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "receipt",
            'content' => "\F449",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "record",
            'content' => "\F44A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "record-circle",
            'content' => "\FEDF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "record-circle-outline",
            'content' => "\FEE0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "record-player",
            'content' => "\F999",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "record-rec",
            'content' => "\F44B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rectangle",
            'content' => "\FE41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rectangle-outline",
            'content' => "\FE42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "recycle",
            'content' => "\F44C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reddit",
            'content' => "\F44D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "redhat",
            'content' => "\F0146",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "redo",
            'content' => "\F44E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "redo-variant",
            'content' => "\F44F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reflect-horizontal",
            'content' => "\FA0D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reflect-vertical",
            'content' => "\FA0E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "refresh",
            'content' => "\F450",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "regex",
            'content' => "\F451",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "registered-trademark",
            'content' => "\FA66",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "relative-scale",
            'content' => "\F452",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reload",
            'content' => "\F453",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reload-alert",
            'content' => "\F0136",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reminder",
            'content' => "\F88B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "remote",
            'content' => "\F454",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "remote-desktop",
            'content' => "\F8B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "remote-off",
            'content' => "\FEE1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "remote-tv",
            'content' => "\FEE2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "remote-tv-off",
            'content' => "\FEE3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rename-box",
            'content' => "\F455",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reorder-horizontal",
            'content' => "\F687",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reorder-vertical",
            'content' => "\F688",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "repeat",
            'content' => "\F456",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "repeat-off",
            'content' => "\F457",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "repeat-once",
            'content' => "\F458",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "replay",
            'content' => "\F459",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reply",
            'content' => "\F45A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reply-all",
            'content' => "\F45B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reply-all-outline",
            'content' => "\FF3C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reply-circle",
            'content' => "\F01D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reply-outline",
            'content' => "\FF3D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "reproduction",
            'content' => "\F45C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "resistor",
            'content' => "\FB1F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "resistor-nodes",
            'content' => "\FB20",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "resize",
            'content' => "\FA67",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "resize-bottom-right",
            'content' => "\F45D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "responsive",
            'content' => "\F45E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "restart",
            'content' => "\F708",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "restart-alert",
            'content' => "\F0137",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "restart-off",
            'content' => "\FD71",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "restore",
            'content' => "\F99A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "restore-alert",
            'content' => "\F0138",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rewind",
            'content' => "\F45F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rewind-10",
            'content' => "\FD06",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rewind-30",
            'content' => "\FD72",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rewind-5",
            'content' => "\F0224",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rewind-outline",
            'content' => "\F709",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rhombus",
            'content' => "\F70A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rhombus-medium",
            'content' => "\FA0F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rhombus-outline",
            'content' => "\F70B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rhombus-split",
            'content' => "\FA10",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ribbon",
            'content' => "\F460",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rice",
            'content' => "\F7E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ring",
            'content' => "\F7EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rivet",
            'content' => "\FE43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "road",
            'content' => "\F461",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "road-variant",
            'content' => "\F462",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robber",
            'content' => "\F007A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robot",
            'content' => "\F6A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robot-industrial",
            'content' => "\FB21",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robot-mower",
            'content' => "\F0222",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robot-mower-outline",
            'content' => "\F021E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robot-vacuum",
            'content' => "\F70C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "robot-vacuum-variant",
            'content' => "\F907",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rocket",
            'content' => "\F463",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roller-skate",
            'content' => "\FD07",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rollerblade",
            'content' => "\FD08",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rollupjs",
            'content' => "\FB9C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-1",
            'content' => "\F00B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-10",
            'content' => "\F00BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-2",
            'content' => "\F00B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-3",
            'content' => "\F00B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-4",
            'content' => "\F00B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-5",
            'content' => "\F00B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-6",
            'content' => "\F00B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-7",
            'content' => "\F00B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-8",
            'content' => "\F00BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "roman-numeral-9",
            'content' => "\F00BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "room-service",
            'content' => "\F88C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "room-service-outline",
            'content' => "\FD73",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-3d",
            'content' => "\FEE4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-3d-variant",
            'content' => "\F464",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-left",
            'content' => "\F465",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-left-variant",
            'content' => "\F466",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-orbit",
            'content' => "\FD74",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-right",
            'content' => "\F467",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rotate-right-variant",
            'content' => "\F468",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rounded-corner",
            'content' => "\F607",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "router",
            'content' => "\F020D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "router-wireless",
            'content' => "\F469",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "router-wireless-settings",
            'content' => "\FA68",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "routes",
            'content' => "\F46A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "routes-clock",
            'content' => "\F007B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rowing",
            'content' => "\F608",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rss",
            'content' => "\F46B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rss-box",
            'content' => "\F46C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rss-off",
            'content' => "\FF3E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ruby",
            'content' => "\FD09",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rugby",
            'content' => "\FD75",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ruler",
            'content' => "\F46D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ruler-square",
            'content' => "\FC9E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ruler-square-compass",
            'content' => "\FEDB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "run",
            'content' => "\F70D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "run-fast",
            'content' => "\F46E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "rv-truck",
            'content' => "\F01FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sack",
            'content' => "\FD0A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sack-percent",
            'content' => "\FD0B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "safe",
            'content' => "\FA69",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "safe-square",
            'content' => "\F02A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "safe-square-outline",
            'content' => "\F02A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "safety-goggles",
            'content' => "\FD0C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sailing",
            'content' => "\FEE5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sale",
            'content' => "\F46F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "salesforce",
            'content' => "\F88D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sass",
            'content' => "\F7EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "satellite",
            'content' => "\F470",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "satellite-uplink",
            'content' => "\F908",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "satellite-variant",
            'content' => "\F471",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sausage",
            'content' => "\F8B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "saw-blade",
            'content' => "\FE44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "saxophone",
            'content' => "\F609",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scale",
            'content' => "\F472",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scale-balance",
            'content' => "\F5D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scale-bathroom",
            'content' => "\F473",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scale-off",
            'content' => "\F007C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scanner",
            'content' => "\F6AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scanner-off",
            'content' => "\F909",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scatter-plot",
            'content' => "\FEE6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scatter-plot-outline",
            'content' => "\FEE7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "school",
            'content' => "\F474",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "school-outline",
            'content' => "\F01AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scissors-cutting",
            'content' => "\FA6A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scooter",
            'content' => "\F0214",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scoreboard",
            'content' => "\F02A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "scoreboard-outline",
            'content' => "\F02AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screen-rotation",
            'content' => "\F475",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screen-rotation-lock",
            'content' => "\F476",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screw-flat-top",
            'content' => "\FDCF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screw-lag",
            'content' => "\FE54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screw-machine-flat-top",
            'content' => "\FE55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screw-machine-round-top",
            'content' => "\FE56",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screw-round-top",
            'content' => "\FE57",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "screwdriver",
            'content' => "\F477",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "script",
            'content' => "\FB9D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "script-outline",
            'content' => "\F478",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "script-text",
            'content' => "\FB9E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "script-text-outline",
            'content' => "\FB9F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sd",
            'content' => "\F479",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seal",
            'content' => "\F47A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seal-variant",
            'content' => "\FFFA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "search-web",
            'content' => "\F70E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat",
            'content' => "\FC9F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-flat",
            'content' => "\F47B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-flat-angled",
            'content' => "\F47C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-individual-suite",
            'content' => "\F47D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-legroom-extra",
            'content' => "\F47E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-legroom-normal",
            'content' => "\F47F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-legroom-reduced",
            'content' => "\F480",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-outline",
            'content' => "\FCA0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-passenger",
            'content' => "\F0274",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-recline-extra",
            'content' => "\F481",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seat-recline-normal",
            'content' => "\F482",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seatbelt",
            'content' => "\FCA1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "security",
            'content' => "\F483",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "security-network",
            'content' => "\F484",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seed",
            'content' => "\FE45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "seed-outline",
            'content' => "\FE46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "segment",
            'content' => "\FEE8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select",
            'content' => "\F485",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-all",
            'content' => "\F486",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-color",
            'content' => "\FD0D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-compare",
            'content' => "\FAD8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-drag",
            'content' => "\FA6B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-group",
            'content' => "\FF9F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-inverse",
            'content' => "\F487",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-marker",
            'content' => "\F02AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-multiple",
            'content' => "\F02AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-multiple-marker",
            'content' => "\F02AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-off",
            'content' => "\F488",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-place",
            'content' => "\FFFB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "select-search",
            'content' => "\F022F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection",
            'content' => "\F489",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-drag",
            'content' => "\FA6C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-ellipse",
            'content' => "\FD0E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-ellipse-arrow-inside",
            'content' => "\FF3F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-marker",
            'content' => "\F02AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-multiple-marker",
            'content' => "\F02AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-mutliple",
            'content' => "\F02B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-off",
            'content' => "\F776",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "selection-search",
            'content' => "\F0230",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "semantic-web",
            'content' => "\F0341",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send",
            'content' => "\F48A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-check",
            'content' => "\F018C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-check-outline",
            'content' => "\F018D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-circle",
            'content' => "\FE58",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-circle-outline",
            'content' => "\FE59",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-clock",
            'content' => "\F018E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-clock-outline",
            'content' => "\F018F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-lock",
            'content' => "\F7EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-lock-outline",
            'content' => "\F0191",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "send-outline",
            'content' => "\F0190",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "serial-port",
            'content' => "\F65C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server",
            'content' => "\F48B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-minus",
            'content' => "\F48C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-network",
            'content' => "\F48D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-network-off",
            'content' => "\F48E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-off",
            'content' => "\F48F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-plus",
            'content' => "\F490",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-remove",
            'content' => "\F491",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "server-security",
            'content' => "\F492",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-all",
            'content' => "\F777",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-center",
            'content' => "\F778",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-center-right",
            'content' => "\F779",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-left",
            'content' => "\F77A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-left-center",
            'content' => "\F77B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-left-right",
            'content' => "\F77C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-none",
            'content' => "\F77D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-right",
            'content' => "\F77E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "set-top-box",
            'content' => "\F99E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "settings",
            'content' => "\F493",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "settings-box",
            'content' => "\F494",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "settings-helper",
            'content' => "\FA6D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "settings-outline",
            'content' => "\F8BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "settings-transfer",
            'content' => "\F007D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "settings-transfer-outline",
            'content' => "\F007E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shaker",
            'content' => "\F0139",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shaker-outline",
            'content' => "\F013A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape",
            'content' => "\F830",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-circle-plus",
            'content' => "\F65D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-outline",
            'content' => "\F831",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-oval-plus",
            'content' => "\F0225",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-plus",
            'content' => "\F495",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-polygon-plus",
            'content' => "\F65E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-rectangle-plus",
            'content' => "\F65F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shape-square-plus",
            'content' => "\F660",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share",
            'content' => "\F496",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-all",
            'content' => "\F021F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-all-outline",
            'content' => "\F0220",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-circle",
            'content' => "\F01D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-off",
            'content' => "\FF40",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-off-outline",
            'content' => "\FF41",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-outline",
            'content' => "\F931",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "share-variant",
            'content' => "\F497",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sheep",
            'content' => "\FCA2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield",
            'content' => "\F498",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-account",
            'content' => "\F88E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-account-outline",
            'content' => "\FA11",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-airplane",
            'content' => "\F6BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-airplane-outline",
            'content' => "\FCA3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-alert",
            'content' => "\FEE9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-alert-outline",
            'content' => "\FEEA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-car",
            'content' => "\FFA0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-check",
            'content' => "\F565",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-check-outline",
            'content' => "\FCA4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-cross",
            'content' => "\FCA5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-cross-outline",
            'content' => "\FCA6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-edit",
            'content' => "\F01CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-edit-outline",
            'content' => "\F01CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-half-full",
            'content' => "\F77F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-home",
            'content' => "\F689",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-home-outline",
            'content' => "\FCA7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-key",
            'content' => "\FBA0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-key-outline",
            'content' => "\FBA1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-link-variant",
            'content' => "\FD0F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-link-variant-outline",
            'content' => "\FD10",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-lock",
            'content' => "\F99C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-lock-outline",
            'content' => "\FCA8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-off",
            'content' => "\F99D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-off-outline",
            'content' => "\F99B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-outline",
            'content' => "\F499",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-plus",
            'content' => "\FAD9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-plus-outline",
            'content' => "\FADA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-refresh",
            'content' => "\F01CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-refresh-outline",
            'content' => "\F01CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-remove",
            'content' => "\FADB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-remove-outline",
            'content' => "\FADC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-search",
            'content' => "\FD76",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-star",
            'content' => "\F0166",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-star-outline",
            'content' => "\F0167",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-sun",
            'content' => "\F007F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shield-sun-outline",
            'content' => "\F0080",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ship-wheel",
            'content' => "\F832",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shoe-formal",
            'content' => "\FB22",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shoe-heel",
            'content' => "\FB23",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shoe-print",
            'content' => "\FE5A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shopify",
            'content' => "\FADD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shopping",
            'content' => "\F49A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shopping-music",
            'content' => "\F49B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shopping-outline",
            'content' => "\F0200",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shopping-search",
            'content' => "\FFA1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shovel",
            'content' => "\F70F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shovel-off",
            'content' => "\F710",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shower",
            'content' => "\F99F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shower-head",
            'content' => "\F9A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shredder",
            'content' => "\F49C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shuffle",
            'content' => "\F49D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shuffle-disabled",
            'content' => "\F49E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "shuffle-variant",
            'content' => "\F49F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sigma",
            'content' => "\F4A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sigma-lower",
            'content' => "\F62B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-caution",
            'content' => "\F4A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-direction",
            'content' => "\F780",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-direction-minus",
            'content' => "\F0022",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-direction-plus",
            'content' => "\FFFD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-direction-remove",
            'content' => "\FFFE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-real-estate",
            'content' => "\F0143",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sign-text",
            'content' => "\F781",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal",
            'content' => "\F4A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-2g",
            'content' => "\F711",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-3g",
            'content' => "\F712",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-4g",
            'content' => "\F713",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-5g",
            'content' => "\FA6E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-cellular-1",
            'content' => "\F8BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-cellular-2",
            'content' => "\F8BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-cellular-3",
            'content' => "\F8BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-cellular-outline",
            'content' => "\F8BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-distance-variant",
            'content' => "\FE47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-hspa",
            'content' => "\F714",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-hspa-plus",
            'content' => "\F715",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-off",
            'content' => "\F782",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signal-variant",
            'content' => "\F60A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signature",
            'content' => "\FE5B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signature-freehand",
            'content' => "\FE5C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signature-image",
            'content' => "\FE5D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "signature-text",
            'content' => "\FE5E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silo",
            'content' => "\FB24",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silverware",
            'content' => "\F4A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silverware-clean",
            'content' => "\FFFF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silverware-fork",
            'content' => "\F4A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silverware-fork-knife",
            'content' => "\FA6F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silverware-spoon",
            'content' => "\F4A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "silverware-variant",
            'content' => "\F4A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sim",
            'content' => "\F4A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sim-alert",
            'content' => "\F4A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sim-off",
            'content' => "\F4A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "simple-icons",
            'content' => "\F0348",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sina-weibo",
            'content' => "\FADE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sitemap",
            'content' => "\F4AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skate",
            'content' => "\FD11",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skew-less",
            'content' => "\FD12",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skew-more",
            'content' => "\FD13",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ski",
            'content' => "\F032F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ski-cross-country",
            'content' => "\F0330",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ski-water",
            'content' => "\F0331",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-backward",
            'content' => "\F4AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-backward-outline",
            'content' => "\FF42",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-forward",
            'content' => "\F4AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-forward-outline",
            'content' => "\FF43",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-next",
            'content' => "\F4AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-next-circle",
            'content' => "\F661",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-next-circle-outline",
            'content' => "\F662",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-next-outline",
            'content' => "\FF44",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-previous",
            'content' => "\F4AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-previous-circle",
            'content' => "\F663",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-previous-circle-outline",
            'content' => "\F664",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skip-previous-outline",
            'content' => "\FF45",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skull",
            'content' => "\F68B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skull-crossbones",
            'content' => "\FBA2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skull-crossbones-outline",
            'content' => "\FBA3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skull-outline",
            'content' => "\FBA4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skype",
            'content' => "\F4AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "skype-business",
            'content' => "\F4B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slack",
            'content' => "\F4B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slackware",
            'content' => "\F90A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slash-forward",
            'content' => "\F0000",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slash-forward-box",
            'content' => "\F0001",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sleep",
            'content' => "\F4B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sleep-off",
            'content' => "\F4B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slope-downhill",
            'content' => "\FE5F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slope-uphill",
            'content' => "\FE60",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slot-machine",
            'content' => "\F013F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "slot-machine-outline",
            'content' => "\F0140",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smart-card",
            'content' => "\F00E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smart-card-outline",
            'content' => "\F00E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smart-card-reader",
            'content' => "\F00EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smart-card-reader-outline",
            'content' => "\F00EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smog",
            'content' => "\FA70",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smoke-detector",
            'content' => "\F392",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smoking",
            'content' => "\F4B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "smoking-off",
            'content' => "\F4B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snapchat",
            'content' => "\F4B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snowboard",
            'content' => "\F0332",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snowflake",
            'content' => "\F716",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snowflake-alert",
            'content' => "\FF46",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snowflake-melt",
            'content' => "\F02F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snowflake-variant",
            'content' => "\FF47",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "snowman",
            'content' => "\F4B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "soccer",
            'content' => "\F4B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "soccer-field",
            'content' => "\F833",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sofa",
            'content' => "\F4B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "solar-panel",
            'content' => "\FD77",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "solar-panel-large",
            'content' => "\FD78",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "solar-power",
            'content' => "\FA71",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "soldering-iron",
            'content' => "\F00BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "solid",
            'content' => "\F68C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort",
            'content' => "\F4BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-alphabetical",
            'content' => "\F4BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-alphabetical-ascending",
            'content' => "\F0173",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-alphabetical-descending",
            'content' => "\F0174",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-ascending",
            'content' => "\F4BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-descending",
            'content' => "\F4BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-numeric",
            'content' => "\F4BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-variant",
            'content' => "\F4BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-variant-lock",
            'content' => "\FCA9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-variant-lock-open",
            'content' => "\FCAA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sort-variant-remove",
            'content' => "\F0172",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "soundcloud",
            'content' => "\F4C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-branch",
            'content' => "\F62C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit",
            'content' => "\F717",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit-end",
            'content' => "\F718",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit-end-local",
            'content' => "\F719",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit-local",
            'content' => "\F71A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit-next-local",
            'content' => "\F71B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit-start",
            'content' => "\F71C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-commit-start-next-local",
            'content' => "\F71D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-fork",
            'content' => "\F4C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-merge",
            'content' => "\F62D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-pull",
            'content' => "\F4C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-repository",
            'content' => "\FCAB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "source-repository-multiple",
            'content' => "\FCAC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "soy-sauce",
            'content' => "\F7ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spa",
            'content' => "\FCAD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spa-outline",
            'content' => "\FCAE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "space-invaders",
            'content' => "\FBA5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spade",
            'content' => "\FE48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speaker",
            'content' => "\F4C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speaker-bluetooth",
            'content' => "\F9A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speaker-multiple",
            'content' => "\FD14",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speaker-off",
            'content' => "\F4C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speaker-wireless",
            'content' => "\F71E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speedometer",
            'content' => "\F4C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speedometer-medium",
            'content' => "\FFA2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "speedometer-slow",
            'content' => "\FFA3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spellcheck",
            'content' => "\F4C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spider",
            'content' => "\F0215",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spider-thread",
            'content' => "\F0216",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spider-web",
            'content' => "\FBA6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spotify",
            'content' => "\F4C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spotlight",
            'content' => "\F4C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spotlight-beam",
            'content' => "\F4C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spray",
            'content' => "\F665",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "spray-bottle",
            'content' => "\FADF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sprinkler",
            'content' => "\F0081",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sprinkler-variant",
            'content' => "\F0082",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sprout",
            'content' => "\FE49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sprout-outline",
            'content' => "\FE4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square",
            'content' => "\F763",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-edit-outline",
            'content' => "\F90B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-inc",
            'content' => "\F4CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-inc-cash",
            'content' => "\F4CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-medium",
            'content' => "\FA12",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-medium-outline",
            'content' => "\FA13",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-off",
            'content' => "\F0319",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-off-outline",
            'content' => "\F031A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-outline",
            'content' => "\F762",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-root",
            'content' => "\F783",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-root-box",
            'content' => "\F9A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "square-small",
            'content' => "\FA14",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "squeegee",
            'content' => "\FAE0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ssh",
            'content' => "\F8BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stack-exchange",
            'content' => "\F60B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stack-overflow",
            'content' => "\F4CC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stackpath",
            'content' => "\F359",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stadium",
            'content' => "\F001A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stadium-variant",
            'content' => "\F71F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stairs",
            'content' => "\F4CD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stairs-down",
            'content' => "\F02E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stairs-up",
            'content' => "\F02E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stamper",
            'content' => "\FD15",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "standard-definition",
            'content' => "\F7EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star",
            'content' => "\F4CE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-box",
            'content' => "\FA72",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-box-multiple",
            'content' => "\F02B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-box-multiple-outline",
            'content' => "\F02B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-box-outline",
            'content' => "\FA73",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-circle",
            'content' => "\F4CF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-circle-outline",
            'content' => "\F9A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-face",
            'content' => "\F9A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-four-points",
            'content' => "\FAE1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-four-points-outline",
            'content' => "\FAE2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-half",
            'content' => "\F4D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-off",
            'content' => "\F4D1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-outline",
            'content' => "\F4D2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-three-points",
            'content' => "\FAE3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "star-three-points-outline",
            'content' => "\FAE4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "state-machine",
            'content' => "\F021A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "steam",
            'content' => "\F4D3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "steam-box",
            'content' => "\F90C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "steering",
            'content' => "\F4D4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "steering-off",
            'content' => "\F90D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "step-backward",
            'content' => "\F4D5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "step-backward-2",
            'content' => "\F4D6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "step-forward",
            'content' => "\F4D7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "step-forward-2",
            'content' => "\F4D8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stethoscope",
            'content' => "\F4D9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sticker",
            'content' => "\F5D0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sticker-emoji",
            'content' => "\F784",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stocking",
            'content' => "\F4DA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stomach",
            'content' => "\F00BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stop",
            'content' => "\F4DB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stop-circle",
            'content' => "\F666",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stop-circle-outline",
            'content' => "\F667",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "store",
            'content' => "\F4DC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "store-24-hour",
            'content' => "\F4DD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "storefront",
            'content' => "\F00EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stove",
            'content' => "\F4DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "strategy",
            'content' => "\F0201",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "strava",
            'content' => "\FB25",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stretch-to-page",
            'content' => "\FF48",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "stretch-to-page-outline",
            'content' => "\FF49",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "string-lights",
            'content' => "\F02E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "string-lights-off",
            'content' => "\F02E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subdirectory-arrow-left",
            'content' => "\F60C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subdirectory-arrow-right",
            'content' => "\F60D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subtitles",
            'content' => "\FA15",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subtitles-outline",
            'content' => "\FA16",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subway",
            'content' => "\F6AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subway-alert-variant",
            'content' => "\FD79",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "subway-variant",
            'content' => "\F4DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "summit",
            'content' => "\F785",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sunglasses",
            'content' => "\F4E0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "surround-sound",
            'content' => "\F5C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "surround-sound-2-0",
            'content' => "\F7EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "surround-sound-3-1",
            'content' => "\F7F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "surround-sound-5-1",
            'content' => "\F7F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "surround-sound-7-1",
            'content' => "\F7F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "svg",
            'content' => "\F720",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-horizontal",
            'content' => "\F4E1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-horizontal-bold",
            'content' => "\FBA9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-horizontal-circle",
            'content' => "\F0002",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-horizontal-circle-outline",
            'content' => "\F0003",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-horizontal-variant",
            'content' => "\F8C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-vertical",
            'content' => "\F4E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-vertical-bold",
            'content' => "\FBAA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-vertical-circle",
            'content' => "\F0004",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-vertical-circle-outline",
            'content' => "\F0005",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swap-vertical-variant",
            'content' => "\F8C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "swim",
            'content' => "\F4E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "switch",
            'content' => "\F4E4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sword",
            'content' => "\F4E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sword-cross",
            'content' => "\F786",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "symfony",
            'content' => "\FAE5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sync",
            'content' => "\F4E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sync-alert",
            'content' => "\F4E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "sync-off",
            'content' => "\F4E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tab",
            'content' => "\F4E9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tab-minus",
            'content' => "\FB26",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tab-plus",
            'content' => "\F75B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tab-remove",
            'content' => "\FB27",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tab-unselected",
            'content' => "\F4EA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table",
            'content' => "\F4EB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-border",
            'content' => "\FA17",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-chair",
            'content' => "\F0083",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-column",
            'content' => "\F834",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-column-plus-after",
            'content' => "\F4EC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-column-plus-before",
            'content' => "\F4ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-column-remove",
            'content' => "\F4EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-column-width",
            'content' => "\F4EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-edit",
            'content' => "\F4F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-eye",
            'content' => "\F00BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-headers-eye",
            'content' => "\F0248",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-headers-eye-off",
            'content' => "\F0249",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-large",
            'content' => "\F4F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-large-plus",
            'content' => "\FFA4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-large-remove",
            'content' => "\FFA5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-merge-cells",
            'content' => "\F9A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-of-contents",
            'content' => "\F835",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-plus",
            'content' => "\FA74",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-remove",
            'content' => "\FA75",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-row",
            'content' => "\F836",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-row-height",
            'content' => "\F4F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-row-plus-after",
            'content' => "\F4F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-row-plus-before",
            'content' => "\F4F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-row-remove",
            'content' => "\F4F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-search",
            'content' => "\F90E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-settings",
            'content' => "\F837",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "table-tennis",
            'content' => "\FE4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tablet",
            'content' => "\F4F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tablet-android",
            'content' => "\F4F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tablet-cellphone",
            'content' => "\F9A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tablet-dashboard",
            'content' => "\FEEB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tablet-ipad",
            'content' => "\F4F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "taco",
            'content' => "\F761",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag",
            'content' => "\F4F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-faces",
            'content' => "\F4FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-heart",
            'content' => "\F68A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-heart-outline",
            'content' => "\FBAB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-minus",
            'content' => "\F90F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-minus-outline",
            'content' => "\F024A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-multiple",
            'content' => "\F4FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-multiple-outline",
            'content' => "\F0322",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-off",
            'content' => "\F024B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-off-outline",
            'content' => "\F024C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-outline",
            'content' => "\F4FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-plus",
            'content' => "\F721",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-plus-outline",
            'content' => "\F024D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-remove",
            'content' => "\F722",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-remove-outline",
            'content' => "\F024E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-text",
            'content' => "\F024F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tag-text-outline",
            'content' => "\F4FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tank",
            'content' => "\FD16",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tanker-truck",
            'content' => "\F0006",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tape-measure",
            'content' => "\FB28",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "target",
            'content' => "\F4FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "target-account",
            'content' => "\FBAC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "target-variant",
            'content' => "\FA76",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "taxi",
            'content' => "\F4FF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tea",
            'content' => "\FD7A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tea-outline",
            'content' => "\FD7B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "teach",
            'content' => "\F88F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "teamviewer",
            'content' => "\F500",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "telegram",
            'content' => "\F501",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "telescope",
            'content' => "\FB29",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television",
            'content' => "\F502",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-box",
            'content' => "\F838",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-classic",
            'content' => "\F7F3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-classic-off",
            'content' => "\F839",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-clean",
            'content' => "\F013B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-guide",
            'content' => "\F503",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-off",
            'content' => "\F83A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-pause",
            'content' => "\FFA6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-play",
            'content' => "\FEEC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "television-stop",
            'content' => "\FFA7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "temperature-celsius",
            'content' => "\F504",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "temperature-fahrenheit",
            'content' => "\F505",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "temperature-kelvin",
            'content' => "\F506",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tennis",
            'content' => "\FD7C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tennis-ball",
            'content' => "\F507",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tent",
            'content' => "\F508",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "terraform",
            'content' => "\F0084",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "terrain",
            'content' => "\F509",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "test-tube",
            'content' => "\F668",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "test-tube-empty",
            'content' => "\F910",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "test-tube-off",
            'content' => "\F911",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text",
            'content' => "\F9A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text-recognition",
            'content' => "\F0168",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text-shadow",
            'content' => "\F669",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text-short",
            'content' => "\F9A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text-subject",
            'content' => "\F9A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text-to-speech",
            'content' => "\F50A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "text-to-speech-off",
            'content' => "\F50B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "textarea",
            'content' => "\F00C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "textbox",
            'content' => "\F60E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "textbox-password",
            'content' => "\F7F4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "texture",
            'content' => "\F50C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "texture-box",
            'content' => "\F0007",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "theater",
            'content' => "\F50D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "theme-light-dark",
            'content' => "\F50E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer",
            'content' => "\F50F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-alert",
            'content' => "\FE61",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-chevron-down",
            'content' => "\FE62",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-chevron-up",
            'content' => "\FE63",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-high",
            'content' => "\F00ED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-lines",
            'content' => "\F510",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-low",
            'content' => "\F00EE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-minus",
            'content' => "\FE64",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermometer-plus",
            'content' => "\FE65",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermostat",
            'content' => "\F393",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thermostat-box",
            'content' => "\F890",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thought-bubble",
            'content' => "\F7F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thought-bubble-outline",
            'content' => "\F7F6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thumb-down",
            'content' => "\F511",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thumb-down-outline",
            'content' => "\F512",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thumb-up",
            'content' => "\F513",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thumb-up-outline",
            'content' => "\F514",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "thumbs-up-down",
            'content' => "\F515",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ticket",
            'content' => "\F516",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ticket-account",
            'content' => "\F517",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ticket-confirmation",
            'content' => "\F518",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ticket-outline",
            'content' => "\F912",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ticket-percent",
            'content' => "\F723",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tie",
            'content' => "\F519",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tilde",
            'content' => "\F724",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timelapse",
            'content' => "\F51A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline",
            'content' => "\FBAD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-alert",
            'content' => "\FFB2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-alert-outline",
            'content' => "\FFB5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-clock",
            'content' => "\F0226",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-clock-outline",
            'content' => "\F0227",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-help",
            'content' => "\FFB6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-help-outline",
            'content' => "\FFB7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-outline",
            'content' => "\FBAE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-plus",
            'content' => "\FFB3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-plus-outline",
            'content' => "\FFB4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-text",
            'content' => "\FBAF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timeline-text-outline",
            'content' => "\FBB0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer",
            'content' => "\F51B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer-10",
            'content' => "\F51C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer-3",
            'content' => "\F51D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer-off",
            'content' => "\F51E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer-sand",
            'content' => "\F51F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer-sand-empty",
            'content' => "\F6AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timer-sand-full",
            'content' => "\F78B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "timetable",
            'content' => "\F520",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toaster",
            'content' => "\F0085",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toaster-off",
            'content' => "\F01E2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toaster-oven",
            'content' => "\FCAF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toggle-switch",
            'content' => "\F521",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toggle-switch-off",
            'content' => "\F522",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toggle-switch-off-outline",
            'content' => "\FA18",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toggle-switch-outline",
            'content' => "\FA19",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toilet",
            'content' => "\F9AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toolbox",
            'content' => "\F9AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toolbox-outline",
            'content' => "\F9AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tools",
            'content' => "\F0086",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip",
            'content' => "\F523",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-account",
            'content' => "\F00C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-edit",
            'content' => "\F524",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-edit-outline",
            'content' => "\F02F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-image",
            'content' => "\F525",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-image-outline",
            'content' => "\FBB1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-outline",
            'content' => "\F526",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-plus",
            'content' => "\FBB2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-plus-outline",
            'content' => "\F527",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-text",
            'content' => "\F528",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooltip-text-outline",
            'content' => "\FBB3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooth",
            'content' => "\F8C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tooth-outline",
            'content' => "\F529",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toothbrush",
            'content' => "\F0154",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toothbrush-electric",
            'content' => "\F0157",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toothbrush-paste",
            'content' => "\F0155",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tor",
            'content' => "\F52A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tortoise",
            'content' => "\FD17",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toslink",
            'content' => "\F02E3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tournament",
            'content' => "\F9AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tower-beach",
            'content' => "\F680",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tower-fire",
            'content' => "\F681",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "towing",
            'content' => "\F83B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick",
            'content' => "\F02B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-marker",
            'content' => "\F02B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-marker-outline",
            'content' => "\F02B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-minus",
            'content' => "\F02B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-minus-outline",
            'content' => "\F02B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-outline",
            'content' => "\F02B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-plus",
            'content' => "\F02B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-plus-outline",
            'content' => "\F02BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-remove",
            'content' => "\F02BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-remove-outline",
            'content' => "\F02BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-search",
            'content' => "\F02BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "toy-brick-search-outline",
            'content' => "\F02BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "track-light",
            'content' => "\F913",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trackpad",
            'content' => "\F7F7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trackpad-lock",
            'content' => "\F932",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tractor",
            'content' => "\F891",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trademark",
            'content' => "\FA77",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "traffic-light",
            'content' => "\F52B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "train",
            'content' => "\F52C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "train-car",
            'content' => "\FBB4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "train-variant",
            'content' => "\F8C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tram",
            'content' => "\F52D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tram-side",
            'content' => "\F0008",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transcribe",
            'content' => "\F52E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transcribe-close",
            'content' => "\F52F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transfer",
            'content' => "\F0087",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transfer-down",
            'content' => "\FD7D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transfer-left",
            'content' => "\FD7E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transfer-right",
            'content' => "\F530",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transfer-up",
            'content' => "\FD7F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transit-connection",
            'content' => "\FD18",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transit-connection-variant",
            'content' => "\FD19",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transit-detour",
            'content' => "\FFA8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transit-transfer",
            'content' => "\F6AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transition",
            'content' => "\F914",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transition-masked",
            'content' => "\F915",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "translate",
            'content' => "\F5CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "translate-off",
            'content' => "\FE66",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "transmission-tower",
            'content' => "\FD1A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trash-can",
            'content' => "\FA78",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trash-can-outline",
            'content' => "\FA79",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tray",
            'content' => "\F02BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tray-alert",
            'content' => "\F02C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tray-full",
            'content' => "\F02C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tray-minus",
            'content' => "\F02C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tray-plus",
            'content' => "\F02C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tray-remove",
            'content' => "\F02C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "treasure-chest",
            'content' => "\F725",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tree",
            'content' => "\F531",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tree-outline",
            'content' => "\FE4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trello",
            'content' => "\F532",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trending-down",
            'content' => "\F533",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trending-neutral",
            'content' => "\F534",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trending-up",
            'content' => "\F535",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "triangle",
            'content' => "\F536",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "triangle-outline",
            'content' => "\F537",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "triforce",
            'content' => "\FBB5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trophy",
            'content' => "\F538",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trophy-award",
            'content' => "\F539",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trophy-broken",
            'content' => "\FD80",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trophy-outline",
            'content' => "\F53A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trophy-variant",
            'content' => "\F53B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trophy-variant-outline",
            'content' => "\F53C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck",
            'content' => "\F53D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-check",
            'content' => "\FCB0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-check-outline",
            'content' => "\F02C5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-delivery",
            'content' => "\F53E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-delivery-outline",
            'content' => "\F02C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-fast",
            'content' => "\F787",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-fast-outline",
            'content' => "\F02C7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-outline",
            'content' => "\F02C8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "truck-trailer",
            'content' => "\F726",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "trumpet",
            'content' => "\F00C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tshirt-crew",
            'content' => "\FA7A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tshirt-crew-outline",
            'content' => "\F53F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tshirt-v",
            'content' => "\FA7B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tshirt-v-outline",
            'content' => "\F540",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tumble-dryer",
            'content' => "\F916",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tumble-dryer-alert",
            'content' => "\F01E5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tumble-dryer-off",
            'content' => "\F01E6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tumblr",
            'content' => "\F541",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tumblr-box",
            'content' => "\F917",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tumblr-reblog",
            'content' => "\F542",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tune",
            'content' => "\F62E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "tune-vertical",
            'content' => "\F66A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "turnstile",
            'content' => "\FCB1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "turnstile-outline",
            'content' => "\FCB2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "turtle",
            'content' => "\FCB3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "twitch",
            'content' => "\F543",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "twitter",
            'content' => "\F544",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "twitter-box",
            'content' => "\F545",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "twitter-circle",
            'content' => "\F546",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "twitter-retweet",
            'content' => "\F547",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "two-factor-authentication",
            'content' => "\F9AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "typewriter",
            'content' => "\FF4A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "uber",
            'content' => "\F748",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ubisoft",
            'content' => "\FBB6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ubuntu",
            'content' => "\F548",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ufo",
            'content' => "\F00EF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ufo-outline",
            'content' => "\F00F0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ultra-high-definition",
            'content' => "\F7F8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "umbraco",
            'content' => "\F549",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "umbrella",
            'content' => "\F54A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "umbrella-closed",
            'content' => "\F9AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "umbrella-outline",
            'content' => "\F54B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "undo",
            'content' => "\F54C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "undo-variant",
            'content' => "\F54D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unfold-less-horizontal",
            'content' => "\F54E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unfold-less-vertical",
            'content' => "\F75F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unfold-more-horizontal",
            'content' => "\F54F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unfold-more-vertical",
            'content' => "\F760",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "ungroup",
            'content' => "\F550",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unicode",
            'content' => "\FEED",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unity",
            'content' => "\F6AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "unreal",
            'content' => "\F9B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "untappd",
            'content' => "\F551",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "update",
            'content' => "\F6AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload",
            'content' => "\F552",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload-multiple",
            'content' => "\F83C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload-network",
            'content' => "\F6F5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload-network-outline",
            'content' => "\FCB4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload-off",
            'content' => "\F00F1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload-off-outline",
            'content' => "\F00F2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "upload-outline",
            'content' => "\FE67",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "usb",
            'content' => "\F553",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "usb-flash-drive",
            'content' => "\F02C9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "usb-flash-drive-outline",
            'content' => "\F02CA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "usb-port",
            'content' => "\F021B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "valve",
            'content' => "\F0088",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "valve-closed",
            'content' => "\F0089",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "valve-open",
            'content' => "\F008A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "van-passenger",
            'content' => "\F7F9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "van-utility",
            'content' => "\F7FA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vanish",
            'content' => "\F7FB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vanity-light",
            'content' => "\F020C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "variable",
            'content' => "\FAE6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "variable-box",
            'content' => "\F013C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-arrange-above",
            'content' => "\F554",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-arrange-below",
            'content' => "\F555",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-bezier",
            'content' => "\FAE7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-circle",
            'content' => "\F556",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-circle-variant",
            'content' => "\F557",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-combine",
            'content' => "\F558",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-curve",
            'content' => "\F559",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-difference",
            'content' => "\F55A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-difference-ab",
            'content' => "\F55B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-difference-ba",
            'content' => "\F55C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-ellipse",
            'content' => "\F892",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-intersection",
            'content' => "\F55D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-line",
            'content' => "\F55E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-link",
            'content' => "\F0009",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-point",
            'content' => "\F55F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-polygon",
            'content' => "\F560",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-polyline",
            'content' => "\F561",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-polyline-edit",
            'content' => "\F0250",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-polyline-minus",
            'content' => "\F0251",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-polyline-plus",
            'content' => "\F0252",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-polyline-remove",
            'content' => "\F0253",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-radius",
            'content' => "\F749",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-rectangle",
            'content' => "\F5C6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-selection",
            'content' => "\F562",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-square",
            'content' => "\F001",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-triangle",
            'content' => "\F563",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vector-union",
            'content' => "\F564",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "venmo",
            'content' => "\F578",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vhs",
            'content' => "\FA1A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vibrate",
            'content' => "\F566",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vibrate-off",
            'content' => "\FCB5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video",
            'content' => "\F567",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-3d",
            'content' => "\F7FC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-3d-variant",
            'content' => "\FEEE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-4k-box",
            'content' => "\F83D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-account",
            'content' => "\F918",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-check",
            'content' => "\F008B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-check-outline",
            'content' => "\F008C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-image",
            'content' => "\F919",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-input-antenna",
            'content' => "\F83E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-input-component",
            'content' => "\F83F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-input-hdmi",
            'content' => "\F840",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-input-scart",
            'content' => "\FFA9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-input-svideo",
            'content' => "\F841",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-minus",
            'content' => "\F9B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-off",
            'content' => "\F568",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-off-outline",
            'content' => "\FBB7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-outline",
            'content' => "\FBB8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-plus",
            'content' => "\F9B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-stabilization",
            'content' => "\F91A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-switch",
            'content' => "\F569",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-vintage",
            'content' => "\FA1B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-wireless",
            'content' => "\FEEF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "video-wireless-outline",
            'content' => "\FEF0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-agenda",
            'content' => "\F56A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-agenda-outline",
            'content' => "\F0203",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-array",
            'content' => "\F56B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-carousel",
            'content' => "\F56C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-column",
            'content' => "\F56D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-comfy",
            'content' => "\FE4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-compact",
            'content' => "\FE4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-compact-outline",
            'content' => "\FE4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-dashboard",
            'content' => "\F56E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-dashboard-outline",
            'content' => "\FA1C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-dashboard-variant",
            'content' => "\F842",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-day",
            'content' => "\F56F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-grid",
            'content' => "\F570",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-grid-outline",
            'content' => "\F0204",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-grid-plus",
            'content' => "\FFAA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-grid-plus-outline",
            'content' => "\F0205",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-headline",
            'content' => "\F571",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-list",
            'content' => "\F572",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-module",
            'content' => "\F573",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-parallel",
            'content' => "\F727",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-quilt",
            'content' => "\F574",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-sequential",
            'content' => "\F728",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-split-horizontal",
            'content' => "\FBA7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-split-vertical",
            'content' => "\FBA8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-stream",
            'content' => "\F575",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "view-week",
            'content' => "\F576",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vimeo",
            'content' => "\F577",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "violin",
            'content' => "\F60F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "virtual-reality",
            'content' => "\F893",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "visual-studio",
            'content' => "\F610",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "visual-studio-code",
            'content' => "\FA1D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vk",
            'content' => "\F579",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vk-box",
            'content' => "\F57A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vk-circle",
            'content' => "\F57B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vlc",
            'content' => "\F57C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "voice",
            'content' => "\F5CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "voice-off",
            'content' => "\FEF1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "voicemail",
            'content' => "\F57D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volleyball",
            'content' => "\F9B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-high",
            'content' => "\F57E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-low",
            'content' => "\F57F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-medium",
            'content' => "\F580",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-minus",
            'content' => "\F75D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-mute",
            'content' => "\F75E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-off",
            'content' => "\F581",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-plus",
            'content' => "\F75C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-source",
            'content' => "\F014B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-variant-off",
            'content' => "\FE68",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "volume-vibrate",
            'content' => "\F014C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vote",
            'content' => "\FA1E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vote-outline",
            'content' => "\FA1F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vpn",
            'content' => "\F582",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vuejs",
            'content' => "\F843",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "vuetify",
            'content' => "\FE50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "walk",
            'content' => "\F583",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wall",
            'content' => "\F7FD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wall-sconce",
            'content' => "\F91B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wall-sconce-flat",
            'content' => "\F91C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wall-sconce-variant",
            'content' => "\F91D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet",
            'content' => "\F584",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet-giftcard",
            'content' => "\F585",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet-membership",
            'content' => "\F586",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet-outline",
            'content' => "\FBB9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet-plus",
            'content' => "\FFAB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet-plus-outline",
            'content' => "\FFAC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallet-travel",
            'content' => "\F587",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wallpaper",
            'content' => "\FE69",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wan",
            'content' => "\F588",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wardrobe",
            'content' => "\FFAD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wardrobe-outline",
            'content' => "\FFAE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "warehouse",
            'content' => "\FFBB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "washing-machine",
            'content' => "\F729",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "washing-machine-alert",
            'content' => "\F01E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "washing-machine-off",
            'content' => "\F01E8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch",
            'content' => "\F589",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-export",
            'content' => "\F58A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-export-variant",
            'content' => "\F894",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-import",
            'content' => "\F58B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-import-variant",
            'content' => "\F895",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-variant",
            'content' => "\F896",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-vibrate",
            'content' => "\F6B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watch-vibrate-off",
            'content' => "\FCB6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water",
            'content' => "\F58C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-boiler",
            'content' => "\FFAF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-boiler-alert",
            'content' => "\F01DE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-boiler-off",
            'content' => "\F01DF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-off",
            'content' => "\F58D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-outline",
            'content' => "\FE6A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-percent",
            'content' => "\F58E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-polo",
            'content' => "\F02CB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-pump",
            'content' => "\F58F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-pump-off",
            'content' => "\FFB0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-well",
            'content' => "\F008D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "water-well-outline",
            'content' => "\F008E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "watermark",
            'content' => "\F612",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wave",
            'content' => "\FF4B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "waves",
            'content' => "\F78C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "waze",
            'content' => "\FBBA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-cloudy",
            'content' => "\F590",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-cloudy-alert",
            'content' => "\FF4C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-cloudy-arrow-right",
            'content' => "\FE51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-fog",
            'content' => "\F591",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-hail",
            'content' => "\F592",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-hazy",
            'content' => "\FF4D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-hurricane",
            'content' => "\F897",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-lightning",
            'content' => "\F593",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-lightning-rainy",
            'content' => "\F67D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-night",
            'content' => "\F594",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-night-partly-cloudy",
            'content' => "\FF4E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-partly-cloudy",
            'content' => "\F595",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-partly-lightning",
            'content' => "\FF4F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-partly-rainy",
            'content' => "\FF50",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-partly-snowy",
            'content' => "\FF51",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-partly-snowy-rainy",
            'content' => "\FF52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-pouring",
            'content' => "\F596",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-rainy",
            'content' => "\F597",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-snowy",
            'content' => "\F598",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-snowy-heavy",
            'content' => "\FF53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-snowy-rainy",
            'content' => "\F67E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-sunny",
            'content' => "\F599",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-sunny-alert",
            'content' => "\FF54",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-sunset",
            'content' => "\F59A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-sunset-down",
            'content' => "\F59B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-sunset-up",
            'content' => "\F59C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-tornado",
            'content' => "\FF55",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-windy",
            'content' => "\F59D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weather-windy-variant",
            'content' => "\F59E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "web",
            'content' => "\F59F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "web-box",
            'content' => "\FFB1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "web-clock",
            'content' => "\F0275",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "webcam",
            'content' => "\F5A0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "webhook",
            'content' => "\F62F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "webpack",
            'content' => "\F72A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "webrtc",
            'content' => "\F0273",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wechat",
            'content' => "\F611",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weight",
            'content' => "\F5A1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weight-gram",
            'content' => "\FD1B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weight-kilogram",
            'content' => "\F5A2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weight-lifter",
            'content' => "\F0188",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "weight-pound",
            'content' => "\F9B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "whatsapp",
            'content' => "\F5A3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wheelchair-accessibility",
            'content' => "\F5A4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "whistle",
            'content' => "\F9B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "whistle-outline",
            'content' => "\F02E7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "white-balance-auto",
            'content' => "\F5A5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "white-balance-incandescent",
            'content' => "\F5A6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "white-balance-iridescent",
            'content' => "\F5A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "white-balance-sunny",
            'content' => "\F5A8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "widgets",
            'content' => "\F72B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi",
            'content' => "\F5A9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-off",
            'content' => "\F5AA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-star",
            'content' => "\FE6B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-1",
            'content' => "\F91E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-1-alert",
            'content' => "\F91F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-1-lock",
            'content' => "\F920",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-2",
            'content' => "\F921",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-2-alert",
            'content' => "\F922",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-2-lock",
            'content' => "\F923",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-3",
            'content' => "\F924",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-3-alert",
            'content' => "\F925",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-3-lock",
            'content' => "\F926",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-4",
            'content' => "\F927",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-4-alert",
            'content' => "\F928",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-4-lock",
            'content' => "\F929",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-alert-outline",
            'content' => "\F92A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-lock-outline",
            'content' => "\F92B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-off",
            'content' => "\F92C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-off-outline",
            'content' => "\F92D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wifi-strength-outline",
            'content' => "\F92E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wii",
            'content' => "\F5AB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wiiu",
            'content' => "\F72C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wikipedia",
            'content' => "\F5AC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wind-turbine",
            'content' => "\FD81",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-close",
            'content' => "\F5AD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-closed",
            'content' => "\F5AE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-closed-variant",
            'content' => "\F0206",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-maximize",
            'content' => "\F5AF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-minimize",
            'content' => "\F5B0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-open",
            'content' => "\F5B1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-open-variant",
            'content' => "\F0207",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-restore",
            'content' => "\F5B2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-shutter",
            'content' => "\F0147",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-shutter-alert",
            'content' => "\F0148",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "window-shutter-open",
            'content' => "\F0149",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "windows",
            'content' => "\F5B3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "windows-classic",
            'content' => "\FA20",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wiper",
            'content' => "\FAE8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wiper-wash",
            'content' => "\FD82",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wordpress",
            'content' => "\F5B4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "worker",
            'content' => "\F5B5",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wrap",
            'content' => "\F5B6",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wrap-disabled",
            'content' => "\FBBB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wrench",
            'content' => "\F5B7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wrench-outline",
            'content' => "\FBBC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "wunderlist",
            'content' => "\F5B8",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xamarin",
            'content' => "\F844",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xamarin-outline",
            'content' => "\F845",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xaml",
            'content' => "\F673",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox",
            'content' => "\F5B9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller",
            'content' => "\F5BA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-alert",
            'content' => "\F74A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-charging",
            'content' => "\FA21",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-empty",
            'content' => "\F74B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-full",
            'content' => "\F74C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-low",
            'content' => "\F74D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-medium",
            'content' => "\F74E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-battery-unknown",
            'content' => "\F74F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-menu",
            'content' => "\FE52",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-off",
            'content' => "\F5BB",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xbox-controller-view",
            'content' => "\FE53",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xda",
            'content' => "\F5BC",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xing",
            'content' => "\F5BD",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xing-box",
            'content' => "\F5BE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xing-circle",
            'content' => "\F5BF",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xml",
            'content' => "\F5C0",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "xmpp",
            'content' => "\F7FE",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "yahoo",
            'content' => "\FB2A",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "yammer",
            'content' => "\F788",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "yeast",
            'content' => "\F5C1",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "yelp",
            'content' => "\F5C2",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "yin-yang",
            'content' => "\F67F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "yoga",
            'content' => "\F01A7",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "youtube",
            'content' => "\F5C3",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "youtube-creator-studio",
            'content' => "\F846",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "youtube-gaming",
            'content' => "\F847",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "youtube-subscription",
            'content' => "\FD1C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "youtube-tv",
            'content' => "\F448",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "z-wave",
            'content' => "\FAE9",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zend",
            'content' => "\FAEA",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zigbee",
            'content' => "\FD1D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zip-box",
            'content' => "\F5C4",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zip-box-outline",
            'content' => "\F001B",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zip-disk",
            'content' => "\FA22",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-aquarius",
            'content' => "\FA7C",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-aries",
            'content' => "\FA7D",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-cancer",
            'content' => "\FA7E",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-capricorn",
            'content' => "\FA7F",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-gemini",
            'content' => "\FA80",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-leo",
            'content' => "\FA81",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-libra",
            'content' => "\FA82",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-pisces",
            'content' => "\FA83",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-sagittarius",
            'content' => "\FA84",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-scorpio",
            'content' => "\FA85",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-taurus",
            'content' => "\FA86",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "zodiac-virgo",
            'content' => "\FA87",
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => "blank",
            'content' => "\F68C",
        ]);
    }
}
