<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            StatusesTableSeeder::class,
            UnitElementTypesTableSeeder::class,
            UsersTableSeeder::class,
            LessonElementTypesTableSeeder::class,
            MaterialIconsTableSeeder::class,
        ]);

        $faker = new Faker();

        factory(App\Course::class)->create([
            'title' => 'Rerum molestiae ad autem sit.',
            'description' => 'Commodi voluptas est sint magnam consequuntur eum quam adipisci. Mollitia dolor et consequatur sit. Tempora expedita sint sunt et et mollitia voluptas. Sequi similique quidem ipsa itaque.',
            'status_id' => 1,
            'user_id' => 1
        ]);

        factory(App\Course::class)->create([
            'title' => 'Rerum molestiae ad autem sit.',
            'description' => 'Commodi voluptas est sint magnam consequuntur eum quam adipisci. Mollitia dolor et consequatur sit. Tempora expedita sint sunt et et mollitia voluptas. Sequi similique quidem ipsa itaque.',
            'status_id' => 1,
            'user_id' => 1
        ]);

        factory(App\Unit::class)->create([
            'title' => 'Unit 1.',
            'description' => 'Commodi voluptas est sint magnam consequuntur.',
            'icon_name' => 'account',
            'user_id' => 1,
            'status_id' => 1,
            'course_id' => 1,
        ]);
        factory(App\Unit::class)->create([
            'title' => 'Unit 2.',
            'description' => 'Commodi magnam consequuntur.',
            'icon_name' => 'book',
            'user_id' => 2,
            'status_id' => 1,
            'course_id' => 1,
        ]);
        factory(App\Unit::class)->create([
            'title' => 'Unit 3.',
            'description' => 'Commodi voluptas est sint.',
            'icon_name' => 'weather-sunny',
            'user_id' => 3,
            'status_id' => 1,
            'course_id' => 1,
        ]);
    }
}
