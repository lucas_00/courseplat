<?php

use Illuminate\Database\Seeder;

class LessonElementTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\LessonElementType::class)->create([
            'name' => 'ql-content',
        ]);
        factory(App\LessonElementType::class)->create([
            'name' => 'link',
        ]);
        factory(App\LessonElementType::class)->create([
            'name' => 'video',
        ]);
        factory(App\LessonElementType::class)->create([
            'name' => 'image',
        ]);
    }
}
