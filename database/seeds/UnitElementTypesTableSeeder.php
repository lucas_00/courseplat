<?php

use Illuminate\Database\Seeder;

class UnitElementTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UnitElementType::class)->create([
            'name' => 'activity',
        ]);
        factory(App\UnitElementType::class)->create([
            'name' => 'lesson',
        ]);
        factory(App\UnitElementType::class)->create([
            'name' => 'exam',
        ]);
    }
}
