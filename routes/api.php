<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(["namespace" => "Api"],function() {
    Route::post('/login', 'AuthController@login')->name('login');
    Route::post('/register', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/logout', 'AuthController@logout');

        // Courses
        Route::get('/courses', 'CoursesController@index');
        Route::post('/courses', 'CoursesController@create');
        Route::get('/courses/{course}', 'CoursesController@show')
        ->where('course', '[0-9]+');
        Route::get('/courses/{course}/edit', 'CoursesController@edit')
        ->where('course', '[0-9]+');
        Route::put('/courses/{course}/update', 'CoursesController@update')
        ->where('course', '[0-9]+');

        // Units
        Route::post('/courses/{course}/units', 'UnitsController@create')
        ->where('course', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}', 'UnitsController@show')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');

        // Lessons
        Route::post('/courses/{course}/units/{unit}/lessons', 'LessonsController@create')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}/lessons/{lesson}', 'LessonsController@show')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}/lessons/{lesson}/edit', 'LessonsController@edit')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');

        // Comments
        Route::post('/courses/{course}/comments', 'CommentController@courseStore')
        ->where('course', '[0-9]+');
        Route::post('/courses/{course}/lessons/{lesson}/comments', 'CommentController@lessonStore')
        ->where('course', '[0-9]+')
        ->where('lesson', '[0-9]+');

        // Icons
        Route::get('/icons', 'MaterialIconsController@show');
    });
});
