<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonElementList extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lesson_id', 'lesson_element_type_id', 'order'
    ];

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function lessonElementType()
    {
        return $this->belongsTo(LessonElementType::class);
    }

    public function lessonElementContent()
    {
        return $this->hasOne(LessonElementContent::class);
    }

    public function lessonElementVideo()
    {
        return $this->hasOne(LessonElementVideo::class);
    }

    public function lessonElementLink()
    {
        return $this->hasOne(LessonElementLink::class);
    }

    public function lessonElementImages()
    {
        return $this->hasMany(LessonElementImage::class);
    }

}
