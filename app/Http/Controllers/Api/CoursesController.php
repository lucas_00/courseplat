<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Course;
use App\ElementList;
use App\CommentCourse;
use App\CourseImage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Traits\FileUpload;

class CoursesController extends Controller
{
    use FileUpload;

    public function index()
    {
        $user = auth()->user();

        $courses = $user->course;

        $course_json = array();

        for ($i = 0; $i < count($courses); $i++) {
            if($courses[$i]->courseImage){
                $backgroundImage = $courses[$i]->courseImage->path;
            }else{
                $backgroundImage = null;
            }

            array_push(
                $course_json,
                array(
                    'id' => $courses[$i]->id,
                    'title' => $courses[$i]->title,
                    'description' => $courses[$i]->description,
                    'status' => $courses[$i]->status->title,
                    'background' => $backgroundImage,
                )
            );

        }
        return response()->json($course_json);
    }

    public function show($id)
    {
        $course = Course::with('status')
        ->with('courseImage')
        ->with('units')
        ->with('commentCourse')
        ->find($id);

        $comments = CommentCourse::with('user')
        ->where('course_id', $id)
        ->orderBy('created_at', 'asc')
        ->get();

        return response()->json(array('course' => $course, 'comments' => $comments));
    }

    public function edit($id)
    {
        $course = Course::with('courseImage')
        ->find($id);

        return response()->json(array('course' => $course));
    }

    public function update($course_id, Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title' => 'required|string',
                'description' => 'required|string',
            ]);

            if($request->file != 'null'){
                $image = $this->saveFiles($request->file, 'courses/');

                $courseImage = new CourseImage([
                    'name'     => $image,
                    'original_name'     => $request->file->getClientOriginalName(),
                    'path'     => ('/uploads/courses/' . $image),
                    'user_id'    => $user->id,
                ]);

                $courseImage->save();

                $course = Course::find($course_id);
                $course->title = $request->title;
                $course->description = $request->description;
                $course->course_image_id = $courseImage->id;
                $course->save();

            }else{
                $course = Course::find($course_id);
                $course->title = $request->title;
                $course->description = $request->description;
                $course->save();
            }

            return response()->json([
                'message' => 'Successfully created course!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }

    public function create(Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title'     => 'required|string',
                'description'    => 'required|string|',
            ]);

            if($request->file != 'null'){
                $image = $this->saveFiles($request->file, 'courses/');

                $courseImage = new CourseImage([
                    'name'     => $image,
                    'original_name'     => $request->file->getClientOriginalName(),
                    'path'     => ('/uploads/courses/' . $image),
                    'user_id'    => $user->id,
                ]);

                $courseImage->save();

                $course = new Course([
                    'title'     => $request->title,
                    'description'    => $request->description,
                    'status_id'    => 1,
                    'user_id'    => $user->id,
                ]);

                $course->save();

            }else{
                $course = new Course([
                    'title'     => $request->title,
                    'description'    => $request->description,
                    'status_id'    => 1,
                    'user_id'    => $user->id,
                ]);

                $course->save();
            }

            return response()->json([
                'message' => 'Successfully created course!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }
}
