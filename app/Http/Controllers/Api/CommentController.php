<?php

namespace App\Http\Controllers\Api;

use App\CommentCourse;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    public function courseStore($course_id, Request $request)
    {
        try{
            $validator = request()->validate([
                'message' => 'required|string',
            ]);

            $user = auth()->user();

            $comment = new CommentCourse([
                'content'   => $request->message,
                'course_id'     => $course_id,
                'user_id'    => $user->id,
            ]);

            $comment->save();

            $newComment = CommentCourse::with('user')
                ->where('user_id', $user->id)
                ->where('course_id', $course_id)
                ->orderBy('created_at', 'desc')
                ->first();

            return response()->json([
                'comment' => $newComment],  201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json('Something went wrong on the server.', $e->status);
        }
    }

    public function lessonStore($lesson_id, Request $request)
    {
        try{
            $validator = request()->validate([
                'message' => 'required|string',
            ]);

            $user = auth()->user();

            $comment = new CommentLesson([
                'content'   => $request->message,
                'lesson_id'     => $lesson_id,
                'user_id'    => $user->id,
            ]);

            $comment->save();

            $newComment = CommentLesson::with('user')
                ->where('user_id', $user->id)
                ->where('lesson_id', $lesson_id)
                ->orderBy('created_at', 'desc')
                ->first();

            return response()->json([
                'comment' => $newComment],  201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json('Something went wrong on the server.', $e->status);
        }
    }

    public function unitStore($unit_id, Request $request)
    {
        try{
            $validator = request()->validate([
                'message' => 'required|string',
            ]);

            $user = auth()->user();

            $comment = new CommentUnit([
                'content'   => $request->message,
                'unit_id'     => $unit_id,
                'user_id'    => $user->id,
            ]);

            $comment->save();

            $newComment = CommentUnit::with('user')
                ->where('user_id', $user->id)
                ->where('unit_id', $unit_id)
                ->orderBy('created_at', 'desc')
                ->first();

            return response()->json([
                'comment' => $newComment],  201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json('Something went wrong on the server.', $e->status);
        }
    }
}
