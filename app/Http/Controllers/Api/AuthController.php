<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'username'    => 'required|string|email',
            'password'    => 'required|string|min:6',
        ]);

        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);

            return $response->getBody();

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            if ($e->getCode() === 400) {
                return response()->json('Your credentials are incorrect. Please try again.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Please enter a valid email or a password.', $e->getCode());
            }

            return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function signup(Request $request)
    {
        try{
            $validator = $request->validate([
                'name'     => 'required|string',
                'email'    => 'required|string|email|confirmed|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

            $user = new User([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $user->save();

            return response()->json([
                'message' => 'Successfully created user!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){

            if ($e->status === 422) {
                return response()->json('The email has already been taken.', $e->status);
            }

            return response()->json('Something went wrong on the server.', $e->status);
        }
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }
}
