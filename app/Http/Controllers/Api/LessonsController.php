<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Lesson;
use App\LessonFile;
use App\LessonElementList;
use App\LessonElementContent;
use App\LessonElementLink;
use App\LessonElementVideo;
use App\LessonElementImage;
use App\UnitElement;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Traits\FileUpload;

class LessonsController extends Controller
{
    use FileUpload;

    public function index($course_id, $lesson_id)
    {
    }

    public function show($course_id, $unit_id, $lesson_id)
    {
        $lesson = Lesson::whereHas('unit', function($q) use($course_id){
            $q->where('course_id', $course_id);
         })
        ->where('unit_id', $unit_id)
        ->with('lessonFiles')
        ->with('status')
        ->with('lessonElementLists', 'lessonElementLists.lessonElementType', 'lessonElementLists.lessonElementContent', 'lessonElementLists.lessonElementLink', 'lessonElementLists.lessonElementVideo', 'lessonElementLists.lessonElementImages')
        ->find($lesson_id);

        return response()->json(array('lesson' => $lesson));
    }

    public function edit($course_id, $unit_id, $lesson_id)
    {
        $lesson = Lesson::where('course_id', $course_id)
        ->where('unit_id', $unit_id)
        ->with('lessonFiles')
        ->with('status')
        ->with('lessonElementLists', 'lessonElementLists.lessonElementType', 'lessonElementLists.lessonElementContent', 'lessonElementLists.lessonElementLink', 'lessonElementLists.lessonElementVideo', 'lessonElementLists.lessonElementImages')
        ->find($lesson_id);

        return response()->json(array('lesson' => $lesson));
    }

    public function create($course_id, $unit_id, Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title'     => 'required|string',
            ]);

            // Lesson store
            $lesson = new Lesson([
                'title'     => $request->title,
                'info'    => $request->info,
                'status_id'    => 1,
                'unit_id'    => $unit_id,
                'user_id'    => $user->id,
            ]);

            $lesson->save();

            // Lesson files store
            for ($i=0; $i < count($request->files); $i++) {
                if ($request->hasFile('files-' . $i)) {
                    $requestFile = $request->file('files-' . $i);
                    $file = $this->saveFiles($requestFile, 'lessons/');

                    $lessonFile = new LessonFile([
                        'name'     => $file,
                        'original_name'     => $requestFile->getClientOriginalName(),
                        'path'     => ('/uploads/lessons/' . $file),
                        'user_id'    => $user->id,
                        'lesson_id'    => $lesson->id,
                    ]);

                    $lessonFile->save();
                }
            }

            $lessonContents = json_decode($request->lessonContent, true);

            if (count($lessonContents)) {
                for ($i=0; $i < count($lessonContents); $i++) {

                    $object = json_decode(json_encode($lessonContents[$i]));

                    if ($object->type == 'ql-content') {
                        $lessonElementList = new LessonElementList([
                            'order'    => $i,
                            'lesson_id'    => $lesson->id,
                            'lesson_element_type_id' => 1
                        ]);

                        $lessonElementList->save();

                        $lessonElementContent = new LessonElementContent([
                            'content'    => $object->content,
                            'lesson_element_list_id' => $lessonElementList->id,
                        ]);

                        $lessonElementContent->save();

                    }elseif ($object->type == 'link') {
                        $lessonElementList = new LessonElementList([
                            'order'    => $i,
                            'lesson_id'    => $lesson->id,
                            'lesson_element_type_id' => 2
                        ]);

                        $lessonElementList->save();

                        $lessonElementLink = new LessonElementLink([
                            'content'    => $object->content,
                            'lesson_element_list_id' => $lessonElementList->id,
                        ]);

                        $lessonElementLink->save();

                    }elseif ($object->type == 'video') {
                        $lessonElementList = new LessonElementList([
                            'order'    => $i,
                            'lesson_id'    => $lesson->id,
                            'lesson_element_type_id' => 3
                        ]);

                        $lessonElementList->save();

                        $lessonElementVideo = new LessonElementVideo([
                            'content'    => $object->content,
                            'lesson_element_list_id' => $lessonElementList->id,
                        ]);

                        $lessonElementVideo->save();

                    }elseif ($object->type == 'image') {
                        $lessonElementList = new LessonElementList([
                            'order'    => $i,
                            'lesson_id'    => $lesson->id,
                            'lesson_element_type_id' => 4
                        ]);

                        $lessonElementList->save();

                        for ($y=0; $y < count($request->files); $y++) {
                            if ($request->hasFile('lessonContents-' . $i . '-image-' . $y)) {
                                $requestFile = $request->file('lessonContents-' . $i . '-image-' . $y);
                                $file = $this->saveFiles($requestFile, 'lessons/contents');

                                $lessonElementImage = new LessonElementImage([
                                    'name'     => $file,
                                    'original_name'     => $requestFile->getClientOriginalName(),
                                    'path'     => ('/uploads/lessons/contents/' . $file),
                                    'lesson_element_list_id' => $lessonElementList->id,
                                ]);

                                $lessonElementImage->save();
                            }
                        }
                    }
                }
            }

            // Add Element List
            $countList = UnitElement::where('unit_id', $unit_id)
                ->count();

            $unitElement = new UnitElement([
                'order'    => $countList + 1,
                'unit_id'    => $unit_id,
                'lesson_id'    => $lesson->id,
                'activity_id'    => null,
                'exam_id'    => null,
                'unit_element_type_id' => 2
            ]);

            $unitElement->save();

            return response()->json([
                'message' => 'Successfully created unit!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }
}
