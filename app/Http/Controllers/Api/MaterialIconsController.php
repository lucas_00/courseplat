<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\MaterialIcon;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class MaterialIconsController extends Controller
{
    public function show()
    {
        $material_icons = MaterialIcon::all();

        return response()->json(array('icons' => $material_icons));
    }

}
