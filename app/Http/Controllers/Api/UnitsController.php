<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Unit;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UnitsController extends Controller
{
    public function show($course_id, $unit_id)
    {
        $unit = Unit::with('status')
        ->where('course_id', $course_id)
        ->with('course')
        ->with('unitElements', 'unitElements.unitElementType', 'unitElements.lesson', 'unitElements.activity', 'unitElements.exam')
        ->find($unit_id);

        // $comments = CommentCourse::with('user')
        // ->where('course_id', $id)
        // ->orderBy('created_at', 'asc')
        // ->get();

        return response()->json(array('unit' => $unit));
    }

    public function create($course_id, Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title'     => 'required|string',
                'description'    => 'required|string|',
            ]);

            $unit = new Unit([
                'title'     => $request->title,
                'description'    => $request->description,
                'icon_name'    => $request->iconName,
                'user_id'    => $user->id,
                'status_id'    => 1,
                'course_id'    => $course_id,
            ]);

            $unit->save();

            return response()->json([
                'message' => 'Successfully created Unit!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }
}
