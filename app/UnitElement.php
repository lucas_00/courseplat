<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitElement extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order', 'unit_id', 'unit_element_type_id', 'lesson_id', 'activity_id', 'exam_id'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function unitElementType()
    {
        return $this->belongsTo(UnitElementType::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }
}

