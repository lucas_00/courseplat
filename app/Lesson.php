<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'info', 'status_id', 'unit_id', 'user_id',
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function comments()
    {
        return $this->hasMany(CommentLesson::class);
    }

    public function lessonFiles()
    {
        return $this->hasMany(LessonFile::class);
    }

    public function lessonElementLists()
    {
        return $this->hasMany(LessonElementList::class);
    }

}
